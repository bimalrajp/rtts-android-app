package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Bounds implements Parcelable {

	public static final Creator<Bounds> CREATOR = new Creator<Bounds>() {
		@Override
		public Bounds createFromParcel(Parcel in) {
			return new Bounds(in);
		}

		@Override
		public Bounds[] newArray(int size) {
			return new Bounds[size];
		}
	};

	private Northeast northeast;
	private Southwest southwest;

	protected Bounds(Parcel in) {
		northeast = in.readParcelable(Northeast.class.getClassLoader());
		southwest = in.readParcelable(Southwest.class.getClassLoader());
	}

	public Northeast getNortheast() {
		return northeast;
	}
	public void setNortheast(Northeast northeast) {
		this.northeast = northeast;
	}
	public Southwest getSouthwest() {
		return southwest;
	}
	public void setSouthwest(Southwest southwest) {
		this.southwest = southwest;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(northeast, i);
		parcel.writeParcelable(southwest, i);
	}
}
