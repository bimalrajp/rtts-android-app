package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.api_callbacks.Customer;
import com.cordsinnovations.rtts.api_callbacks.Register;
import com.cordsinnovations.rtts.api_callbacks.VehicleType;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.CustomerUtils;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.api;
import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.ui.RegisterOtpVerificationActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.RegisterOtpVerificationActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.RegisterOtpVerificationActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.RegisterOtpVerificationActivity.UI_ELEMENTS.OTP_FORM.*;

public class RegisterOtpVerificationActivity extends AppCompatActivity implements View.OnClickListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Typeface typeFace;
		static InputMethodManager inputMethodManager = null;
	}

	static class ACTIVITY_UTILS {
		static AppCommonUtils appCommonUtils;
		static CustomerUtils customerUtils;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static TextView tv_register_otp_verification_title;
		}

		static class OTP_FORM {
			static EditText et_customer_otp;
			static Button btn_customer_otp_verification;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_otp_verification);

		configView();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_customer_otp_verification:
				verifyOTP();
				break;
		}
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(context, LoginActivity.class));
		finish();
	}

	void configView() {

		getSupportActionBar().hide();

		activity = this;
		context = this;
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");

		customerUtils = new CustomerUtils(activity, context);
		appCommonUtils = new AppCommonUtils(activity, context);

		tv_register_otp_verification_title = findViewById(R.id.tv_register_otp_verification_title);
		tv_register_otp_verification_title.setTypeface(typeFace);

		et_customer_otp = findViewById(R.id.et_customer_otp);
		btn_customer_otp_verification = findViewById(R.id.btn_customer_otp_verification);
		btn_customer_otp_verification.setTypeface(typeFace);

		btn_customer_otp_verification.setOnClickListener(this);
	}

	void verifyOTP() {
		try {
			inputMethodManager.hideSoftInputFromWindow(et_customer_otp.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String OTP = et_customer_otp.getText().toString();
		if (OTP.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide your OTP!");
		} else {
			String savedOTP = sharedPreferences.getString(PreferenceKeys.REGISTRATION_OTP, null);
			if (OTP.equals(savedOTP)) {
				String PKCustomerID = sharedPreferences.getString(PreferenceKeys.CUSTOMER_TOKEN, null);
				final Call<Register> registerCall = api.registerOTPVerification(
						OTP,
						PKCustomerID
				);
				registerCall.enqueue(new Callback<Register>() {
					@Override
					public void onResponse(Call<Register> call, Response<Register> response) {
						if (response.isSuccessful()) {
							Register responseBody = response.body();
							if (responseBody.ErrorFlag == 0) {
								Customer customer = responseBody.CustomerDetails[0];
								ArrayList<VehicleType> VehicleTypes = new ArrayList<VehicleType>();
								Collections.addAll(VehicleTypes, responseBody.VehicleTypeDetails);
								appCommonUtils.appShowShortToast("OTP Verification Successful");
								customerUtils.customerOTPVerified();
								customerUtils.customerLogin(responseBody.CustomerLoginToken, customer);
							} else {
								appCommonUtils.appShowShortToast("Unable to verify your OTP at this point of time! Please try again later!");
							}
						} else {
							appCommonUtils.appShowShortToast("Error! Please try again!");
						}
					}

					@Override
					public void onFailure(Call<Register> call, Throwable t) {
						appCommonUtils.appShowShortToast("Can't connect to Server! Please check your Network Connection!");
					}
				});
			} else {
				appCommonUtils.appShowShortToast("Provided OTP doesn't match!");
			}
		}
	}
}
