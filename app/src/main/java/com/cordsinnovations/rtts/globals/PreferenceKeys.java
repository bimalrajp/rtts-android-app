package com.cordsinnovations.rtts.globals;

public class PreferenceKeys {

	public static final String APP_ALREADY_OPENED = "appAlreadyOpened";

	public static final String PERMISSION_CAL = "permissionCal";
	public static final String PERMISSION_CAM = "permissionCam";
	public static final String PERMISSION_CNT = "permissionCnt";
	public static final String PERMISSION_LOC = "permissionLoc";
	public static final String PERMISSION_MIC = "permissionMic";
	public static final String PERMISSION_NET = "permissionNet";
	public static final String PERMISSION_SMS = "permissionSms";
	public static final String PERMISSION_STR = "permissionStr";
	public static final String PERMISSION_VBR = "permissionVbr";

	public static final String REGISTRATION_PARTIAL = "registrationPartial";
	public static final String REGISTRATION_COMPLETE = "registrationComplete";

	public static final String REGISTRATION_OTP = "registrationOTP";

	public static final String CUSTOMER_LOGGED_IN = "customerLoggedIn";
	public static final String CUSTOMER_TOKEN = "customerToken";
	public static final String CUSTOMER_FULL_NAME = "customerFullName";
	public static final String CUSTOMER_GENDER = "customerGender";
	public static final String CUSTOMER_MOBILE = "customerMobile";
	public static final String CUSTOMER_EMAIL = "customerEmail";
	public static final String CUSTOMER_EMERGENCY_CONTACT_1 = "customerEmergencyContact1";
	public static final String CUSTOMER_EMERGENCY_CONTACT_2 = "customerEmergencyContact2";
	public static final String CUSTOMER_IMAGE = "customerImage";

	public static final String SETTINGS_MAP_STYLE_ID = "settingsMapStyleID";
	public static final String SETTINGS_MAP_STYLE_POSITION = "settingsMapStylePosition";

	public static final String DIRECTION_VIBRATIONS = "directionVibrations";
	public static final String DIRECTION_VIBRATION_NORTH = "directionVibrationNorth";
	public static final String DIRECTION_VIBRATION_EAST = "directionVibrationEast";
	public static final String DIRECTION_VIBRATION_SOUTH = "directionVibrationSouth";
	public static final String DIRECTION_VIBRATION_WEST = "directionVibrationWest";

}
