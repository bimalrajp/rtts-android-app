package com.cordsinnovations.rtts.interfaces;

import com.cordsinnovations.rtts.api_callbacks.Login;
import com.cordsinnovations.rtts.api_callbacks.RegisterOTP;
import com.cordsinnovations.rtts.api_callbacks.Register;
import com.cordsinnovations.rtts.data_models.map_data_models.MapRootObject;
import com.cordsinnovations.rtts.globals.PreferenceKeys;

import java.io.File;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;
import static com.cordsinnovations.rtts.globals.Constants.LOGIN_USER_TYPE;

public interface API {

	/*@FormUrlEncoded
	@POST("/" + LOGIN_USER_TYPE + "/login")
	void login(
			@Field("customerUsername") String customerUsername,
			@Field("customerPassword") String customerPassword,
			Callback<Login> response
	);

	@Multipart
	@POST("/" + LOGIN_USER_TYPE + "/register")
	Call<RegisterOTP> register(
			@Part("CustomerFullName") String customerFullName,
			@Part("CustomerType") String customerType,
			@Part("CustomerUsername") String customerUsername,
			@Part("CustomerPassword") String customerPassword,
			@Part("CustomerMobileNumber") String customerMobileNo,
			@Part("CustomerDOB") String customerDOB,
			@Part("CustomerCurrentAddress") String customerCurrentAddress,
			@Part("CustomerCurrentPincode") String customerCurrentPincode,
			@Part("CustomerPermanentAddress") String customerPermanentAddress,
			@Part("CustomerPermanentPincode") String customerPermanentPincode,
			@Part("CustomerContactNumber1") String customerContactNumber1,
			@Part("CustomerContactNumber2") String customerContactNumber2,
			@Part("CustomerEmail") String customerEmail,
			@Part("CustomerImage") File customerImage
	);*/

	@GET
	Call<MapRootObject> getMapDirections(
			@Url String url
	);

	@FormUrlEncoded
	@POST("Login.php")
	Call<Login> login(
			@Field("CustomerUsername") String customerUsername,
			@Field("CustomerPassword") String customerPassword
	);

	@Multipart
	@POST("Register.php")
	Call<RegisterOTP> register(
			@Part("CustomerFullName") String customerFullName,
			@Part("CustomerType") String customerType,
			@Part("CustomerUsername") String customerUsername,
			@Part("CustomerPassword") String customerPassword,
			@Part("CustomerMobileNumber") String customerMobileNo,
			@Part("CustomerDOB") String customerDOB,
			@Part("CustomerGender") String customerGender,
			@Part("CustomerCurrentAddress") String customerCurrentAddress,
			@Part("CustomerCurrentPincode") String customerCurrentPincode,
			@Part("CustomerPermanentAddress") String customerPermanentAddress,
			@Part("CustomerPermanentPincode") String customerPermanentPincode,
			@Part("CustomerContactNumber1") String customerContactNumber1,
			@Part("CustomerContactNumber2") String customerContactNumber2,
			@Part("CustomerEmail") String customerEmail,
			@Part("CustomerImage") File customerImage
	);

	@FormUrlEncoded
	@POST("OTPVerification.php")
	Call<Register> registerOTPVerification(
			@Field("OTP") String customerOTP,
			@Field("PKCustomerID") String customerID
	);
}
