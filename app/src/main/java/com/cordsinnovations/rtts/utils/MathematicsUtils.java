package com.cordsinnovations.rtts.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;

/**
 * Created by Bimal on 14-09-2018.
 */

public class MathematicsUtils {

	static class UTIL_COMMONS {
		@SuppressLint("StaticFieldLeak")
		static Activity activity;
		@SuppressLint("StaticFieldLeak")
		static Context context;
		static Handler handler;
	}

	public MathematicsUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		UTIL_COMMONS.handler = new Handler();
	}

	public int absoluteValue(float floatValue){
		return (int) Math.abs(floatValue);
	}

	public int roundFloat(float floatValue) {
		return Math.round(floatValue);
	}
}