package com.cordsinnovations.rtts;

import android.app.Application;
import android.content.SharedPreferences;

import com.cordsinnovations.rtts.interfaces.API;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.cordsinnovations.rtts.globals.Constants.HTTP.*;

public class RightToTravelSafe extends Application {

	public static API api, mapsApi;
	public static SharedPreferences sharedPreferences;
	public static CalligraphyConfig calligraphyConfig;

	@Override
	public void onCreate() {
		super.onCreate();

		calligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				.setDefaultFontPath("fonts/neo_sans_medium_italic.otf")
				.setFontAttrId(R.attr.fontPath)
				.build()
		);

		sharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

		Retrofit commonAdapter = new Retrofit.Builder()
				.baseUrl(BASE_URL)
				.addConverterFactory(ScalarsConverterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.build();

		api = commonAdapter.create(API.class);

		Retrofit mapsAdapter = new Retrofit.Builder()
				.baseUrl(MAPS_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.build();

		mapsApi = mapsAdapter.create(API.class);
	}
}
