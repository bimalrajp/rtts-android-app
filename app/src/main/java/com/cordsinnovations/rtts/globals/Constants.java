package com.cordsinnovations.rtts.globals;

import android.os.Environment;

import com.cordsinnovations.rtts.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.File;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.SETTINGS_MENU_DESCRIPTION_ELEMENTS.*;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.SETTINGS_MENU_TITLE_ELEMENTS.*;

public class Constants {

	public static final String APPLICATION_TAG = "Right To Travel Safe";

	public static final String LOGIN_USER_TYPE = "customer";

	//public static final LatLngBounds INDIA_LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-33.880490, 151.184363), new LatLng(-33.858754, 151.229596));

	public static final class HTTP {
		//public static final String COMPANY_URL = "http://colonialvision.com";
		//public static final String BASE_URL = "http://colonialvision.com/";
		//public static final String BASE_URL = "http://192.168.1.34/androidClass/";
		public static final String BASE_URL = "http://jewellgroup.in/rtts/";
		public static final String MAPS_URL = "https://maps.googleapis.com/maps/api/directions/";
		//public static final String PRIVACY_POLICY_URL = "http://192.168.1.33/rtts/termsandconditions";
	}

	public static final class APPLICATION_CONSTANTS {

		public static final class DEVICE_VIBRATION_CONSTANTS {
			public static final int VIBRATION_SHORT_MILLI_SECONDS = 100;
			public static final int VIBRATION_LONG_MILLI_SECONDS = 300;
			public static final int VIBRATION_PAUSE_MILLI_SECONDS = 100;
		}

		public static final class DEVICE_ROTATION_CONSTANTS {

			public static class EXACT_DIRECTION_ANGLES {
				public static final int DIRECTION_NORTH = 0;
				public static final int DIRECTION_EAST = 90;
				public static final int DIRECTION_SOUTH = 180;
				public static final int DIRECTION_WEST = 270;
			}

			public static class EXACT_DIRECTION_NAMES {
				public static final String DIRECTION_NORTH = " North ";
				public static final String DIRECTION_EAST = " East ";
				public static final String DIRECTION_SOUTH = " South ";
				public static final String DIRECTION_WEST = " West ";
			}

			public static class EXACT_DIRECTION_NOTATIONS {
				public static final String DIRECTION_NORTH = " N ";
				public static final String DIRECTION_EAST = " E ";
				public static final String DIRECTION_SOUTH = " S ";
				public static final String DIRECTION_WEST = " W ";
			}
		}

		public static final class DEVICE_SHAKE_CONSTANTS {
			public static final int SHAKE_TIMEOUT_SECONDS = 10;

			public static class DEVICE_SENSOR_CONSTANTS {
				public static final String SENSOR_TAG = "ShakeDetector";
				public static final int FORCE_THRESHOLD = 550;
				public static final int TIME_THRESHOLD = 100;
				public static final int SHAKE_TIMEOUT = 500;
				public static final int SHAKE_DURATION = 500;
				public static final int SHAKE_COUNT = 1;
			}
		}

		public static final class SETTINGS_MENU_TITLE_ELEMENTS {
			public static final String SETTINGS_MENU_TITLE_1 = " Profile ";
			public static final String SETTINGS_MENU_TITLE_2 = " Map Style ";
		}

		static final class SETTINGS_MENU_DESCRIPTION_ELEMENTS {
			static final String SETTINGS_MENU_DESCRIPTION_1 = " View and update your profile details. ";
			static final String SETTINGS_MENU_DESCRIPTION_2 = " Choose a map styles for better comfort and convenience. ";
		}

		public static final String[] SETTINGS_MENU_TITLES_ARRAY = {SETTINGS_MENU_TITLE_1, SETTINGS_MENU_TITLE_2};

		public static final String[] SETTINGS_MENU_DESCRIPTIONS_ARRAY = {SETTINGS_MENU_DESCRIPTION_1, SETTINGS_MENU_DESCRIPTION_2};
		public static final String[] MAP_STYLE_TITLES_ARRAY = {"Default", "Standard", "Silver", "Retro", "Dark", "Night", "Aubergine"};

		public static final int[] MAP_STYLE_IMAGES_ARRAY = {R.drawable.map_style_01, R.drawable.map_style_02, R.drawable.map_style_03, R.drawable.map_style_04, R.drawable.map_style_05, R.drawable.map_style_06, R.drawable.map_style_07};
		public static final int[] MAP_STYLE_IDS_ARRAY = {R.raw.map_style_01, R.raw.map_style_02, R.raw.map_style_03, R.raw.map_style_04, R.raw.map_style_05, R.raw.map_style_06, R.raw.map_style_07};
	}

	public static final class CUSTOMER_CONSTANTS {
		public static final String[] CUSTOMER_TYPES = {"Regular", "Disabled"};
		public static final String[] CUSTOMER_GENDERS = {"Male", "Female"};
	}

	public static final class PUT_EXTRA_KEYS {
		public static final class DOWNLOAD_FILE_PUT_EXTRA_KEYS {
			public static final String PUT_EXTRA_KEY_DOWNLOAD_FILE_NAME = "keyDownloadFileName";
			public static final String PUT_EXTRA_KEY_DOWNLOAD_FILE_TYPE = "keyDownloadFileType";
		}
	}

	public static final class FIREBASE {
		//public static final String TOPIC = "/topics/RightToTravelSafePublic";
		//public static final String TOPIC = "/topics/RightToTravelSafeTest";
		public static final String TOPIC = "/topics/RightToTravelSafeLocalTest";
	}

	public static final class PERMISSION_REQUESTS {
		public static final int REQUEST_ALL_PERMISSIONS = 0;
		public static final int REQUEST_CALL_PERMISSION = 1;
		public static final int REQUEST_CONTACT_PERMISSIONS = 2;
		public static final int REQUEST_CAMERA_PERMISSION = 3;
		public static final int REQUEST_LOCATION_PERMISSIONS = 4;
		public static final int REQUEST_MICROPHONE_PERMISSION = 5;
		public static final int REQUEST_NETWORK_STATE_PERMISSION = 6;
		public static final int REQUEST_SMS_PERMISSION = 7;
		public static final int REQUEST_STORAGE_PERMISSIONS = 8;
		public static final int REQUEST_VIBRATE_PERMISSIONS = 9;

		public static final int REQUEST_CAMERA_AND_STORAGE_PERMISSIONS = 10;
		public static final int REQUEST_VIDEO_AND_STORAGE_PERMISSIONS = 11;
		public static final int REQUEST_LOCATION_AND_NETWORK_STATE_PERMISSIONS = 12;
		public static final int REQUEST_CALL_AND_LOCATION_AND_NETWORK_STATE_PERMISSIONS = 13;
	}

	public static final class FOLDERS_ONLINE {
		public static final String FOLDER_IMAGE_CUSTOMER = "assets/uploads/images/customer/";
		public static final String FOLDER_IMAGE_PROOF = "assets/uploads/images/proof/";
		public static final String FOLDER_IMAGE_SOS = "assets/uploads/images/sos/";
		public static final String FOLDER_IMAGE_ACCIDENT = "assets/uploads/images/accident/";
		public static final String FOLDER_IMAGE_BREAKDOWN = "assets/uploads/images/breakdown/";
		public static final String FOLDER_IMAGE_OTHER = "assets/uploads/images/other/";
		public static final String FOLDER_DOCUMENT_PROOF = "assets/uploads/documents/proof/";
		public static final String FOLDER_DOCUMENT_OTHER = "assets/uploads/documents/other/";
	}

	public static final class FOLDER_PATHS_APP {
		public static final String PATH_RTTS_CACHE = "RTTS/.cache/";
		public static final String PATH_IMAGE_CUSTOMER = "RTTS/images/customer/";
		public static final String PATH_IMAGE_PROOF = "RTTS/images/proof/";
		public static final String PATH_IMAGE_SOS = "RTTS/images/sos/";
		public static final String PATH_IMAGE_ACCIDENT = "RTTS/images/accident/";
		public static final String PATH_IMAGE_BREAKDOWN = "RTTS/images/breakdown/";
		public static final String PATH_IMAGE_OTHER = "RTTS/images/other/";
		public static final String PATH_DOCUMENT_PROOF = "RTTS/documents/proof/";
		public static final String PATH_DOCUMENT_OTHER = "RTTS/documents/other/";
		//public static final String PATH_VIDEO_THUMBNAILS = "RTTS/images/videos/.thumbnails/";
	}

	public static final class FILES_APP {
		public static final File FOLDER_ROOT = Environment.getExternalStorageDirectory();
		public static final File FOLDER_RTTS_CACHE = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_RTTS_CACHE);
		public static final File FOLDER_IMAGE_CUSTOMER = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_CUSTOMER);
		public static final File FOLDER_IMAGE_PROOF = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_PROOF);
		public static final File FOLDER_IMAGE_SOS = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_SOS);
		public static final File FOLDER_IMAGE_ACCIDENT = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_ACCIDENT);
		public static final File FOLDER_IMAGE_BREAKDOWN = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_BREAKDOWN);
		public static final File FOLDER_IMAGE_OTHER = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_IMAGE_OTHER);
		public static final File FOLDER_DOCUMENT_PROOF = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_DOCUMENT_PROOF);
		public static final File FOLDER_DOCUMENT_OTHER = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_DOCUMENT_OTHER);
		//public static final File FOLDER_VIDEO_THUMBNAILS = new File(FOLDER_ROOT, FOLDER_PATHS_APP.PATH_VIDEO_THUMBNAILS);
	}

	public static final class FILE_DOWNLOAD_TYPES {
		public static final int FILE_DOWNLOAD_IMAGE_CUSTOMER = 1;
		public static final int FILE_DOWNLOAD_IMAGE_PROOF = 2;
		public static final int FILE_DOWNLOAD_IMAGE_SOS = 3;
		public static final int FILE_DOWNLOAD_IMAGE_ACCIDENT = 4;
		public static final int FILE_DOWNLOAD_IMAGE_BREAKDOWN = 5;
		public static final int FILE_DOWNLOAD_IMAGE_OTHER = 6;
		public static final int FILE_DOWNLOAD_DOCUMENT_PROOF = 7;
		public static final int FILE_DOWNLOAD_DOCUMENT_OTHER = 8;
	}

	public static final class FILE_REQUEST_TYPES {
		public static final int FILE_REQUEST_IMAGE = 1;
		public static final int FILE_REQUEST_VIDEO = 2;
		public static final int FILE_REQUEST_AUDIO = 3;
		public static final int FILE_REQUEST_ATTACH = 4;
	}

	public static final class FILE_TYPES {
		public static final String FILE_IMAGE = "image/*";
		public static final String FILE_IMAGE_JPEG = "image/jpeg";
		public static final String FILE_IMAGE_JPG = "image/jpg";
		public static final String FILE_IMAGE_PNG = "image/png";
		public static final String FILE_VIDEO = "video/*";
		public static final String FILE_VIDEO_MP4 = "video/mp4";
		public static final String FILE_VIDEO_3GP = "video/3gp";
		public static final String FILE_VIDEO_3GPP = "video/3gpp";
		public static final String FILE_VIDEO_AVI = "video/avi";
		public static final String FILE_AUDIO = "audio/*";
		public static final String FILE_AUDIO_MPEG = "audio/mpeg";
		public static final String FILE_AUDIO_3GP = "audio/3gp";
		public static final String FILE_AUDIO_AMR = "audio/amr";
		public static final String FILE_AUDIO_WAV = "audio/x-wav";
		public static final String FILE_AUDIO_OGG = "application/ogg";
		public static final String FILE_PDF = "application/pdf";
		public static final String FILE_DOC = "application/msword";
		public static final String FILE_XLS = "application/vnd.ms-excel";
		public static final String FILE_PPT = "application/vnd.ms-powerpoint";
	}

	public static final class FILE_SIZES {
		public static final int ONE_KB = 1024;
		public static final int ONE_MB = 1024 * 1024;
		public static final float FILE_IMAGE_SIZE = 5;
		public static final float FILE_VIDEO_SIZE = 15;
		public static final float FILE_AUDIO_SIZE = 5;
		public static final float FILE_ATTACH_SIZE = 5;
	}

	public static final class FILE_OPERATIONS {
		public static final int ACTIVITY_CAPTURE_IMAGE = 1;
		public static final int ACTIVITY_CHOOSE_IMAGE = 2;
		public static final int ACTIVITY_CAPTURE_VIDEO = 3;
		public static final int ACTIVITY_CHOOSE_VIDEO = 4;
		public static final int ACTIVITY_CAPTURE_AUDIO = 5;
		public static final int ACTIVITY_CHOOSE_AUDIO = 6;
		public static final int ACTIVITY_CHOOSE_FILE = 7;
	}

	public static final class ADS {
		public static final String AD_SPONSOR = "https://www.facebook.com/ESAFMicrofin/";
		public static final int ADS_IMAGE_COUNT = 3;
		public static final int[] ADS_IDS = {R.drawable.ad_example_1, R.drawable.ad_example_2, R.drawable.ad_example_3};
		public static final String[] AD_URLS = {"http://colonialvision.com", "http://fruitomans.com/", "http://www.bednbeyond.in/"};
	}

	public static final class MAP_PARAMETERS {
		public static final LatLngBounds INDIA_LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));
		public static final String MY_LOCATION = "My Location";
		public static final float DEFAULT_MAP_CAMERA_ZOOM = 18f;
		public static final int DEFAULT_MAP_CAMERA_ZOOM_DURATION = 2000;
		public static final int DEFAULT_MAP_ROUTE_PADDING = 250;
	}

	public static final class MAP_SEARCH_REQUESTS {
		public static final int REQUEST_CURRENT_LOCATION = 0;
		public static final int REQUEST_SOURCE_CURRENT_LOCATION = 1;
		public static final int REQUEST_DESTINATION_CURRENT_LOCATION = 2;
		public static final int REQUEST_SOURCE_LOCATION = 3;
		public static final int REQUEST_DESTINATION_LOCATION = 4;
		public static final int REQUEST_BOTH_LOCATIONS = 5;
	}

	public static final class MAP_MARKER_TYPES {
		public static final int MARKER_CURRENT_LOCATION = 0;
		public static final int MARKER_SOURCE_LOCATION = 1;
		public static final int MARKER_DESTINATION_LOCATION = 2;
		public static final int MARKER_TAXI_LOCATION = 3;
	}

	public static final class MAP_MARKER_OPERATION_TYPES {
		public static final int MARKER_REMOVE_OPERATION = 0;
		public static final int MARKER_ADD_OPERATION = 1;
	}

	public static final class MAP_POLYLINE_OPERATION_TYPES {
		public static final int POLYLINE_REMOVE_OPERATION = 0;
		public static final int POLYLINE_ADD_OPERATION = 1;
	}
}
