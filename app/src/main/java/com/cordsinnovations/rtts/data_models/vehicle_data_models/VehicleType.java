package com.cordsinnovations.rtts.data_models.vehicle_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleType implements Parcelable {

	private int PKVehicleTypeID;
	private String VehicleTypeName;
	private double VehicleTypeRate;
	private double VehicleTypeWaitingCharge;
	private int VehicleTypeSeatingCapacity;
	private String VehicleTypeDescription;
	private int VehicleTypeImage;
	private int VehicleTypeStatus;
	private String VehicleTypeCreatedTimestamp;
	private String VehicleTypeUpdatedTimestamp;
	private int VehicleTypeActiveFlag;
	private boolean VehicleTypeSelected;

	public VehicleType(Parcel in) {
		PKVehicleTypeID = in.readInt();
		VehicleTypeName = in.readString();
		VehicleTypeRate = in.readDouble();
		VehicleTypeWaitingCharge = in.readDouble();
		VehicleTypeSeatingCapacity = in.readInt();
		VehicleTypeDescription = in.readString();
		VehicleTypeImage = in.readInt();
		VehicleTypeStatus = in.readInt();
		VehicleTypeCreatedTimestamp = in.readString();
		VehicleTypeUpdatedTimestamp = in.readString();
		VehicleTypeActiveFlag = in.readInt();
	}

	public static final Creator<VehicleType> CREATOR = new Creator<VehicleType>() {
		@Override
		public VehicleType createFromParcel(Parcel in) {
			return new VehicleType(in);
		}

		@Override
		public VehicleType[] newArray(int size) {
			return new VehicleType[size];
		}
	};

	public VehicleType(int PKVehicleTypeID, String vehicleTypeName, double vehicleTypeRate, double vehicleTypeWaitingCharge, int vehicleTypeSeatingCapacity, String vehicleTypeDescription, int vehicleTypeImage, int vehicleTypeStatus, String vehicleTypeCreatedTimestamp, String vehicleTypeUpdatedTimestamp, int vehicleTypeActiveFlag) {
		this.PKVehicleTypeID = PKVehicleTypeID;
		VehicleTypeName = vehicleTypeName;
		VehicleTypeRate = vehicleTypeRate;
		VehicleTypeWaitingCharge = vehicleTypeWaitingCharge;
		VehicleTypeSeatingCapacity = vehicleTypeSeatingCapacity;
		VehicleTypeDescription = vehicleTypeDescription;
		VehicleTypeImage = vehicleTypeImage;
		VehicleTypeStatus = vehicleTypeStatus;
		VehicleTypeCreatedTimestamp = vehicleTypeCreatedTimestamp;
		VehicleTypeUpdatedTimestamp = vehicleTypeUpdatedTimestamp;
		VehicleTypeActiveFlag = vehicleTypeActiveFlag;
		VehicleTypeSelected = false;
	}

	public int getPKVehicleTypeID() {
		return PKVehicleTypeID;
	}
	public void setPKVehicleTypeID(int PKVehicleTypeID) {
		this.PKVehicleTypeID = PKVehicleTypeID;
	}
	public String getVehicleTypeName() {
		return VehicleTypeName;
	}
	public void setVehicleTypeName(String vehicleTypeName) {
		VehicleTypeName = vehicleTypeName;
	}
	public double getVehicleTypeRate() {
		return VehicleTypeRate;
	}
	public void setVehicleTypeRate(double vehicleTypeRate) {
		VehicleTypeRate = vehicleTypeRate;
	}
	public double getVehicleTypeWaitingCharge() {
		return VehicleTypeWaitingCharge;
	}
	public void setVehicleTypeWaitingCharge(double vehicleTypeWaitingCharge) {
		VehicleTypeWaitingCharge = vehicleTypeWaitingCharge;
	}
	public int getVehicleTypeSeatingCapacity() {
		return VehicleTypeSeatingCapacity;
	}
	public void setVehicleTypeSeatingCapacity(int vehicleTypeSeatingCapacity) {
		VehicleTypeSeatingCapacity = vehicleTypeSeatingCapacity;
	}
	public String getVehicleTypeDescription() {
		return VehicleTypeDescription;
	}
	public void setVehicleTypeDescription(String vehicleTypeDescription) {
		VehicleTypeDescription = vehicleTypeDescription;
	}
	public int getVehicleTypeImage() {
		return VehicleTypeImage;
	}
	public void setVehicleTypeImage(int vehicleTypeImage) {
		VehicleTypeImage = vehicleTypeImage;
	}
	public int getVehicleTypeStatus() {
		return VehicleTypeStatus;
	}
	public void setVehicleTypeStatus(int vehicleTypeStatus) {
		VehicleTypeStatus = vehicleTypeStatus;
	}
	public String getVehicleTypeCreatedTimestamp() {
		return VehicleTypeCreatedTimestamp;
	}
	public void setVehicleTypeCreatedTimestamp(String vehicleTypeCreatedTimestamp) {
		VehicleTypeCreatedTimestamp = vehicleTypeCreatedTimestamp;
	}
	public String getVehicleTypeUpdatedTimestamp() {
		return VehicleTypeUpdatedTimestamp;
	}
	public void setVehicleTypeUpdatedTimestamp(String vehicleTypeUpdatedTimestamp) {
		VehicleTypeUpdatedTimestamp = vehicleTypeUpdatedTimestamp;
	}
	public int getVehicleTypeActiveFlag() {
		return VehicleTypeActiveFlag;
	}
	public void setVehicleTypeActiveFlag(int vehicleTypeActiveFlag) {
		VehicleTypeActiveFlag = vehicleTypeActiveFlag;
	}
	public boolean getVehicleTypeSelected() {
		return VehicleTypeSelected;
	}
	public void setVehicleTypeSelected(boolean vehicleTypeSelected) {
		VehicleTypeSelected = vehicleTypeSelected;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(PKVehicleTypeID);
		parcel.writeString(VehicleTypeName);
		parcel.writeDouble(VehicleTypeRate);
		parcel.writeDouble(VehicleTypeWaitingCharge);
		parcel.writeInt(VehicleTypeSeatingCapacity);
		parcel.writeString(VehicleTypeDescription);
		parcel.writeInt(VehicleTypeImage);
		parcel.writeInt(VehicleTypeStatus);
		parcel.writeString(VehicleTypeCreatedTimestamp);
		parcel.writeString(VehicleTypeUpdatedTimestamp);
		parcel.writeInt(VehicleTypeActiveFlag);
	}
}
