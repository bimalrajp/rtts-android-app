package com.cordsinnovations.rtts.adapters.ui_adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.adapters.view_holders.VehicleTypeViewHolder;
import com.cordsinnovations.rtts.data_models.vehicle_data_models.VehicleType;

import java.util.ArrayList;

public class VehicleTypeAdapter extends RecyclerView.Adapter<VehicleTypeViewHolder> {

	private Context context;
	private ArrayList<VehicleType> vehicleTypeList;

	private OnItemClickListener onItemClickListener;

	public interface OnItemClickListener {
		void onItemClick(int position);
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	public VehicleTypeAdapter(Context context, ArrayList<VehicleType> vehicleTypeList) {
		this.context = context;
		this.vehicleTypeList = vehicleTypeList;
	}

	@NonNull
	@Override
	public VehicleTypeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		//View vehicleTypeView = layoutInflater.inflate(R.layout.layout_vehicle_type, viewGroup, false);
		View vehicleTypeView = layoutInflater.inflate(R.layout.layout_vehicle_type, null);
		return new VehicleTypeViewHolder(vehicleTypeView, onItemClickListener);
	}

	@Override
	public void onBindViewHolder(@NonNull final VehicleTypeViewHolder vehicleTypeViewHolder, final int position) {
		VehicleType vehicleType = vehicleTypeList.get(position);

		vehicleTypeViewHolder.tv_seating_capacity.setText(String.valueOf(vehicleType.getVehicleTypeSeatingCapacity()));
		vehicleTypeViewHolder.tv_vehicle_rate.setText(String.valueOf(vehicleType.getVehicleTypeRate()));
		vehicleTypeViewHolder.tv_waiting_charge.setText(String.valueOf(vehicleType.getVehicleTypeWaitingCharge()));

		vehicleTypeViewHolder.VehicleTypeName.setText(vehicleType.getVehicleTypeName());
		vehicleTypeViewHolder.VehicleTypeImage.setImageDrawable(context.getResources().getDrawable(vehicleType.getVehicleTypeImage()));
		if (vehicleType.getVehicleTypeSelected()) {
			vehicleTypeViewHolder.cv_vehicle_type_details.setVisibility(View.VISIBLE);
			vehicleTypeViewHolder.cv_vehicle_type.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
		} else {
			vehicleTypeViewHolder.cv_vehicle_type_details.setVisibility(View.GONE);
			vehicleTypeViewHolder.cv_vehicle_type.setCardBackgroundColor(context.getResources().getColor(R.color.colorHalfTransparent));
		}
	}

	@Override
	public int getItemCount() {
		return vehicleTypeList.size();
	}
}
