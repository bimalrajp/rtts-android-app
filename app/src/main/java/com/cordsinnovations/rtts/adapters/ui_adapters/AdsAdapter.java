package com.cordsinnovations.rtts.adapters.ui_adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.globals.Constants;

/**
 * Created by Bimal on 25-02-2017.
 */

public class AdsAdapter extends PagerAdapter {

	private int[] ads = Constants.ADS.ADS_IDS;
	private Context context;
	private LayoutInflater layoutInflater;
	public static final int IMG_COUNT = Constants.ADS.ADS_IMAGE_COUNT;

	public AdsAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return ads.length;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = layoutInflater.inflate(R.layout.layout_ads, container, false);
		ImageView adImage = itemView.findViewById(R.id.img_ad);
		adImage.setImageResource(ads[position]);
		adImage.setOnClickListener(v -> {
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(Constants.ADS.AD_URLS[position]));
			context.startActivity(i);
		});
		container.addView(itemView);
		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}
}