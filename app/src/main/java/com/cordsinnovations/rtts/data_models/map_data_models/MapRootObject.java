package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MapRootObject implements Parcelable {

	private GeocodedWaypoint[] geocoded_waypoints;
	private Route[] routes;
	private String status;

	protected MapRootObject(Parcel in) {
		geocoded_waypoints = in.createTypedArray(GeocodedWaypoint.CREATOR);
		routes = in.createTypedArray(Route.CREATOR);
		status = in.readString();
	}

	public static final Creator<MapRootObject> CREATOR = new Creator<MapRootObject>() {
		@Override
		public MapRootObject createFromParcel(Parcel in) {
			return new MapRootObject(in);
		}

		@Override
		public MapRootObject[] newArray(int size) {
			return new MapRootObject[size];
		}
	};

	public GeocodedWaypoint[] getGeocoded_waypoints() {
		return geocoded_waypoints;
	}
	public void setGeocoded_waypoints(GeocodedWaypoint[] geocoded_waypoints) {
		this.geocoded_waypoints = geocoded_waypoints;
	}
	public Route[] getRoutes() {
		return routes;
	}
	public void setRoutes(Route[] routes) {
		this.routes = routes;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeTypedArray(geocoded_waypoints, i);
		parcel.writeTypedArray(routes, i);
		parcel.writeString(status);
	}
}
