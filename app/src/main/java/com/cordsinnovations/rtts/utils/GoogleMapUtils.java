package com.cordsinnovations.rtts.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.cordsinnovations.rtts.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.cordsinnovations.rtts.globals.Constants.MAP_MARKER_OPERATION_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_MARKER_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_PARAMETERS.*;

import static com.cordsinnovations.rtts.utils.GoogleMapUtils.UTIL_COMMONS.*;
import static com.cordsinnovations.rtts.utils.GoogleMapUtils.OTHER_UTILS.*;
import static com.cordsinnovations.rtts.utils.GoogleMapUtils.UTIL_BOOLEANS.*;
import static com.cordsinnovations.rtts.utils.GoogleMapUtils.UTIL_CONSTANTS.*;
import static com.cordsinnovations.rtts.utils.GoogleMapUtils.UTIL_VARIABLES.*;

public class GoogleMapUtils {

	static class UTIL_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
	}

	static class OTHER_UTILS {
		static AppCommonUtils appCommonUtils;
	}

	static class UTIL_BOOLEANS {
		static boolean isMarkerRotating;
	}

	static class UTIL_CONSTANTS {
		static double PI = 3.14159;
	}

	static class UTIL_VARIABLES {
		static double bearing;
	}

	public GoogleMapUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		UTIL_COMMONS.handler = new Handler();
		appCommonUtils = new AppCommonUtils(activity, context);
	}

	public PolylineOptions drawPolyLine(List<List<LatLng>> lists) {
		ArrayList points = null;
		PolylineOptions polylineOptions = null;

		for (List<LatLng> path : lists) {
			points = new ArrayList();
			polylineOptions = new PolylineOptions();
			for (LatLng latLng : path) {
				points.add(latLng);
			}

			polylineOptions.addAll(points);
			polylineOptions.width(15);
			polylineOptions.color(context.getResources().getColor(R.color.colorPrimary));
			polylineOptions.geodesic(true);
		}

		if (polylineOptions == null) {
			appCommonUtils.appShowShortToast("Direction Not Found!");
		}
		return polylineOptions;
	}

	public void mapZoom(GoogleMap googleMap, boolean zoom) {
		if (zoom) {
			googleMap.animateCamera(CameraUpdateFactory.zoomIn());
		} else {
			googleMap.animateCamera(CameraUpdateFactory.zoomOut());
		}
	}

	public void clearMap(GoogleMap googleMap) {
		googleMap.clear();
	}

	private void markerOperations(int markerOperationType, int markerType, boolean zoom, GoogleMap googleMap, Marker marker, MarkerOptions markerOptions, LatLng latLng, LatLngBounds latLngBounds, CameraUpdate cameraUpdate, double latitude, double longitude) {
		switch (markerOperationType) {
			case MARKER_ADD_OPERATION:
				switch (markerType) {
					case MARKER_CURRENT_LOCATION:
						if (marker != null) {
							marker.remove();
							marker = null;
						}
						marker = googleMap.addMarker(markerOptions);
						if (zoom) {
							googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_MAP_CAMERA_ZOOM), DEFAULT_MAP_CAMERA_ZOOM_DURATION, null);
						}
						break;
					case MARKER_SOURCE_LOCATION:
						marker = googleMap.addMarker(markerOptions);
						break;
					case MARKER_DESTINATION_LOCATION:
						marker = googleMap.addMarker(markerOptions);
						break;
					case MARKER_TAXI_LOCATION:
						break;
				}
				break;
			case MARKER_REMOVE_OPERATION:
				switch (markerType) {
					case MARKER_CURRENT_LOCATION:
						if (marker != null) {
							marker.remove();
							marker = null;
						}
						markerOptions = null;
						if (latLngBounds != null) {
							cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds, DEFAULT_MAP_ROUTE_PADDING);
							googleMap.animateCamera(cameraUpdate);
						}
						break;
					case MARKER_SOURCE_LOCATION:
						latLng = null;
						latitude = 0;
						longitude = 0;
						if (marker != null) {
							marker.remove();
							marker = null;
						}
						markerOptions = null;
						break;
					case MARKER_DESTINATION_LOCATION:
						latLng = null;
						latitude = 0;
						longitude = 0;
						if (marker != null) {
							marker.remove();
							marker = null;
						}
						markerOptions = null;
						break;
					case MARKER_TAXI_LOCATION:
						break;
				}
				break;
		}
	}

	public double bearingBetweenLocations(LatLng sourceLatLng, LatLng destinationLatLng) {

		double sourceLatitude = sourceLatLng.latitude * PI / 180;
		double sourceLongitude = sourceLatLng.longitude * PI / 180;
		double destinationLatitude = destinationLatLng.latitude * PI / 180;
		double destinationLongitude = destinationLatLng.longitude * PI / 180;

		double longitudeDifference = (destinationLongitude - sourceLongitude);

		double Y = Math.sin(longitudeDifference) * Math.cos(destinationLatitude);
		double X = Math.cos(sourceLatitude) * Math.sin(destinationLatitude) - Math.sin(sourceLatitude) * Math.cos(destinationLatitude) * Math.cos(longitudeDifference);

		bearing = Math.atan2(Y, X);

		bearing = Math.toDegrees(bearing);
		bearing = (bearing + 360) % 360;

		return bearing;
	}

	public void rotateMarker(final Marker marker, final float toRotation) {
		if (!isMarkerRotating) {
			handler = new Handler();
			final long start = SystemClock.uptimeMillis();
			final float startRotation = marker.getRotation();
			final long duration = 50;

			final Interpolator interpolator = new LinearInterpolator();

			handler.post(new Runnable() {
				@Override
				public void run() {
					isMarkerRotating = true;

					long elapsed = SystemClock.uptimeMillis() - start;
					float t = interpolator.getInterpolation((float) elapsed / duration);

					float rot = t * toRotation + (1 - t) * startRotation;

					marker.setRotation(-rot > 180 ? rot / 2 : rot);
					if (t < 1.0) {
						// Post again 16ms later.
						handler.postDelayed(this, 16);
					} else {
						isMarkerRotating = false;
					}
				}
			});
		}
	}

	public void updateCamera(GoogleMap googleMap, LatLng targetLatLng, float angleAzimuth, float tilt) {
		CameraPosition currentPlace = new CameraPosition.Builder().target(targetLatLng).bearing(angleAzimuth).tilt(tilt).zoom(DEFAULT_MAP_CAMERA_ZOOM).build();
		googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace));
	}

	public static void startMarkerAnimation(GoogleMap myMap, final List<LatLng> directionPoint, final Bitmap bitmap) {
		Marker marker = myMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(bitmap)).position(directionPoint.get(0)).flat(true));
		myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(0), 10));
		animateMarker(myMap, marker, directionPoint, false);
	}

	private static void animateMarker(GoogleMap myMap, final Marker marker, final List<LatLng> directionPoint, final boolean hideMarker) {
		handler = new Handler();
		final long start = SystemClock.uptimeMillis();
		Projection proj = myMap.getProjection();
		final long duration = 30000;

		final Interpolator interpolator = new LinearInterpolator();

		handler.post(new Runnable() {
			int i = 0;

			@Override
			public void run() {
				long elapsed = SystemClock.uptimeMillis() - start;
				float t = interpolator.getInterpolation((float) elapsed / duration);

				if (i < directionPoint.size()) {
					marker.setPosition(directionPoint.get(i));
				}
				i++;


				if (t < 1.0) {
					// Post again 16ms later.
					handler.postDelayed(this, 16);
				} else {
					if (hideMarker) {
						marker.setVisible(false);
					} else {
						marker.setVisible(true);
					}
				}
			}
		});
	}

}
