package com.cordsinnovations.rtts.api_callbacks;

import android.os.Parcel;
import android.os.Parcelable;

public class Customer implements Parcelable {

	public String CustomerFullName;
	public int CustomerType;
	public String CustomerDOB;
	public String CustomerGender;
	public String CustomerMobileNumber;
	public String CustomerCurrentAddress;
	public String CustomerCurrentPincode;
	public String CustomerContactNumber1;
	public String CustomerContactNumber2;
	public String CustomerEmail;
	public String CustomerImage;
	public int CustomerStatus;
	public int CustomerActiveFlag;

	public Customer(String customerFullName, int customerType, String customerDOB, String customerGender, String customerMobileNumber, String customerCurrentAddress, String customerCurrentPincode, String customerContactNumber1, String customerContactNumber2, String customerEmail, String customerImage, int customerStatus, int customerActiveFlag) {
		CustomerFullName = customerFullName;
		CustomerType = customerType;
		CustomerDOB = customerDOB;
		CustomerGender = customerGender;
		CustomerMobileNumber = customerMobileNumber;
		CustomerCurrentAddress = customerCurrentAddress;
		CustomerCurrentPincode = customerCurrentPincode;
		CustomerContactNumber1 = customerContactNumber1;
		CustomerContactNumber2 = customerContactNumber2;
		CustomerEmail = customerEmail;
		CustomerImage = customerImage;
		CustomerStatus = customerStatus;
		CustomerActiveFlag = customerActiveFlag;
	}

	protected Customer(Parcel in) {
		CustomerFullName = in.readString();
		CustomerType = in.readInt();
		CustomerDOB = in.readString();
		CustomerGender = in.readString();
		CustomerMobileNumber = in.readString();
		CustomerCurrentAddress = in.readString();
		CustomerCurrentPincode = in.readString();
		CustomerContactNumber1 = in.readString();
		CustomerContactNumber2 = in.readString();
		CustomerEmail = in.readString();
		CustomerImage = in.readString();
		CustomerStatus = in.readInt();
		CustomerActiveFlag = in.readInt();
	}

	public static final Creator<Customer> CREATOR = new Creator<Customer>() {
		@Override
		public Customer createFromParcel(Parcel in) {
			return new Customer(in);
		}

		@Override
		public Customer[] newArray(int size) {
			return new Customer[size];
		}
	};

	public String getCustomerFullName() {
		return CustomerFullName;
	}

	public void setCustomerFullName(String customerFullName) {
		CustomerFullName = customerFullName;
	}

	public int getCustomerType() {
		return CustomerType;
	}

	public void setCustomerType(int customerType) {
		CustomerType = customerType;
	}

	public String getCustomerDOB() {
		return CustomerDOB;
	}

	public void setCustomerDOB(String customerDOB) {
		CustomerDOB = customerDOB;
	}

	public String getCustomerGender() {
		return CustomerGender;
	}

	public void setCustomerGender(String customerGender) {
		CustomerGender = customerGender;
	}

	public String getCustomerMobileNumber() {
		return CustomerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		CustomerMobileNumber = customerMobileNumber;
	}

	public String getCustomerCurrentAddress() {
		return CustomerCurrentAddress;
	}

	public void setCustomerCurrentAddress(String customerCurrentAddress) {
		CustomerCurrentAddress = customerCurrentAddress;
	}

	public String getCustomerCurrentPincode() {
		return CustomerCurrentPincode;
	}

	public void setCustomerCurrentPincode(String customerCurrentPincode) {
		CustomerCurrentPincode = customerCurrentPincode;
	}

	public String getCustomerContactNumber1() {
		return CustomerContactNumber1;
	}

	public void setCustomerContactNumber1(String customerContactNumber1) {
		CustomerContactNumber1 = customerContactNumber1;
	}

	public String getCustomerContactNumber2() {
		return CustomerContactNumber2;
	}

	public void setCustomerContactNumber2(String customerContactNumber2) {
		CustomerContactNumber2 = customerContactNumber2;
	}

	public String getCustomerEmail() {
		return CustomerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}

	public String getCustomerImage() {
		return CustomerImage;
	}

	public void setCustomerImage(String customerImage) {
		CustomerImage = customerImage;
	}

	public int getCustomerStatus() {
		return CustomerStatus;
	}

	public void setCustomerStatus(int customerStatus) {
		CustomerStatus = customerStatus;
	}

	public int getCustomerActiveFlag() {
		return CustomerActiveFlag;
	}

	public void setCustomerActiveFlag(int customerActiveFlag) {
		CustomerActiveFlag = customerActiveFlag;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(CustomerFullName);
		parcel.writeInt(CustomerType);
		parcel.writeString(CustomerDOB);
		parcel.writeString(CustomerGender);
		parcel.writeString(CustomerMobileNumber);
		parcel.writeString(CustomerCurrentAddress);
		parcel.writeString(CustomerCurrentPincode);
		parcel.writeString(CustomerContactNumber1);
		parcel.writeString(CustomerContactNumber2);
		parcel.writeString(CustomerEmail);
		parcel.writeString(CustomerImage);
		parcel.writeInt(CustomerStatus);
		parcel.writeInt(CustomerActiveFlag);
	}
}
