package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class StartLocation2 implements Parcelable {

	private double lat;
	private double lng;

	protected StartLocation2(Parcel in) {
		lat = in.readDouble();
		lng = in.readDouble();
	}

	public static final Creator<StartLocation2> CREATOR = new Creator<StartLocation2>() {
		@Override
		public StartLocation2 createFromParcel(Parcel in) {
			return new StartLocation2(in);
		}

		@Override
		public StartLocation2[] newArray(int size) {
			return new StartLocation2[size];
		}
	};

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "StartLocation2 [lat = " + lat + ", lng = " + lng + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeDouble(lat);
		parcel.writeDouble(lng);
	}
}
