package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.WindowManager;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.utils.AppCommonUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.ui.SplashScreenActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.SplashScreenActivity.ACTIVITY_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.SplashScreenActivity.ACTIVITY_UTILS.*;

public class SplashScreenActivity extends AppCompatActivity {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Typeface typeFace;
	}

	static class ACTIVITY_UTILS {
		static AppCommonUtils appCommonUtils;
	}

	static class ACTIVITY_BOOLEANS {
		static boolean closeOnBackPress = false;
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		configView();
	}

	@Override
	public void onBackPressed() {
		if (!closeOnBackPress) {
			closeOnBackPress = true;
			handler.postDelayed(() -> closeOnBackPress = false, 1000);
		} else {
			finish();
		}
	}

	void configView() {

		getSupportActionBar().hide();

		activity = this;
		context = this;
		handler = new Handler();
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");

		appCommonUtils = new AppCommonUtils(activity, context);

		appCommonUtils.appAlreadyOpened();
		handler.postDelayed(() -> {
			if (!closeOnBackPress) {
				if (!sharedPreferences.getBoolean(PreferenceKeys.CUSTOMER_LOGGED_IN, false)) {
					startActivity(new Intent(getApplicationContext(), LoginActivity.class));
				} else {
					startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
				}
			}
			finish();
		}, 1500);
	}
}