package com.cordsinnovations.rtts.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.cordsinnovations.rtts.api_callbacks.Customer;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.ui.DashboardActivity;
import com.cordsinnovations.rtts.ui.LoginActivity;
import com.cordsinnovations.rtts.ui.RegisterOtpVerificationActivity;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.utils.CustomerUtils.UTIL_COMMONS.activity;

public class CustomerUtils {

	static class UTIL_COMMONS {
		static Activity activity;
		static Context context;
	}

	public CustomerUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
	}

	public void customerRegister(String OTP, int PKCustomerID) {
		sharedPreferences.edit()
				.putBoolean(PreferenceKeys.REGISTRATION_PARTIAL, true)
				.putBoolean(PreferenceKeys.REGISTRATION_COMPLETE, false)
				.putString(PreferenceKeys.REGISTRATION_OTP, OTP)
				.putString(PreferenceKeys.CUSTOMER_TOKEN, String.valueOf(PKCustomerID))
				.apply();
		activity.startActivity(new Intent(activity, RegisterOtpVerificationActivity.class));
		activity.finish();
	}

	public void customerOTPVerified() {
		sharedPreferences.edit()
				.putBoolean(PreferenceKeys.REGISTRATION_PARTIAL, false)
				.putBoolean(PreferenceKeys.REGISTRATION_COMPLETE, true)
				.putString(PreferenceKeys.REGISTRATION_OTP, null)
				.apply();
	}

	public void customerLogin(String customerToken, Customer customer) {
		sharedPreferences.edit()
				.putBoolean(PreferenceKeys.CUSTOMER_LOGGED_IN, true)
				.putString(PreferenceKeys.CUSTOMER_TOKEN, customerToken)
				.putString(PreferenceKeys.CUSTOMER_FULL_NAME, customer.CustomerFullName)
				.putString(PreferenceKeys.CUSTOMER_GENDER, customer.CustomerGender)
				.putString(PreferenceKeys.CUSTOMER_MOBILE, customer.CustomerMobileNumber)
				.putString(PreferenceKeys.CUSTOMER_EMAIL, customer.CustomerEmail)
				.putString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_1, customer.CustomerContactNumber1)
				.putString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_2, customer.CustomerContactNumber2)
				.putString(PreferenceKeys.CUSTOMER_IMAGE, customer.CustomerImage)
				.apply();
		activity.startActivity(new Intent(activity, DashboardActivity.class));
		activity.finish();
	}

	public void customerLogout() {
		sharedPreferences.edit()
				.putBoolean(PreferenceKeys.CUSTOMER_LOGGED_IN, false)
				.putString(PreferenceKeys.CUSTOMER_TOKEN, null)
				.putString(PreferenceKeys.CUSTOMER_FULL_NAME, null)
				.putString(PreferenceKeys.CUSTOMER_GENDER, null)
				.putString(PreferenceKeys.CUSTOMER_MOBILE, null)
				.putString(PreferenceKeys.CUSTOMER_EMAIL, null)
				.putString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_1, null)
				.putString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_2, null)
				.putString(PreferenceKeys.CUSTOMER_IMAGE, null)
				.apply();
		activity.startActivity(new Intent(activity, LoginActivity.class));
		activity.finish();
	}
}
