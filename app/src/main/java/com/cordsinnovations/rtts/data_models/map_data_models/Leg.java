package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Leg implements Parcelable {

	private Distance distance;
	private Duration duration;
	private String end_address;
	private EndLocation end_location;
	private String start_address;
	private StartLocation start_location;
	private Step[] steps;
	private Object[] traffic_speed_entry;
	private Object[] via_waypoint;

	protected Leg(Parcel in) {
		distance = in.readParcelable(Distance.class.getClassLoader());
		duration = in.readParcelable(Duration.class.getClassLoader());
		end_address = in.readString();
		end_location = in.readParcelable(EndLocation.class.getClassLoader());
		start_address = in.readString();
		start_location = in.readParcelable(StartLocation.class.getClassLoader());
		steps = in.createTypedArray(Step.CREATOR);
	}

	public static final Creator<Leg> CREATOR = new Creator<Leg>() {
		@Override
		public Leg createFromParcel(Parcel in) {
			return new Leg(in);
		}

		@Override
		public Leg[] newArray(int size) {
			return new Leg[size];
		}
	};

	public Distance getDistance() {
		return distance;
	}
	public void setDistance(Distance distance) {
		this.distance = distance;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	public String getEnd_address() {
		return end_address;
	}
	public void setEnd_address(String end_address) {
		this.end_address = end_address;
	}
	public EndLocation getEnd_location() {
		return end_location;
	}
	public void setEnd_location(EndLocation end_location) {
		this.end_location = end_location;
	}
	public String getStart_address() {
		return start_address;
	}
	public void setStart_address(String start_address) {
		this.start_address = start_address;
	}
	public StartLocation getStart_location() {
		return start_location;
	}
	public void setStart_location(StartLocation start_location) {
		this.start_location = start_location;
	}
	public Step[] getSteps() {
		return steps;
	}
	public void setSteps(Step[] steps) {
		this.steps = steps;
	}
	public Object[] getTraffic_speed_entry() {
		return traffic_speed_entry;
	}
	public void setTraffic_speed_entry(Object[] traffic_speed_entry) {
		this.traffic_speed_entry = traffic_speed_entry;
	}
	public Object[] getVia_waypoint() {
		return via_waypoint;
	}
	public void setVia_waypoint(Object[] via_waypoint) {
		this.via_waypoint = via_waypoint;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(distance, i);
		parcel.writeParcelable(duration, i);
		parcel.writeString(end_address);
		parcel.writeParcelable(end_location, i);
		parcel.writeString(start_address);
		parcel.writeParcelable(start_location, i);
		parcel.writeTypedArray(steps, i);
	}
}
