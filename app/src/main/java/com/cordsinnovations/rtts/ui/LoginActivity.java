package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.api_callbacks.Customer;
import com.cordsinnovations.rtts.api_callbacks.Login;
import com.cordsinnovations.rtts.api_callbacks.VehicleType;
import com.cordsinnovations.rtts.globals.Constants;
import com.cordsinnovations.rtts.globals.Constants.FILES_APP;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.CustomerUtils;
import com.cordsinnovations.rtts.utils.FileOperationUtils;
import com.cordsinnovations.rtts.utils.PermissionRequestUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.api;
import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.FILE_DOWNLOAD_TYPES.FILE_DOWNLOAD_IMAGE_CUSTOMER;

import static com.cordsinnovations.rtts.ui.LoginActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.LoginActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.LoginActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.LoginActivity.UI_ELEMENTS.LOGIN_FORM.*;
import static com.cordsinnovations.rtts.ui.LoginActivity.UI_ELEMENTS.OTHER_OPTIONS.*;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static Typeface typeFace;
		static InputMethodManager inputMethodManager = null;
	}

	static class ACTIVITY_UTILS {
		static PermissionRequestUtils permissionRequestUtils;
		static AppCommonUtils appCommonUtils;
		static CustomerUtils customerUtils;
		static FileOperationUtils fileOperationUtils;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static LinearLayout ll_login_activity;
			static TextView tv_login_title;
			static File profileImage;
			static Animation animationAlphaFadeIn, animationAlphaFadeOut;
			static boolean loginButtonShown = false;
		}

		static class LOGIN_FORM {
			static EditText et_customer_username, et_customer_password;
			static Button btn_customer_login;
		}

		static class OTHER_OPTIONS {
			static Button btn_customer_sign_up, btn_customer_otp_verification;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		configView();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS);
		} else {
			runnable.run();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_customer_login:
				customerLogin();
				break;
			case R.id.btn_customer_sign_up:
				startActivity(new Intent(context, RegisterActivity.class));
				finish();
				break;
			case R.id.btn_customer_otp_verification:
				startActivity(new Intent(context, RegisterOtpVerificationActivity.class));
				finish();
				break;
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
				if (runnable != null) {
					runnable.run();
				}
			}
		}
	}

	@Override
	public void onBackPressed() {
		appCommonUtils.appExitConfirmation();
	}

	void configView() {

		getSupportActionBar().hide();

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");
		runnable = () -> mandatoryFieldsCheck();

		permissionRequestUtils = new PermissionRequestUtils(activity, context);
		appCommonUtils = new AppCommonUtils(activity, context);
		customerUtils = new CustomerUtils(activity, context);
		fileOperationUtils = new FileOperationUtils(activity, context);

		boolean registrationPartial = sharedPreferences.getBoolean(PreferenceKeys.REGISTRATION_PARTIAL, false);
		boolean registrationComplete = sharedPreferences.getBoolean(PreferenceKeys.REGISTRATION_COMPLETE, false);

		ll_login_activity = findViewById(R.id.ll_login_activity);

		tv_login_title = findViewById(R.id.tv_login_title);
		tv_login_title.setTypeface(typeFace);
		animationAlphaFadeIn = AnimationUtils.loadAnimation(context, R.anim.alpha_fade_in);
		animationAlphaFadeOut = AnimationUtils.loadAnimation(context, R.anim.alpha_fade_out);

		et_customer_username = findViewById(R.id.et_customer_username);
		et_customer_password = findViewById(R.id.et_customer_password);

		appCommonUtils.editTextWatcher(et_customer_username, runnable);
		appCommonUtils.editTextWatcher(et_customer_password, runnable);

		btn_customer_login = findViewById(R.id.btn_customer_login);
		btn_customer_login.setTypeface(typeFace);
		btn_customer_sign_up = findViewById(R.id.btn_customer_sign_up);
		btn_customer_sign_up.setTypeface(typeFace);
		btn_customer_otp_verification = findViewById(R.id.btn_customer_otp_verification);
		btn_customer_otp_verification.setTypeface(typeFace);

		if (registrationPartial) {
			btn_customer_sign_up.setVisibility(View.GONE);
			btn_customer_otp_verification.setVisibility(View.VISIBLE);
		} else if (registrationComplete) {
			btn_customer_otp_verification.setVisibility(View.GONE);
			btn_customer_sign_up.setVisibility(View.VISIBLE);
		}

		btn_customer_login.setOnClickListener(this);
		btn_customer_sign_up.setOnClickListener(this);
		btn_customer_otp_verification.setOnClickListener(this);
	}

	void mandatoryFieldsCheck() {
		String customerUsername = et_customer_username.getText().toString();
		String customerPassword = et_customer_password.getText().toString();

		if (customerUsername.equals("") || customerPassword.equals("")) {
			if (loginButtonShown) {
				runnable = () -> {
					//btn_customer_login.setBackground(context.getResources().getDrawable(R.drawable.app_secondary_button_inverse));
					btn_customer_login.setVisibility(View.INVISIBLE);
				};
				appCommonUtils.animationListener(animationAlphaFadeOut, runnable);
				btn_customer_login.startAnimation(animationAlphaFadeOut);
				loginButtonShown = false;
			}
		} else if (!customerUsername.equals("") && !customerPassword.equals("")) {
			runnable = () -> customerLogin();
			appCommonUtils.onEditorActionListener(et_customer_password, EditorInfo.IME_ACTION_DONE, runnable);
			btn_customer_login.setEnabled(true);
			if (!loginButtonShown) {
				btn_customer_login.startAnimation(animationAlphaFadeIn);
				//btn_customer_login.setBackground(context.getResources().getDrawable(R.drawable.app_secondary_button));
				btn_customer_login.setVisibility(View.VISIBLE);
				loginButtonShown = true;
			}
		}
	}

	void customerLogin() {
		try {
			inputMethodManager.hideSoftInputFromWindow(et_customer_username.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_password.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String customerUsername = et_customer_username.getText().toString();
		String customerPassword = et_customer_password.getText().toString();
		if (customerUsername.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide your Username!");
		} else if (customerPassword.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide your Password!");
		} else {
			final Call<Login> loginCall = api.login(
					customerUsername,
					customerPassword
			);
			loginCall.enqueue(new Callback<Login>() {
				@Override
				public void onResponse(Call<Login> call, Response<Login> response) {
					if (response.isSuccessful()) {
						Login responseBody = response.body();
						if (responseBody.ErrorFlag == 0) {
							Customer customer = responseBody.CustomerDetails[0];
							ArrayList<VehicleType> VehicleTypes = new ArrayList<VehicleType>();
							Collections.addAll(VehicleTypes, responseBody.VehicleTypeDetails);
							if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
								try {
									profileImage = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, customer.CustomerImage);
									if (!profileImage.isFile()) {
										fileOperationUtils.downloadFile(customer.CustomerImage, FILE_DOWNLOAD_IMAGE_CUSTOMER);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							customerUtils.customerLogin(responseBody.CustomerLoginToken, customer);
						} else {
							switch (responseBody.LoginFlag) {
								case 2:
									appCommonUtils.appShowShortToast(responseBody.LoginMessage);
									break;
								case 3:
									appCommonUtils.appShowShortToast(responseBody.LoginMessage);
									break;
							}
						}
					} else {
						appCommonUtils.appShowShortToast("Unable to Login at this point of time! Please try again later!");
					}
				}

				@Override
				public void onFailure(Call<Login> call, Throwable t) {
					appCommonUtils.appShowShortToast("Can't connect to Server! Please check your Network Connection!");
				}
			});
		}
	}
}
