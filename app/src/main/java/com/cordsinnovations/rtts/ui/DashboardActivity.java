package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.adapters.ui_adapters.AdsAdapter;
import com.cordsinnovations.rtts.controllers.ui_controllers.AdsController;
import com.cordsinnovations.rtts.globals.Constants;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.hardware.ShakeDetector;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.CustomerUtils;
import com.cordsinnovations.rtts.utils.FileOperationUtils;
import com.cordsinnovations.rtts.utils.LocationUtils;
import com.cordsinnovations.rtts.utils.PermissionRequestUtils;
import com.mikhaellopez.circularimageview.CircularImageView;

import com.cordsinnovations.rtts.globals.Constants.FILES_APP;

import java.io.File;

import io.codetail.widget.RevealFrameLayout;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_SHAKE_CONSTANTS.SHAKE_TIMEOUT_SECONDS;
import static com.cordsinnovations.rtts.globals.Constants.FILE_DOWNLOAD_TYPES.FILE_DOWNLOAD_IMAGE_CUSTOMER;

import static com.cordsinnovations.rtts.ui.DashboardActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.ACTIVITY_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.PROFILE_DETAILS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.UI_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.UI_ALERTS.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.MAIN_SECTION.TRAVEL_SECTION.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.MAIN_SECTION.SOS_SECTION.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.INFO_SECTION.*;
import static com.cordsinnovations.rtts.ui.DashboardActivity.UI_ELEMENTS.ADS_SECTION.*;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener, ShakeDetector.OnShakeListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static InputMethodManager inputMethodManager = null;
		static Typeface typeFace;
		static LayoutInflater layoutInflater;
	}

	static class ACTIVITY_UTILS {
		static PermissionRequestUtils permissionRequestUtils;
		static AppCommonUtils appCommonUtils;
		static CustomerUtils customerUtils;
		static FileOperationUtils fileOperationUtils;
		static LocationUtils locationUtils;
	}

	static class ACTIVITY_BOOLEANS {
		static boolean closeOnBackPress = false;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static Toolbar tb_dashboard;
			static CircularImageView civ_customer_pic;
		}

		static class UI_BOOLEANS {
			static boolean deviceShake = false;
			static boolean sosRequest = false;
			static boolean sosCancel = false;
			static boolean customerOptionsOpen = false;
			static boolean customerImageClicked = false;
			static boolean autoSlide = false;
		}

		static class UI_ALERTS {
			static AlertDialog.Builder alertBuilder;
			static AlertDialog alertDialog;
			static View alertDialogView;
		}

		static class PROFILE_DETAILS {
			static File profileImage;
			static ImageView iv_profile_options_close, iv_profile_pic;
			static TextView tv_customer_full_name, tv_customer_mobile;
			static Button btn_customer_email, btn_customer_emergency_contact_1, btn_customer_emergency_contact_2;
			static ImageButton ib_settings, ib_logout;
		}

		static class MAIN_SECTION {

			static class TRAVEL_SECTION {
				static CardView cv_travel_btn;
				static ImageView iv_travel_btn;
				static TextView tv_travel_btn;
			}

			static class SOS_SECTION {
				static RevealFrameLayout rfl_sos_dialog;
				static CardView cv_sos_btn, cv_layout_sos;
				static ImageView iv_sos_btn;
				static TextView tv_sos_btn, tv_sos_message;
				static ImageButton ib_sos_cancel;
				static ShakeDetector shakeDetector;
				static Location sosLocation = null;
				static Double sosLatitude, sosLongitude;

				static Thread threadSosSecondsCount;
			}
		}

		static class INFO_SECTION {
			static CardView cv_info_box;
			static TextView tv_info_box;
			static ImageView iv_info_box;
		}

		static class ADS_SECTION {
			static AdsAdapter adsAdapter;
			static AdsController adsController;
			static CardView cv_ads_box;
			static FrameLayout fl_ads_view;
			static ViewPager vp_ads_view;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		configView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.main_menu_profile:
				setCustomerOptionsOpen();
				break;
			/*case R.id.main_menu_settings:
				startActivity(new Intent(activity, SettingsActivity.class));
				break;*/
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDeviceShake() {
		deviceShake = true;
		runOnUiThread(() -> {
			try {
				sosLocation = locationUtils.getLocation();
				sosLatitude = sosLocation.getLatitude();
				sosLongitude = sosLocation.getLongitude();
				sosLocation = null;
				shakeDetector.stop();
				hideSOSDialog(deviceShake);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.cv_travel_btn:
				runnable = () -> {
					startActivity(new Intent(activity, TravelActivity.class));
					sosLocation = null;
					finish();
				};
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_AND_NETWORK_STATE_PERMISSIONS);
				} else {
					runnable.run();
				}
				break;
			case R.id.cv_sos_btn:
				runnable = () -> {
					showSOSDialog();
					shakeDetector.start();

					handler.postDelayed(() -> {
						if (!deviceShake) {
							shakeDetector.stop();
						}
						deviceShake = false;
					}, SHAKE_TIMEOUT_SECONDS * 1000);
				};
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_CALL_AND_LOCATION_AND_NETWORK_STATE_PERMISSIONS);
				} else {
					runnable.run();
				}
				break;
		}
	}

	@Override
	public void onBackPressed() {
		if (customerOptionsOpen) {
			setCustomerOptionsOpen();
		} else if (!closeOnBackPress) {
			closeOnBackPress = true;
			appCommonUtils.appExitConfirmation();
			handler.postDelayed(() -> closeOnBackPress = false, 2018);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
				if (runnable != null) {
					runnable.run();
				}
			}
		}
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_AND_NETWORK_STATE_PERMISSIONS) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_LOC, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_LOC, false).apply();
			}
			if (grantResults[2] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_NET, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_NET, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_LOC, false) && sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_NET, false)) {
				if (runnable != null) {
					runnable.run();
				}
			}
		}
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_CALL_AND_LOCATION_AND_NETWORK_STATE_PERMISSIONS) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAL, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAL, false).apply();
			}
			if (grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_LOC, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_LOC, false).apply();
			}
			if (grantResults[3] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_NET, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_NET, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_CAL, false) && sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_LOC, false) && sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_NET, false)) {
				if (runnable != null) {
					//locationUtils.displayLocationSettingsRequest(context);
					runnable.run();
				}
			}
		}
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAL, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAL, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_CAL, false)) {
				if (runnable != null) {
					runnable.run();
				}
			}
		}
	}

	private void configView() {

		this.getWindow().setStatusBarColor(getColor(R.color.colorPrimaryDark));

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");
		layoutInflater = (LayoutInflater.from(context));

		permissionRequestUtils = new PermissionRequestUtils(activity, context);
		appCommonUtils = new AppCommonUtils(activity, context);
		customerUtils = new CustomerUtils(activity, context);
		fileOperationUtils = new FileOperationUtils(activity, context);
		locationUtils = new LocationUtils(activity, context);

		//tb_dashboard = findViewById(R.id.tb_dashboard);
		tb_dashboard = findViewById(R.id.tb_dashboard);
		tb_dashboard.setTitleTextColor(getColor(R.color.colorWhite));

		setSupportActionBar(tb_dashboard);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setHomeButtonEnabled(false);

		//civ_customer_pic = findViewById(R.id.civ_customer_pic);

		cv_travel_btn = findViewById(R.id.cv_travel_btn);
		iv_travel_btn = findViewById(R.id.iv_travel_btn);
		tv_travel_btn = findViewById(R.id.tv_travel_btn);
		tv_travel_btn.setTypeface(typeFace);

		cv_sos_btn = findViewById(R.id.cv_sos_btn);
		iv_sos_btn = findViewById(R.id.iv_sos_btn);
		shakeDetector = new ShakeDetector(context);
		tv_sos_btn = findViewById(R.id.tv_sos_btn);
		tv_sos_btn.setTypeface(typeFace);

		cv_info_box = findViewById(R.id.cv_info_box);
		tv_info_box = findViewById(R.id.tv_info_box);
		iv_info_box = findViewById(R.id.iv_info_box);

		cv_ads_box = findViewById(R.id.cv_ads_box);
		fl_ads_view = findViewById(R.id.fl_ads_view);
		vp_ads_view = findViewById(R.id.vp_ads_view);

		handler.postDelayed(() -> {
			cv_ads_box.setVisibility(View.VISIBLE);
			sliderStart();
			adsAdapter = new AdsAdapter(context);
			vp_ads_view.setAdapter(adsAdapter);
			adsController = new AdsController(context);
			adsController.applyViewPagerScrollSpeed(vp_ads_view, 2000);
		}, 5000);

		//civ_customer_pic.setOnClickListener(this);

		cv_travel_btn.setOnClickListener(this);
		cv_sos_btn.setOnClickListener(this);
		shakeDetector.setShakeListener(this);
	}

	private void setCustomerOptionsOpen() {
		customerOptionsOpen = !customerOptionsOpen;

		if (customerOptionsOpen) {
			alertBuilder = new AlertDialog.Builder(context);
			alertDialogView = layoutInflater.inflate(R.layout.layout_profile_options, null);

			iv_profile_options_close = alertDialogView.findViewById(R.id.iv_profile_options_close);
			iv_profile_options_close.setOnClickListener(view -> {
				customerOptionsOpen = !customerOptionsOpen;
				alertDialog.cancel();
			});

			iv_profile_pic = alertDialogView.findViewById(R.id.iv_profile_pic);
			loadProfileImage();

			tv_customer_full_name = alertDialogView.findViewById(R.id.tv_customer_full_name);
			tv_customer_full_name.setText(sharedPreferences.getString(PreferenceKeys.CUSTOMER_FULL_NAME, null));
			tv_customer_full_name.setSelected(true);

			tv_customer_mobile = alertDialogView.findViewById(R.id.tv_customer_mobile);
			tv_customer_mobile.setText(sharedPreferences.getString(PreferenceKeys.CUSTOMER_MOBILE, null));
			tv_customer_mobile.setSelected(true);

			btn_customer_email = alertDialogView.findViewById(R.id.btn_customer_email);
			final String customer_email = sharedPreferences.getString(PreferenceKeys.CUSTOMER_EMAIL, null);
			if (customer_email != null) {
				btn_customer_email.setText(customer_email);
				btn_customer_email.setSelected(true);
				btn_customer_email.setOnClickListener(view -> startActivity(new Intent().setData(Uri.parse("mailto:" + customer_email))));
			} else {
				btn_customer_email.setVisibility(View.GONE);
			}

			btn_customer_emergency_contact_1 = alertDialogView.findViewById(R.id.btn_customer_emergency_contact_1);
			final String customer_emergency_contact_1 = sharedPreferences.getString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_1, null);
			if (customer_emergency_contact_1 != null) {
				btn_customer_emergency_contact_1.setText(customer_emergency_contact_1);
				btn_customer_emergency_contact_1.setSelected(true);
				btn_customer_emergency_contact_1.setOnClickListener(view -> {
					runnable = () -> startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + customer_emergency_contact_1)));
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
						permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION);
					} else {
						runnable.run();
					}
				});
			} else {
				btn_customer_emergency_contact_1.setVisibility(View.GONE);
			}

			btn_customer_emergency_contact_2 = alertDialogView.findViewById(R.id.btn_customer_emergency_contact_2);
			final String customer_emergency_contact_2 = sharedPreferences.getString(PreferenceKeys.CUSTOMER_EMERGENCY_CONTACT_2, null);
			if (customer_emergency_contact_2 != null) {
				btn_customer_emergency_contact_2.setText(customer_emergency_contact_2);
				btn_customer_emergency_contact_2.setSelected(true);
				btn_customer_emergency_contact_2.setOnClickListener(view -> {
					runnable = () -> startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + customer_emergency_contact_2)));
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
						permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION);
					} else {
						runnable.run();
					}
				});
			} else {
				btn_customer_emergency_contact_2.setVisibility(View.GONE);
			}

			ib_settings = alertDialogView.findViewById(R.id.ib_settings);
			ib_settings.setOnClickListener(view -> {
				customerOptionsOpen = !customerOptionsOpen;
				alertDialog.cancel();
				startActivity(new Intent(activity, SettingsActivity.class));
				finish();
			});

			ib_logout = alertDialogView.findViewById(R.id.ib_logout);
			ib_logout.setOnClickListener(view -> {
				customerOptionsOpen = !customerOptionsOpen;
				alertDialog.dismiss();
				customerUtils.customerLogout();
			});

			alertBuilder.setView(alertDialogView);
			alertDialog = alertBuilder.create();
			alertDialog.setCancelable(false);
			alertDialog.show();
		} else {
			if (alertDialog != null && alertDialog.isShowing()) {
				alertDialog.dismiss();
			}
		}
	}

	private void sliderStart() {
		autoSlide = true;
		handler.postDelayed(new Runnable() {
			int page = 0;
			@Override
			public void run() {
				page++;
				if (page == AdsAdapter.IMG_COUNT) page = 0;
				vp_ads_view.setCurrentItem(page, false);
				if (autoSlide) handler.postDelayed(this, 3000);
			}
		}, 5000);
	}

	private void loadProfileImage() {
		try {
			profileImage = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, sharedPreferences.getString(PreferenceKeys.CUSTOMER_IMAGE, null));
			if (profileImage.isFile()) {
				customerImageClicked = false;
				iv_profile_pic.setImageURI(Uri.fromFile(profileImage));
				profileImage = null;
			} else {
				iv_profile_pic.setBackground(context.getResources().getDrawable(R.drawable.app_primary_button));
				iv_profile_pic.setImageResource(R.drawable.ic_profile);
				Log.d("Downloading", "Clicked");
				iv_profile_pic.setOnClickListener(view -> {
					runnable = () -> {
						if (!customerImageClicked) {
							customerImageClicked = true;
							fileOperationUtils.downloadFile(sharedPreferences.getString(PreferenceKeys.CUSTOMER_IMAGE, null), FILE_DOWNLOAD_IMAGE_CUSTOMER);
							handler.postDelayed(() -> loadProfileImage(), 2500);
						}
					};
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
						permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS);
					} else {
						runnable.run();
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showSOSDialog() {
		sosRequest = true;
		alertBuilder = new AlertDialog.Builder(context);
		alertDialogView = layoutInflater.inflate(R.layout.layout_sos, null);

		rfl_sos_dialog = alertDialogView.findViewById(R.id.rfl_sos_dialog);
		cv_layout_sos = alertDialogView.findViewById(R.id.cv_layout_sos);

		tv_sos_message = alertDialogView.findViewById(R.id.tv_sos_message);
		tv_sos_message.setText(sharedPreferences.getString(PreferenceKeys.CUSTOMER_FULL_NAME, null));
		tv_sos_message.setSelected(true);

		ib_sos_cancel = alertDialogView.findViewById(R.id.ib_sos_cancel);
		ib_sos_cancel.setOnClickListener(v -> {
			sosCancel = true;
			sosLocation = null;
			shakeDetector.stop();
			hideSOSDialog(deviceShake);
		});

		threadSosSecondsCount = new Thread() {
			public void run() {
				for (int i = SHAKE_TIMEOUT_SECONDS; i >= 0; i--) {
					try {
						final int remainingSeconds = i;
						runOnUiThread(() -> {
							tv_sos_message.setText(context.getString(R.string.sos_message_before_prefix) + " " + remainingSeconds + " " + context.getString(R.string.sos_message_before_suffix));
							if (remainingSeconds == 0) {
								hideSOSDialog(deviceShake);
							}
						});
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
						return;
					}
				}
			}
		};
		threadSosSecondsCount.start();

		alertBuilder.setView(alertDialogView);
		alertDialog = alertBuilder.create();
		alertDialog.setCancelable(false);
		alertDialog.show();

		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ViewAnimationUtils.createCircularReveal(cv_layout_sos,
					cv_layout_sos.getWidth() / 2,
					cv_layout_sos.getHeight() / 2,
					0,
					1.4f * cv_layout_sos.getWidth()
			).setDuration(800).start();
		} else {
			Animator am = io.codetail.animation.ViewAnimationUtils.createCircularReveal(cv_layout_sos,
					cv_layout_sos.getWidth() / 2,
					cv_layout_sos.getHeight() / 2,
					0,
					1.4f * cv_layout_sos.getWidth()
			);
			am.setDuration(800);
			am.start();
		}*/
	}

	private void hideSOSDialog(boolean deviceShake) {
		sosRequest = false;
		threadSosSecondsCount.interrupt();
		ib_sos_cancel.setVisibility(View.GONE);
		if (deviceShake) {
			cv_layout_sos.setCardBackgroundColor(context.getColor(R.color.colorButtonTeal));
			tv_sos_message.setText(context.getString(R.string.sos_message_success));
			//tv_sos_message.setText(context.getString(R.string.sos_message_success) + "\n" + "SOS Latitude : " + sosLatitude + "\nSOS Longitude : " + sosLongitude);
		} else {
			cv_layout_sos.setCardBackgroundColor(context.getColor(R.color.colorButtonOrange));
			if (sosCancel) {
				sosCancel = false;
				tv_sos_message.setText(context.getString(R.string.sos_message_canceled));
			} else {
				tv_sos_message.setText(context.getString(R.string.sos_message_failed));
			}
		}
		handler.postDelayed(() -> alertDialog.dismiss(), 3500);
	}
}
