package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class EndLocation implements Parcelable {

	public static final Creator<EndLocation> CREATOR = new Creator<EndLocation>() {
		@Override
		public EndLocation createFromParcel(Parcel in) {
			return new EndLocation(in);
		}

		@Override
		public EndLocation[] newArray(int size) {
			return new EndLocation[size];
		}
	};

	private double lat;
	private double lng;

	protected EndLocation(Parcel in) {
		lat = in.readDouble();
		lng = in.readDouble();
	}

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "EndLocation [lat = " + lat + ", lng = " + lng + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeDouble(lat);
		parcel.writeDouble(lng);
	}
}
