package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class StartLocation implements Parcelable {

	public static final Creator<StartLocation> CREATOR = new Creator<StartLocation>() {
		@Override
		public StartLocation createFromParcel(Parcel in) {
			return new StartLocation(in);
		}

		@Override
		public StartLocation[] newArray(int size) {
			return new StartLocation[size];
		}
	};

	private double lat;
	private double lng;

	protected StartLocation(Parcel in) {
		lat = in.readDouble();
		lng = in.readDouble();
	}

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "StartLocation [lat = " + lat + ", lng = " + lng + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeDouble(lat);
		parcel.writeDouble(lng);
	}
}
