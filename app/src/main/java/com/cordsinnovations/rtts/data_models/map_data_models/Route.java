package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Route implements Parcelable {

	private Bounds bounds;
	private String copyrights;
	private Leg[] legs;
	private OverviewPolyline overview_polyline;
	private String summary;
	private Object[] warnings;
	private Object[] waypoint_order;

	protected Route(Parcel in) {
		bounds = in.readParcelable(Bounds.class.getClassLoader());
		copyrights = in.readString();
		legs = in.createTypedArray(Leg.CREATOR);
		overview_polyline = in.readParcelable(OverviewPolyline.class.getClassLoader());
		summary = in.readString();
	}

	public static final Creator<Route> CREATOR = new Creator<Route>() {
		@Override
		public Route createFromParcel(Parcel in) {
			return new Route(in);
		}

		@Override
		public Route[] newArray(int size) {
			return new Route[size];
		}
	};

	public Bounds getBounds() {
		return bounds;
	}
	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}
	public String getCopyrights() {
		return copyrights;
	}
	public void setCopyrights(String copyrights) {
		this.copyrights = copyrights;
	}
	public Leg[] getLegs() {
		return legs;
	}
	public void setLegs(Leg[] legs) {
		this.legs = legs;
	}
	public OverviewPolyline getOverview_polyline() {
		return overview_polyline;
	}
	public void setOverview_polyline(OverviewPolyline overview_polyline) {
		this.overview_polyline = overview_polyline;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Object[] getWarnings() {
		return warnings;
	}
	public void setWarnings(Object[] warnings) {
		this.warnings = warnings;
	}
	public Object[] getWaypoint_order() {
		return waypoint_order;
	}
	public void setWaypoint_order(Object[] waypoint_order) {
		this.waypoint_order = waypoint_order;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(bounds, i);
		parcel.writeString(copyrights);
		parcel.writeTypedArray(legs, i);
		parcel.writeParcelable(overview_polyline, i);
		parcel.writeString(summary);
	}
}
