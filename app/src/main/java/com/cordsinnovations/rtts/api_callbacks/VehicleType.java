package com.cordsinnovations.rtts.api_callbacks;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleType implements Parcelable {

	public int PKVehicleTypeID;
	public String VehicleTypeName;
	public int VehicleTypeRate;
	public int VehicleTypeWaitingCharge;
	public int VehicleTypeSeatingCapacity;
	public String VehicleTypeDescription;
	public String VehicleTypeImage;
	public int VehicleTypeStatus;
	public int VehicleTypeActiveFlag;

	protected VehicleType(Parcel in) {
		PKVehicleTypeID = in.readInt();
		VehicleTypeName = in.readString();
		VehicleTypeRate = in.readInt();
		VehicleTypeWaitingCharge = in.readInt();
		VehicleTypeSeatingCapacity = in.readInt();
		VehicleTypeDescription = in.readString();
		VehicleTypeImage = in.readString();
		VehicleTypeStatus = in.readInt();
		VehicleTypeActiveFlag = in.readInt();
	}

	public static final Creator<VehicleType> CREATOR = new Creator<VehicleType>() {
		@Override
		public VehicleType createFromParcel(Parcel in) {
			return new VehicleType(in);
		}

		@Override
		public VehicleType[] newArray(int size) {
			return new VehicleType[size];
		}
	};

	public int getPKVehicleTypeID() {
		return PKVehicleTypeID;
	}
	public void setPKVehicleTypeID(int PKVehicleTypeID) {
		this.PKVehicleTypeID = PKVehicleTypeID;
	}
	public String getVehicleTypeName() {
		return VehicleTypeName;
	}
	public void setVehicleTypeName(String vehicleTypeName) {
		VehicleTypeName = vehicleTypeName;
	}
	public int getVehicleTypeRate() {
		return VehicleTypeRate;
	}
	public void setVehicleTypeRate(int vehicleTypeRate) {
		VehicleTypeRate = vehicleTypeRate;
	}
	public int getVehicleTypeWaitingCharge() {
		return VehicleTypeWaitingCharge;
	}
	public void setVehicleTypeWaitingCharge(int vehicleTypeWaitingCharge) {
		VehicleTypeWaitingCharge = vehicleTypeWaitingCharge;
	}
	public int getVehicleTypeSeatingCapacity() {
		return VehicleTypeSeatingCapacity;
	}
	public void setVehicleTypeSeatingCapacity(int vehicleTypeSeatingCapacity) {
		VehicleTypeSeatingCapacity = vehicleTypeSeatingCapacity;
	}
	public String getVehicleTypeDescription() {
		return VehicleTypeDescription;
	}
	public void setVehicleTypeDescription(String vehicleTypeDescription) {
		VehicleTypeDescription = vehicleTypeDescription;
	}
	public String getVehicleTypeImage() {
		return VehicleTypeImage;
	}
	public void setVehicleTypeImage(String vehicleTypeImage) {
		VehicleTypeImage = vehicleTypeImage;
	}
	public int getVehicleTypeStatus() {
		return VehicleTypeStatus;
	}
	public void setVehicleTypeStatus(int vehicleTypeStatus) {
		VehicleTypeStatus = vehicleTypeStatus;
	}
	public int getVehicleTypeActiveFlag() {
		return VehicleTypeActiveFlag;
	}
	public void setVehicleTypeActiveFlag(int vehicleTypeActiveFlag) {
		VehicleTypeActiveFlag = vehicleTypeActiveFlag;
	}

	@Override
	public String toString() {
		return "VehicleType [PKVehicleTypeID = " + PKVehicleTypeID + ", VehicleTypeName = " + VehicleTypeName + ", VehicleTypeRate = " + VehicleTypeRate + ", VehicleTypeWaitingCharge = " + VehicleTypeWaitingCharge + ", VehicleTypeSeatingCapacity = " + VehicleTypeSeatingCapacity + ", VehicleTypeDescription = " + VehicleTypeDescription + ", VehicleTypeImage = " + VehicleTypeImage + ", VehicleTypeStatus = " + VehicleTypeStatus + ", VehicleTypeActiveFlag = " + VehicleTypeActiveFlag + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(PKVehicleTypeID);
		dest.writeString(VehicleTypeName);
		dest.writeInt(VehicleTypeRate);
		dest.writeInt(VehicleTypeWaitingCharge);
		dest.writeInt(VehicleTypeSeatingCapacity);
		dest.writeString(VehicleTypeDescription);
		dest.writeString(VehicleTypeImage);
		dest.writeInt(VehicleTypeStatus);
		dest.writeInt(VehicleTypeActiveFlag);
	}
}
