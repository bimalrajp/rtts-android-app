package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.adapters.ui_adapters.PlaceAutocompleteAdapter;
import com.cordsinnovations.rtts.adapters.ui_adapters.VehicleTypeAdapter;
import com.cordsinnovations.rtts.data_models.map_data_models.Distance;
import com.cordsinnovations.rtts.data_models.map_data_models.Duration;
import com.cordsinnovations.rtts.data_models.map_data_models.Leg;
import com.cordsinnovations.rtts.data_models.map_data_models.MapRootObject;
import com.cordsinnovations.rtts.data_models.map_data_models.Polyline;
import com.cordsinnovations.rtts.data_models.map_data_models.Route;
import com.cordsinnovations.rtts.data_models.map_data_models.Step;
import com.cordsinnovations.rtts.data_models.vehicle_data_models.VehicleType;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.hardware.RotationDetector;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.FileOperationUtils;
import com.cordsinnovations.rtts.utils.GoogleMapUtils;
import com.cordsinnovations.rtts.utils.LocationUtils;
import com.cordsinnovations.rtts.utils.MathematicsUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.api;
import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.MAP_MARKER_OPERATION_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_MARKER_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_PARAMETERS.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_POLYLINE_OPERATION_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.MAP_SEARCH_REQUESTS.*;

import static com.cordsinnovations.rtts.ui.TravelActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.UI_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.LOCATION_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.LOCATION_VARIABLES.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.MAP_CONTROLS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.SEARCH_CONTROLS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.ROUTE_DETAILS.*;
import static com.cordsinnovations.rtts.ui.TravelActivity.UI_ELEMENTS.BOOKING_CONTROLS.*;

public class TravelActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, RotationDetector.OnRotationListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static InputMethodManager inputMethodManager = null;
		static Typeface typeFace;
	}

	static class ACTIVITY_UTILS {
		static AppCommonUtils appCommonUtils;
		static FileOperationUtils fileOperationUtils;
		static GoogleMapUtils googleMapUtils;
		static LocationUtils locationUtils;
		static MathematicsUtils mathematicsUtils;
	}

	static class LOCATION_BOOLEANS {
		static boolean current_set = false;
		static boolean source_set = false;
		static boolean destination_set = false;
	}

	static class LOCATION_VARIABLES {
		static MapRootObject mapRootObject;
		static Location currentLocation = null;
		static String source_location, destination_location;
		static LatLng currentLatLng, sourceLatLng, destinationLatLng;
		static Double currentLatitude, currentLongitude, sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude;

		static MarkerOptions currentMarkerOptions, sourceMarkerOptions, destinationMarkerOptions;
		static Marker currentMarker, sourceMarker, destinationMarker;

		static List sectionLatLngs, routeSections;

		static PolylineOptions routePolylineOptions;
		static com.google.android.gms.maps.model.Polyline routePolyline;
		static LatLngBounds routeLatLngBounds = null;
		static CameraUpdate routeCameraUpdate = null;

		static String route_source_city_name, route_destination_city_name;
		static int route_distance, route_duration;
		static String route_distance_text, route_duration_text;

		static GoogleApiClient googleApiClient;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static GoogleMap travelMap;
			static Bitmap markerBitmap;
			static RotationDetector rotationDetector;
			static float angleAzimuth;
		}

		static class UI_BOOLEANS {
			static boolean search_box = false;
			static boolean route_details_shown = false;
			static boolean vehicle_types_shown = false;
			static boolean map_rotation = false;
		}

		static class MAP_CONTROLS {
			static ImageButton ib_zoom_in, ib_zoom_out, ib_current_location, ib_search_box;
		}

		static class SEARCH_CONTROLS {
			static PlaceAutocompleteAdapter placeAutocompleteAdapter;
			static LinearLayout ll_search_box;
			static AutoCompleteTextView actv_search_source, actv_search_destination;
			static ImageButton ib_source_current_location, ib_search_source, ib_destination_current_location, ib_search_destination, ib_search_both;
		}

		static class ROUTE_DETAILS {
			static LinearLayout ll_route_details;
			static TextView tv_source_city_name, tv_destination_city_name, tv_route_distance_text, tv_route_duration_text;
		}

		static class BOOKING_CONTROLS {
			static int currentVehicleTypePosition = -1, previousVehicleTypePosition = -1;
			static boolean currentVehicleTypeSelected = false;
			static Button btn_booking, btn_booking_proceed;
			static LinearLayout ll_booking_vehicle_types;
			static RecyclerView rv_booking_vehicle_type_buttons;
			static RecyclerView.LayoutManager vehicleRecyclerViewLayoutManager;
			static VehicleTypeAdapter vehicleTypeAdapter;
			static ArrayList<VehicleType> vehicleTypeList;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_travel);
		//Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_travel);
		mapFragment.getMapAsync(this);

		configView();
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		travelMap = googleMap;
		try {
			boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, sharedPreferences.getInt(PreferenceKeys.SETTINGS_MAP_STYLE_ID, R.raw.map_style_01)));
			if (!success) {
				Log.e("MapStyle", "Style parsing failed.");
			}
		} catch (Resources.NotFoundException e) {
			Log.e("MapStyle", "Can't find style. Error: ", e);
		}
		travelMap.getUiSettings().setRotateGesturesEnabled(false);
		travelMap.getUiSettings().setTiltGesturesEnabled(false);

		getCurrentLocation(REQUEST_CURRENT_LOCATION, true);

		runnable = () -> {
			try {
				rotationDetector.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		};

		runOnUiThread(() -> runnable.run());

		travelMap.setOnMapLongClickListener(latLng -> {
			if (sourceLatLng != null && destinationLatLng != null) {
				resetMapVariables();
			}
			if (sourceLatLng == null) {
				source_set = true;
				sourceLatLng = latLng;
				setMarkerOptions(REQUEST_SOURCE_LOCATION);
				markerOperations(MARKER_ADD_OPERATION, MARKER_SOURCE_LOCATION, false);
				actv_search_source.setText(locationUtils.getLocationAddress(sourceLatLng, true));
			} else {
				destination_set = true;
				destinationLatLng = latLng;
				setMarkerOptions(REQUEST_DESTINATION_LOCATION);
				markerOperations(MARKER_ADD_OPERATION, MARKER_DESTINATION_LOCATION, false);
				searchRoute();
				actv_search_destination.setText(locationUtils.getLocationAddress(destinationLatLng, true));
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.ib_zoom_in:
				googleMapUtils.mapZoom(travelMap, true);
				break;
			case R.id.ib_zoom_out:
				googleMapUtils.mapZoom(travelMap, false);
				break;
			case R.id.ib_search_box:
				toggleSearchBox();
				break;
			case R.id.ib_current_location:
				changeMapRotation();
				searchLocation(REQUEST_CURRENT_LOCATION);
				break;
			case R.id.ib_source_current_location:
				searchLocation(REQUEST_SOURCE_CURRENT_LOCATION);
				break;
			/*case R.id.ib_search_source:
				searchLocation(REQUEST_SOURCE_LOCATION);
				break;*/
			case R.id.ib_destination_current_location:
				searchLocation(REQUEST_DESTINATION_CURRENT_LOCATION);
				break;
			/*case R.id.ib_search_destination:
				searchLocation(REQUEST_DESTINATION_LOCATION);
				break;*/
			case R.id.ib_search_both:
				searchLocation(REQUEST_BOTH_LOCATIONS);
				break;
			case R.id.btn_booking:
				toggleVehicleOptions();
				break;
			case R.id.btn_booking_proceed:
				proceedVehicleBooking();
				break;
		}
	}

	@Override
	public void onBackPressed() {
		if (vehicle_types_shown) {
			if (currentVehicleTypePosition >= 0 && currentVehicleTypeSelected) {
				selectVehicleType(currentVehicleTypePosition);
			}
			toggleVehicleOptions();
		} else if (search_box) {
			toggleSearchBox();
		}/* else if (current_set) {
			current_set = false;
			markerOperations(MARKER_REMOVE_OPERATION, MARKER_CURRENT_LOCATION);
		}*/ else if (destination_set) {
			destination_set = false;
			polylineOperations(POLYLINE_REMOVE_OPERATION);
			markerOperations(MARKER_REMOVE_OPERATION, MARKER_DESTINATION_LOCATION, false);
			route_destination_city_name = route_distance_text = route_duration_text = "";
			route_distance = route_duration = 0;
			setRouteDetails(route_source_city_name, route_destination_city_name, route_distance_text, route_duration_text);
			actv_search_destination.setText("");
		} else if (source_set) {
			source_set = false;
			markerOperations(MARKER_REMOVE_OPERATION, MARKER_SOURCE_LOCATION, false);
			actv_search_source.setText("");
			if (route_details_shown) {
				toggleRouteDetails();
			}
		} else {
			rotationDetector.stop();
			startActivity(new Intent(activity, DashboardActivity.class));
			finish();
		}
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}

	@Override
	public void onDeviceRotation() {
		angleAzimuth = rotationDetector.getAzimuthAngle();
		if (!map_rotation) {
			googleMapUtils.rotateMarker(currentMarker, angleAzimuth);
		} else {
			googleMapUtils.updateCamera(travelMap, currentLatLng, angleAzimuth, 0f);
		}
	}

	private void configView() {

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");

		appCommonUtils = new AppCommonUtils(activity, context);
		fileOperationUtils = new FileOperationUtils(activity, context);
		googleMapUtils = new GoogleMapUtils(activity, context);
		locationUtils = new LocationUtils(activity, context);
		mathematicsUtils = new MathematicsUtils(activity, context);

		googleApiClient = new GoogleApiClient
				.Builder(context)
				.addApi(Places.GEO_DATA_API)
				.addApi(Places.PLACE_DETECTION_API)
				.enableAutoManage(this, this)
				.build();

		rotationDetector = new RotationDetector(activity, context);

		placeAutocompleteAdapter = new PlaceAutocompleteAdapter(activity, context, googleApiClient, INDIA_LAT_LNG_BOUNDS, null);

		ll_route_details = findViewById(R.id.ll_route_details);
		tv_source_city_name = findViewById(R.id.tv_source_city_name);
		tv_destination_city_name = findViewById(R.id.tv_destination_city_name);
		tv_route_distance_text = findViewById(R.id.tv_route_distance_text);
		tv_route_duration_text = findViewById(R.id.tv_route_duration_text);

		ib_zoom_in = findViewById(R.id.ib_zoom_in);
		ib_zoom_out = findViewById(R.id.ib_zoom_out);

		ib_search_box = findViewById(R.id.ib_search_box);
		ib_current_location = findViewById(R.id.ib_current_location);
		ll_search_box = findViewById(R.id.ll_search_box);

		actv_search_source = findViewById(R.id.actv_search_source);
		actv_search_source.setAdapter(placeAutocompleteAdapter);
		ib_source_current_location = findViewById(R.id.ib_source_current_location);
		ib_search_source = findViewById(R.id.ib_search_source);

		actv_search_destination = findViewById(R.id.actv_search_destination);
		actv_search_destination.setAdapter(placeAutocompleteAdapter);
		ib_destination_current_location = findViewById(R.id.ib_destination_current_location);
		ib_search_destination = findViewById(R.id.ib_search_destination);

		ib_search_both = findViewById(R.id.ib_search_both);

		btn_booking = findViewById(R.id.btn_booking);
		ll_booking_vehicle_types = findViewById(R.id.ll_booking_vehicle_types);
		btn_booking_proceed = findViewById(R.id.btn_booking_proceed);

		createVehicleTypeList();
		setRecyclerViewAdapter();

		rotationDetector.setRotationListener(this);

		ib_zoom_in.setOnClickListener(this);
		ib_zoom_out.setOnClickListener(this);

		ib_search_box.setOnClickListener(this);
		ib_current_location.setOnClickListener(this);

		ib_source_current_location.setOnClickListener(this);
		ib_search_source.setOnClickListener(this);
		ib_destination_current_location.setOnClickListener(this);
		ib_search_destination.setOnClickListener(this);
		ib_search_both.setOnClickListener(this);

		btn_booking.setOnClickListener(this);
		ll_booking_vehicle_types.setOnClickListener(this);

		btn_booking_proceed.setOnClickListener(this);
	}

	public void changeMapRotation() {
		map_rotation = !map_rotation;
		if (map_rotation) {
			googleMapUtils.updateCamera(travelMap, currentLatLng, angleAzimuth, 0f);
			googleMapUtils.rotateMarker(currentMarker, 0f);
			ib_current_location.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_compass));
		} else {
			googleMapUtils.rotateMarker(currentMarker, angleAzimuth);
			googleMapUtils.updateCamera(travelMap, currentLatLng, 0f, 0f);
			ib_current_location.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_location));
		}
	}

	private void setMarkerOptions(int markerType) {
		switch (markerType) {
			case REQUEST_CURRENT_LOCATION:
				markerBitmap = fileOperationUtils.createBitmapFromVector(R.drawable.ic_marker_current, 0, 0);
				currentLatitude = currentLatLng.latitude;
				currentLongitude = currentLatLng.longitude;
				currentMarkerOptions = new MarkerOptions();
				currentMarkerOptions.position(currentLatLng);
				currentMarkerOptions.anchor(0.5f, 0.5f);
				currentMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
				break;
			case REQUEST_SOURCE_LOCATION:
				markerBitmap = fileOperationUtils.createBitmapFromVector(R.drawable.ic_marker_source, 0, 0);
				sourceLatitude = sourceLatLng.latitude;
				sourceLongitude = sourceLatLng.longitude;
				sourceMarkerOptions = new MarkerOptions();
				sourceMarkerOptions.position(sourceLatLng);
				sourceMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
				break;
			case REQUEST_DESTINATION_LOCATION:
				markerBitmap = fileOperationUtils.createBitmapFromVector(R.drawable.ic_marker_destination, 0, 0);
				destinationLatitude = destinationLatLng.latitude;
				destinationLongitude = destinationLatLng.longitude;
				destinationMarkerOptions = new MarkerOptions();
				destinationMarkerOptions.position(destinationLatLng);
				destinationMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
				break;
		}
	}

	private void getCurrentLocation(int current_location_type, boolean zoomCamera) {
		try {
			currentLocation = locationUtils.getLocation();
			currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
			switch (current_location_type) {
				case REQUEST_CURRENT_LOCATION:
					current_set = true;
					setMarkerOptions(REQUEST_CURRENT_LOCATION);
					markerOperations(MARKER_ADD_OPERATION, MARKER_CURRENT_LOCATION, zoomCamera);
					break;
				case REQUEST_SOURCE_CURRENT_LOCATION:
					sourceLatLng = currentLatLng;
					source_set = true;
					setMarkerOptions(REQUEST_SOURCE_LOCATION);
					actv_search_source.setText(MY_LOCATION);
					break;
				case REQUEST_DESTINATION_CURRENT_LOCATION:
					destinationLatLng = currentLatLng;
					destination_set = true;
					setMarkerOptions(REQUEST_DESTINATION_LOCATION);
					actv_search_destination.setText(MY_LOCATION);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void searchLocation(int requestCode) {
		switch (requestCode) {
			case REQUEST_CURRENT_LOCATION:
				getCurrentLocation(REQUEST_CURRENT_LOCATION, true);
				break;
			case REQUEST_SOURCE_CURRENT_LOCATION:
				getCurrentLocation(REQUEST_SOURCE_CURRENT_LOCATION, false);
				break;
			case REQUEST_DESTINATION_CURRENT_LOCATION:
				getCurrentLocation(REQUEST_DESTINATION_CURRENT_LOCATION, false);
				break;
			/*case REQUEST_SOURCE_LOCATION:
				source_location = actv_search_source.getText().toString();
				if (source_location.isEmpty()) {
					appCommonUtils.appShowShortToast("Enter a valid Source");
				} else if (source_location.equals(MY_LOCATION)) {
					getCurrentLocation(REQUEST_SOURCE_CURRENT_LOCATION, false);
				} else {
					sourceLatLng = locationUtils.getLatLngFromAddress(source_location, 1);
					if (sourceLatLng != null) {
						source_set = true;
						setMarkerOptions(REQUEST_SOURCE_LOCATION);
						markerOperations(MARKER_ADD_OPERATION, MARKER_SOURCE_LOCATION, false);
					} else {
						appCommonUtils.appShowShortToast("No such location found!");
					}
				}
				break;
			case REQUEST_DESTINATION_LOCATION:
				destination_location = actv_search_destination.getText().toString();
				if (destination_location.isEmpty()) {
					appCommonUtils.appShowShortToast("Enter a valid Destination");
				} else if (destination_location.equals(MY_LOCATION)) {
					getCurrentLocation(REQUEST_DESTINATION_CURRENT_LOCATION, false);
				} else {
					destinationLatLng = locationUtils.getLatLngFromAddress(destination_location, 1);
					if (destinationLatLng != null) {
						destination_set = true;
						setMarkerOptions(REQUEST_DESTINATION_LOCATION);
						markerOperations(MARKER_ADD_OPERATION, MARKER_DESTINATION_LOCATION, false);
					} else {
						appCommonUtils.appShowShortToast("No such location found!");
					}
				}
				break;*/
			case REQUEST_BOTH_LOCATIONS:
				resetMapVariables();
				try {
					inputMethodManager.hideSoftInputFromWindow(actv_search_source.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
					inputMethodManager.hideSoftInputFromWindow(actv_search_destination.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
				} catch (Exception e) {
					e.printStackTrace();
				}
				source_location = actv_search_source.getText().toString();
				destination_location = actv_search_destination.getText().toString();
				if (source_location.isEmpty() || destination_location.isEmpty()) {
					if (source_location.isEmpty()) {
						appCommonUtils.appShowShortToast("Enter a valid Source");
					} else {
						appCommonUtils.appShowShortToast("Enter a valid Destination");
					}
				} else {
					if (source_location.equals(MY_LOCATION) || destination_location.equals(MY_LOCATION)) {
						if (source_location.equals(MY_LOCATION)) {
							getCurrentLocation(REQUEST_SOURCE_CURRENT_LOCATION, false);
							markerOperations(MARKER_ADD_OPERATION, MARKER_SOURCE_LOCATION, false);
							destinationLatLng = locationUtils.getLatLngFromAddress(destination_location, 1);
							if (destinationLatLng != null) {
								destination_set = true;
								setMarkerOptions(REQUEST_DESTINATION_LOCATION);
								markerOperations(MARKER_ADD_OPERATION, MARKER_DESTINATION_LOCATION, false);
							} else {
								appCommonUtils.appShowShortToast("No such location found!");
							}
							if (source_set && destination_set) {
								searchRoute();
							}
						} else {
							getCurrentLocation(REQUEST_DESTINATION_CURRENT_LOCATION, false);
							markerOperations(MARKER_ADD_OPERATION, MARKER_DESTINATION_LOCATION, false);
							sourceLatLng = locationUtils.getLatLngFromAddress(source_location, 1);
							if (sourceLatLng != null) {
								source_set = true;
								setMarkerOptions(REQUEST_SOURCE_LOCATION);
								markerOperations(MARKER_ADD_OPERATION, MARKER_SOURCE_LOCATION, false);
							} else {
								appCommonUtils.appShowShortToast("No such location found!");
							}
							if (source_set && destination_set) {
								searchRoute();
							}
						}
					} else {
						sourceLatLng = locationUtils.getLatLngFromAddress(source_location, 1);
						if (sourceLatLng != null) {
							source_set = true;
							setMarkerOptions(REQUEST_SOURCE_LOCATION);
							markerOperations(MARKER_ADD_OPERATION, MARKER_SOURCE_LOCATION, false);
						} else {
							appCommonUtils.appShowShortToast("No such location found!");
						}
						destinationLatLng = locationUtils.getLatLngFromAddress(destination_location, 1);
						if (destinationLatLng != null) {
							destination_set = true;
							setMarkerOptions(REQUEST_DESTINATION_LOCATION);
							markerOperations(MARKER_ADD_OPERATION, MARKER_DESTINATION_LOCATION, false);
						} else {
							appCommonUtils.appShowShortToast("No such location found!");
						}
						if (source_set && destination_set) {
							searchRoute();
						}
					}
				}
				break;
		}
	}

	private void resetMapVariables() {
		googleMapUtils.clearMap(travelMap);
		source_set = false;
		sourceLatLng = null;
		sourceLatitude = null;
		sourceLongitude = null;
		sourceMarker = null;
		sourceMarkerOptions = null;
		destination_set = false;
		destinationLatLng = null;
		destinationLatitude = null;
		destinationLongitude = null;
		destinationMarker = null;
		destinationMarkerOptions = null;
		routePolyline = null;
		routePolylineOptions = null;
	}

	private void searchRoute() {

		route_details_shown = true;
		toggleRouteDetails();

		String url = locationUtils.getRequestedUrl(sourceLatLng, destinationLatLng);

		route_source_city_name = locationUtils.getLocationAddress(sourceLatLng, false);
		route_destination_city_name = locationUtils.getLocationAddress(destinationLatLng, false);

		Call<MapRootObject> mapDirectionsCall = api.getMapDirections(url);
		mapDirectionsCall.enqueue(new Callback<MapRootObject>() {
			@Override
			public void onResponse(Call<MapRootObject> call, Response<MapRootObject> response) {
				if (response.isSuccessful()) {
					mapRootObject = response.body();
					routeSections = getMapRoute(mapRootObject);
					drawPolyLineOnMap(routeSections);
					toggleRouteDetails();
				} else {
					appCommonUtils.appShowShortToast(" Error! Please try again! ");
				}
			}

			@Override
			public void onFailure(Call<MapRootObject> call, Throwable t) {
				appCommonUtils.appShowShortToast(" Can't connect to Server! Please check your Network Connection! ");
			}
		});

		routeLatLngBounds = locationUtils.findRouteLatLangBounds(sourceLatLng, destinationLatLng);
		routeCameraUpdate = CameraUpdateFactory.newLatLngBounds(routeLatLngBounds, DEFAULT_MAP_ROUTE_PADDING);
		travelMap.animateCamera(routeCameraUpdate);
		if (search_box) {
			toggleSearchBox();
		}
	}

	private List<List<LatLng>> getMapRoute(MapRootObject mapRootObject) {
		routeSections = new ArrayList<List<LatLng>>();
		sectionLatLngs = new ArrayList<LatLng>();
		Route[] routes = mapRootObject.getRoutes();
		for (Route route : routes) {
			Leg[] legs = route.getLegs();
			for (Leg leg : legs) {
				Distance distance = leg.getDistance();
				route_distance_text = distance.getText();
				route_distance = distance.getValue();
				Duration duration = leg.getDuration();
				route_duration_text = duration.getText();
				route_duration = duration.getValue();
				Step[] steps = leg.getSteps();
				for (Step step : steps) {
					Polyline polyline = step.getPolyline();
					String point = polyline.getPoints();

					List list = locationUtils.decodePoly(point);
					for (Object latLng : list) {
						sectionLatLngs.add(latLng);
					}
				}
				routeSections.add(sectionLatLngs);
			}
		}
		return routeSections;
	}

	private void drawPolyLineOnMap(List<List<LatLng>> routeSections) {
		routePolylineOptions = googleMapUtils.drawPolyLine(routeSections);
		if (routePolylineOptions != null) {
			polylineOperations(POLYLINE_ADD_OPERATION);
		}
	}

	private void toggleSearchBox() {
		search_box = !search_box;
		if (search_box) {
			if (route_details_shown) {
				ll_route_details.setVisibility(View.GONE);
			}
			ib_search_box.setVisibility(View.GONE);
			ll_search_box.setVisibility(View.VISIBLE);
		} else {
			if (route_details_shown) {
				ll_route_details.setVisibility(View.VISIBLE);
			}
			ib_search_box.setVisibility(View.VISIBLE);
			ll_search_box.setVisibility(View.GONE);
		}
	}

	private void toggleRouteDetails() {
		route_details_shown = !route_details_shown;
		if (route_details_shown) {
			ll_route_details.setVisibility(View.VISIBLE);
			setRouteDetails(route_source_city_name, route_destination_city_name, route_distance_text, route_duration_text);
		} else {
			ll_route_details.setVisibility(View.GONE);
			route_source_city_name = route_destination_city_name = route_distance_text = route_duration_text = "";
			route_distance = route_duration = 0;
			setRouteDetails(route_source_city_name, route_destination_city_name, route_distance_text, route_duration_text);
		}
	}

	private void toggleVehicleOptions() {
		vehicle_types_shown = !vehicle_types_shown;
		if (vehicle_types_shown) {
			btn_booking.setVisibility(View.GONE);
			ll_booking_vehicle_types.setVisibility(View.VISIBLE);
		} else {
			btn_booking.setVisibility(View.VISIBLE);
			ll_booking_vehicle_types.setVisibility(View.GONE);
			resetBookingVariables();
		}
	}

	private void setRouteDetails(String route_source_city_name, String route_destination_city_name, String route_distance_text, String route_duration_text) {
		tv_source_city_name.setText(route_source_city_name);
		tv_destination_city_name.setText(route_destination_city_name);
		tv_route_distance_text.setText(route_distance_text);
		tv_route_distance_text.setSelected(true);
		tv_route_duration_text.setText(route_duration_text);
		tv_route_duration_text.setSelected(true);
	}

	private void markerOperations(int markerOperationType, int markerType, boolean zoom) {
		switch (markerOperationType) {
			case MARKER_ADD_OPERATION:
				switch (markerType) {
					case MARKER_CURRENT_LOCATION:
						if (currentMarker != null) {
							currentMarker.remove();
							currentMarker = null;
						}
						currentMarker = travelMap.addMarker(currentMarkerOptions);
						if (zoom) {
							travelMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_MAP_CAMERA_ZOOM), DEFAULT_MAP_CAMERA_ZOOM_DURATION, null);
						}
						break;
					case MARKER_SOURCE_LOCATION:
						sourceMarker = travelMap.addMarker(sourceMarkerOptions);
						break;
					case MARKER_DESTINATION_LOCATION:
						destinationMarker = travelMap.addMarker(destinationMarkerOptions);
						break;
					case MARKER_TAXI_LOCATION:
						break;
				}
				break;
			case MARKER_REMOVE_OPERATION:
				switch (markerType) {
					case MARKER_CURRENT_LOCATION:
						if (currentMarker != null) {
							currentMarker.remove();
							currentMarker = null;
						}
						currentMarkerOptions = null;
						if (routeLatLngBounds != null) {
							routeCameraUpdate = CameraUpdateFactory.newLatLngBounds(routeLatLngBounds, DEFAULT_MAP_ROUTE_PADDING);
							travelMap.animateCamera(routeCameraUpdate);
						}
						break;
					case MARKER_SOURCE_LOCATION:
						sourceLatLng = null;
						sourceLatitude = null;
						sourceLongitude = null;
						if (sourceMarker != null) {
							sourceMarker.remove();
							sourceMarker = null;
						}
						sourceMarkerOptions = null;
						break;
					case MARKER_DESTINATION_LOCATION:
						destinationLatLng = null;
						destinationLatitude = null;
						destinationLongitude = null;
						if (destinationMarker != null) {
							destinationMarker.remove();
							destinationMarker = null;
						}
						destinationMarkerOptions = null;
						break;
					case MARKER_TAXI_LOCATION:
						break;
				}
				break;
		}
	}

	private void polylineOperations(int markerOperationType) {
		switch (markerOperationType) {
			case POLYLINE_ADD_OPERATION:
				routePolyline = travelMap.addPolyline(routePolylineOptions);
				break;
			case POLYLINE_REMOVE_OPERATION:
				if (routePolyline != null) {
					routePolyline.remove();
					routePolyline = null;
				}
				routePolylineOptions = null;
				routeLatLngBounds = null;
				routeCameraUpdate = null;
				break;
		}
	}

	private void createVehicleTypeList() {
		vehicleTypeList = new ArrayList<>();
		vehicleTypeList.add(new VehicleType(1, "Auto", 20, 35, 3, "Description", R.drawable.ic_type_auto, 1, "", "", 1));
		vehicleTypeList.add(new VehicleType(2, "Sedan", 30, 45, 4, "Description", R.drawable.ic_type_sedan, 1, "", "", 1));
		vehicleTypeList.add(new VehicleType(3, "SUV", 40, 60, 7, "Description", R.drawable.ic_type_suv, 1, "", "", 1));
		vehicleTypeList.add(new VehicleType(4, "Tempo", 50, 75, 25, "Description", R.drawable.ic_type_tempo, 1, "", "", 1));
		vehicleTypeList.add(new VehicleType(5, "Bus", 60, 100, 55, "Description", R.drawable.ic_type_bus, 1, "", "", 1));
	}

	private void setRecyclerViewAdapter() {
		rv_booking_vehicle_type_buttons = findViewById(R.id.rv_booking_vehicle_type_buttons);
		rv_booking_vehicle_type_buttons.setHasFixedSize(true);
		vehicleRecyclerViewLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
		vehicleRecyclerViewLayoutManager.scrollToPosition(0);
		rv_booking_vehicle_type_buttons.setLayoutManager(vehicleRecyclerViewLayoutManager);
		vehicleTypeAdapter = new VehicleTypeAdapter(context, vehicleTypeList);
		rv_booking_vehicle_type_buttons.setAdapter(vehicleTypeAdapter);
		vehicleTypeAdapter.setOnItemClickListener(position -> {
			vehicleRecyclerViewLayoutManager.scrollToPosition(position);
			selectVehicleType(position);
		});
	}

	private void selectVehicleType(int position) {
		currentVehicleTypePosition = position;
		if (previousVehicleTypePosition != currentVehicleTypePosition) {
			currentVehicleTypeSelected = true;
			if (previousVehicleTypePosition >= 0) {
				vehicleTypeList.get(previousVehicleTypePosition).setVehicleTypeSelected(false);
			}
		} else {
			currentVehicleTypeSelected = !currentVehicleTypeSelected;
		}
		previousVehicleTypePosition = currentVehicleTypePosition;
		vehicleTypeList.get(currentVehicleTypePosition).setVehicleTypeSelected(currentVehicleTypeSelected);
		vehicleTypeAdapter.notifyDataSetChanged();
	}

	private void resetBookingVariables() {
		currentVehicleTypeSelected = false;
		currentVehicleTypePosition = -1;
		previousVehicleTypePosition = -1;
	}

	private void proceedVehicleBooking() {
		/*if (!source_set) {
			appCommonUtils.appShowShortToast("Select a Source Point..!!");
		} else if (!destination_set) {
			appCommonUtils.appShowShortToast("Select an Destination Point..!!");
		}*/
		if (!source_set || !destination_set) {
			appCommonUtils.appShowShortToast("You have not set a Journey Route..!!");
		} else if (!currentVehicleTypeSelected) {
			appCommonUtils.appShowShortToast("Select a Vehicle Type..!!");
		} else {
			rotationDetector.stop();
			startActivity(new Intent(activity, NavigationActivity.class)
					.putExtra("CURRENT_LATLNG", currentLatLng)
					.putExtra("SOURCE_LATLNG", sourceLatLng)
					.putExtra("DESTINATION_LATLNG", destinationLatLng)
					.putExtra("ROUTE_POLYLINE_OPTIONS", routePolylineOptions)
			);
			resetMapVariables();
			toggleVehicleOptions();
			//finish();
		}
	}
}