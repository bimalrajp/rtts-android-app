package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Step implements Parcelable {

	private Distance2 distance;
	private Duration2 duration;
	private EndLocation2 end_location;
	private String html_instructions;
	private Polyline polyline;
	private StartLocation2 start_location;
	private String travel_mode;
	private String maneuver;

	protected Step(Parcel in) {
		distance = in.readParcelable(Distance2.class.getClassLoader());
		duration = in.readParcelable(Duration2.class.getClassLoader());
		end_location = in.readParcelable(EndLocation2.class.getClassLoader());
		html_instructions = in.readString();
		polyline = in.readParcelable(Polyline.class.getClassLoader());
		start_location = in.readParcelable(StartLocation2.class.getClassLoader());
		travel_mode = in.readString();
		maneuver = in.readString();
	}

	public static final Creator<Step> CREATOR = new Creator<Step>() {
		@Override
		public Step createFromParcel(Parcel in) {
			return new Step(in);
		}

		@Override
		public Step[] newArray(int size) {
			return new Step[size];
		}
	};

	public Distance2 getDistance() {
		return distance;
	}
	public void setDistance(Distance2 distance) {
		this.distance = distance;
	}
	public Duration2 getDuration() {
		return duration;
	}
	public void setDuration(Duration2 duration) {
		this.duration = duration;
	}
	public EndLocation2 getEnd_location() {
		return end_location;
	}
	public void setEnd_location(EndLocation2 end_location) {
		this.end_location = end_location;
	}
	public String getHtml_instructions() {
		return html_instructions;
	}
	public void setHtml_instructions(String html_instructions) {
		this.html_instructions = html_instructions;
	}
	public Polyline getPolyline() {
		return polyline;
	}
	public void setPolyline(Polyline polyline) {
		this.polyline = polyline;
	}
	public StartLocation2 getStart_location() {
		return start_location;
	}
	public void setStart_location(StartLocation2 start_location) {
		this.start_location = start_location;
	}
	public String getTravel_mode() {
		return travel_mode;
	}
	public void setTravel_mode(String travel_mode) {
		this.travel_mode = travel_mode;
	}
	public String getManeuver() {
		return maneuver;
	}
	public void setManeuver(String maneuver) {
		this.maneuver = maneuver;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(distance, i);
		parcel.writeParcelable(duration, i);
		parcel.writeParcelable(end_location, i);
		parcel.writeString(html_instructions);
		parcel.writeParcelable(polyline, i);
		parcel.writeParcelable(start_location, i);
		parcel.writeString(travel_mode);
		parcel.writeString(maneuver);
	}
}
