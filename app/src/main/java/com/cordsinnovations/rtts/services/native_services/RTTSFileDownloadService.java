package com.cordsinnovations.rtts.services.native_services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import com.cordsinnovations.rtts.globals.Constants.HTTP;
import com.cordsinnovations.rtts.globals.Constants.FOLDERS_ONLINE;
import com.cordsinnovations.rtts.globals.Constants.FILES_APP;

import static com.cordsinnovations.rtts.globals.Constants.PUT_EXTRA_KEYS.DOWNLOAD_FILE_PUT_EXTRA_KEYS.*;
import static com.cordsinnovations.rtts.globals.Constants.FILE_DOWNLOAD_TYPES.*;

import static com.cordsinnovations.rtts.services.native_services.RTTSFileDownloadService.SERVICE_COMMONS.http;

/**
 * Created by Bimal on 04-03-2017.
 */

public class RTTSFileDownloadService extends Service {

	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 */

	static class SERVICE_COMMONS {
		static AsyncHttpClient http;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		http = new AsyncHttpClient();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		String fileName = intent.getStringExtra(PUT_EXTRA_KEY_DOWNLOAD_FILE_NAME);
		int fileType = intent.getIntExtra(PUT_EXTRA_KEY_DOWNLOAD_FILE_TYPE, 0);
		downloadFile(fileType, fileName);
		return START_STICKY;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public void downloadFile(int fileType, String fileName) {
		String tempFileUrl = null;
		File downloadFile;
		if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
			switch (fileType) {
				case FILE_DOWNLOAD_IMAGE_CUSTOMER:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_CUSTOMER + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_IMAGE_PROOF:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_PROOF + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_PROOF, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_IMAGE_SOS:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_ACCIDENT + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_SOS, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_IMAGE_ACCIDENT:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_SOS + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_ACCIDENT, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_IMAGE_BREAKDOWN:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_BREAKDOWN + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_BREAKDOWN, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_IMAGE_OTHER:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_IMAGE_OTHER + fileName;
					downloadFile = new File(FILES_APP.FOLDER_IMAGE_OTHER, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_DOCUMENT_PROOF:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_DOCUMENT_PROOF + fileName;
					downloadFile = new File(FILES_APP.FOLDER_DOCUMENT_PROOF, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
				case FILE_DOWNLOAD_DOCUMENT_OTHER:
					tempFileUrl = HTTP.BASE_URL + FOLDERS_ONLINE.FOLDER_DOCUMENT_OTHER + fileName;
					downloadFile = new File(FILES_APP.FOLDER_DOCUMENT_OTHER, fileName);
					if (!downloadFile.exists()) {
						fetchDownloadFile(tempFileUrl, fileType, fileName);
					}
					break;
			}
		}
	}

	private void fetchDownloadFile(String url, int fileType, String fileName) {
		final File downloadFile = makeDownloadFile(fileType, fileName);
		http.get(url, new FileAsyncHttpResponseHandler(downloadFile) {
			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, File file) {
				stopSelf();
			}
		});
	}

	private static File makeDownloadFile(int fileType, String fileName) {
		File downloadFile = null;
		switch (fileType) {
			case FILE_DOWNLOAD_IMAGE_CUSTOMER:
				FILES_APP.FOLDER_IMAGE_CUSTOMER.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, fileName);
				break;
			case FILE_DOWNLOAD_IMAGE_PROOF:
				FILES_APP.FOLDER_IMAGE_PROOF.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_PROOF, fileName);
				break;
			case FILE_DOWNLOAD_IMAGE_SOS:
				FILES_APP.FOLDER_IMAGE_SOS.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_SOS, fileName);
				break;
			case FILE_DOWNLOAD_IMAGE_ACCIDENT:
				FILES_APP.FOLDER_IMAGE_ACCIDENT.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_ACCIDENT, fileName);
				break;
			case FILE_DOWNLOAD_IMAGE_BREAKDOWN:
				FILES_APP.FOLDER_IMAGE_BREAKDOWN.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_BREAKDOWN, fileName);
				break;
			case FILE_DOWNLOAD_IMAGE_OTHER:
				FILES_APP.FOLDER_IMAGE_OTHER.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_IMAGE_OTHER, fileName);
				break;
			case FILE_DOWNLOAD_DOCUMENT_PROOF:
				FILES_APP.FOLDER_DOCUMENT_PROOF.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_DOCUMENT_PROOF, fileName);
				break;
			case FILE_DOWNLOAD_DOCUMENT_OTHER:
				FILES_APP.FOLDER_DOCUMENT_OTHER.mkdirs();
				downloadFile = new File(FILES_APP.FOLDER_DOCUMENT_OTHER, fileName);
				break;
		}
		return downloadFile;
	}
}
