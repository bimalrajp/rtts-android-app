package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Duration implements Parcelable {

	public static final Creator<Duration> CREATOR = new Creator<Duration>() {
		@Override
		public Duration createFromParcel(Parcel in) {
			return new Duration(in);
		}

		@Override
		public Duration[] newArray(int size) {
			return new Duration[size];
		}
	};

	private String text;
	private int value;

	protected Duration(Parcel in) {
		text = in.readString();
		value = in.readInt();
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Duration [text = " + text + ", value = " + value + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(text);
		parcel.writeInt(value);
	}
}
