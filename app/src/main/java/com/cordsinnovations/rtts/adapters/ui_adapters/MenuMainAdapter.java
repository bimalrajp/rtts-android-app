package com.cordsinnovations.rtts.adapters.ui_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;

public class MenuMainAdapter extends BaseAdapter {

	Context context;
	String Titles[];
	String Descreptions[];
	LayoutInflater layoutInflater;

	public MenuMainAdapter(Context context, String[] titles, String[] descreptions) {
		this.context = context;
		Titles = titles;
		Descreptions = descreptions;
		layoutInflater = (LayoutInflater.from(context));
	}

	@Override
	public int getCount() {
		return Titles.length;
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		view = layoutInflater.inflate(R.layout.layout_settings, null);
		TextView tv_settings_item_title = view.findViewById(R.id.tv_settings_item_title);
		TextView tv_settings_item_description = view.findViewById(R.id.tv_settings_item_description);
		tv_settings_item_title.setText(Titles[position]);
		tv_settings_item_description.setText(Descreptions[position]);
		return view;
	}
}
