package com.cordsinnovations.rtts.hardware;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_SHAKE_CONSTANTS.DEVICE_SENSOR_CONSTANTS.*;

import static com.cordsinnovations.rtts.hardware.ShakeDetector.SENSOR_COMMONS.*;
import static com.cordsinnovations.rtts.hardware.ShakeDetector.SENSOR_VARIABLES.*;

public class ShakeDetector implements SensorEventListener {

	static class SENSOR_COMMONS {
		static Context context;
		static SensorManager sensorManager;
		static Sensor sensorAccelerometer;
		static OnShakeListener shakeListener;
	}

	static class SENSOR_VARIABLES {
		static float mLastX = -1.0f, mLastY = -1.0f, mLastZ = -1.0f;
		static long mLastTime, mLastShake, mLastForce;
		static int mShakeCount = 0;
	}

	public interface OnShakeListener {
		void onDeviceShake();
	}

	public ShakeDetector(Context context) {
		SENSOR_COMMONS.context = context;
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (sensorAccelerometer == null) {
			Log.e(SENSOR_TAG, "No accelerometer sensors found");
		}
	}

	public void start() {
		sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void stop() {
		sensorManager.unregisterListener(this, sensorAccelerometer);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		long now = System.currentTimeMillis();

		if ((now - mLastForce) > SHAKE_TIMEOUT) {
			mShakeCount = 0;
		}

		if ((now - mLastTime) > TIME_THRESHOLD) {
			long diff = now - mLastTime;
			float speed = Math.abs(event.values[0] + event.values[1] + event.values[2] - mLastX - mLastY - mLastZ) / diff * 10000;
			if (speed > FORCE_THRESHOLD) {
				if ((++mShakeCount >= SHAKE_COUNT) && (now - mLastShake > SHAKE_DURATION)) {
					mLastShake = now;
					mShakeCount = 0;
					if (shakeListener != null) {
						shakeListener.onDeviceShake();
					}
				}
				mLastForce = now;
			}
			mLastTime = now;
			mLastX = event.values[0];
			mLastY = event.values[1];
			mLastZ = event.values[2];
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	public void setShakeListener(OnShakeListener shakeListener) {
		SENSOR_COMMONS.shakeListener = shakeListener;
	}
}
