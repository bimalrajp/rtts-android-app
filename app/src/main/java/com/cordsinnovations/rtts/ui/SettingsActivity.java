package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.adapters.ui_adapters.AdsAdapter;
import com.cordsinnovations.rtts.adapters.ui_adapters.MenuMainAdapter;
import com.cordsinnovations.rtts.adapters.ui_adapters.MenuMapStyleAdapter;
import com.cordsinnovations.rtts.controllers.ui_controllers.AdsController;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.CustomerUtils;
import com.cordsinnovations.rtts.utils.LocationUtils;
import com.cordsinnovations.rtts.utils.PermissionRequestUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.MAP_STYLE_IDS_ARRAY;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.MAP_STYLE_IMAGES_ARRAY;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.MAP_STYLE_TITLES_ARRAY;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.SETTINGS_MENU_TITLES_ARRAY;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.SETTINGS_MENU_DESCRIPTIONS_ARRAY;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.SETTINGS_MENU_TITLE_ELEMENTS.*;

import static com.cordsinnovations.rtts.ui.SettingsActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.SettingsActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.SettingsActivity.UI_ELEMENTS.MAP_STYLES_MENU.*;
import static com.cordsinnovations.rtts.ui.SettingsActivity.UI_ELEMENTS.UI_BOOLEANS.*;
import static com.cordsinnovations.rtts.ui.SettingsActivity.UI_ELEMENTS.MAIN_SECTION.MAIN_MENU.*;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static Typeface typeFace;
		static InputMethodManager inputMethodManager = null;
	}

	static class ACTIVITY_UTILS {
		static PermissionRequestUtils permissionRequestUtils;
		static AppCommonUtils appCommonUtils;
		static CustomerUtils customerUtils;
		static LocationUtils locationUtils;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
		}

		static class UI_BOOLEANS {
			static boolean appThemesOptionsOpen = false;
			static boolean mapStylesOptionsOpen = false;
		}

		static class UI_ALERTS {
			static AlertDialog.Builder builder;
			static AlertDialog alertDialog;
			static View alertDialogView;
		}

		static class MAIN_SECTION {

			static class MAIN_MENU {
				static MenuMainAdapter menuMainAdapter;
				static ListView lv_settings;
				static String selectedMenuItemTitle;
				static int selectedMenuItemPosition;
			}
		}

		static class MAP_STYLES_MENU {
			static int savedMapStylePosition, selectedMapStylePosition;
			static View savedMapStyleView, selectedMapStyleView;
			static MenuMapStyleAdapter menuMapStyleAdapter;
			static LinearLayout ll_map_styles;
			static ListView lv_map_styles;
			static Button btn_save_map_style;
		}

		static class ADS_SECTION {
			static AdsAdapter adsAdapter;
			static AdsController adsController;
			static CardView cv_ads_box;
			static FrameLayout fl_ads_view;
			static ViewPager vp_ads_view;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		configView();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_save_map_style:
				saveMapStyle();
				break;
		}
	}

	@Override
	public void onBackPressed() {
		if (mapStylesOptionsOpen) {
			mapStylesOptionsOpen = false;
			setActivityTitle(0);
			try {
				if (savedMapStylePosition != selectedMapStylePosition) {
					selectedMapStyleView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
					savedMapStyleView.setBackground(context.getResources().getDrawable(R.drawable.app_selected_item));
					savedMapStylePosition = selectedMapStylePosition = sharedPreferences.getInt(PreferenceKeys.SETTINGS_MAP_STYLE_POSITION, 0);
				}
			} catch (Exception e) {
				//e.printStackTrace();
			}
			lv_settings.setVisibility(View.VISIBLE);
			ll_map_styles.setVisibility(View.GONE);
		} else {
			startActivity(new Intent(activity, DashboardActivity.class));
			finish();
		}
	}

	void configView() {

		this.getWindow().setStatusBarColor(getColor(R.color.colorPrimaryDark));

		//getSupportActionBar().hide();

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");

		appCommonUtils = new AppCommonUtils(activity, context);

		lv_settings = findViewById(R.id.lv_settings);
		menuMainAdapter = new MenuMainAdapter(context, SETTINGS_MENU_TITLES_ARRAY, SETTINGS_MENU_DESCRIPTIONS_ARRAY);
		lv_settings.setAdapter(menuMainAdapter);
		lv_settings.setOnItemClickListener((adapterView, view, position, l) -> {
			selectedMenuItemPosition = position;
			selectedMenuItemTitle = SETTINGS_MENU_TITLES_ARRAY[position];
			displayMenuOptions(selectedMenuItemTitle);
		});

		ll_map_styles = findViewById(R.id.ll_map_styles);
		lv_map_styles = findViewById(R.id.lv_map_styles);
		savedMapStylePosition = selectedMapStylePosition = sharedPreferences.getInt(PreferenceKeys.SETTINGS_MAP_STYLE_POSITION, 0);
		menuMapStyleAdapter = new MenuMapStyleAdapter(context, MAP_STYLE_TITLES_ARRAY, MAP_STYLE_IMAGES_ARRAY, selectedMapStylePosition);
		lv_map_styles.setAdapter(menuMapStyleAdapter);
		lv_map_styles.setOnItemClickListener((adapterView, view, position, l) -> {
			try {
				savedMapStyleView = adapterView.getChildAt(selectedMapStylePosition);
				if (selectedMapStylePosition != position) {
					Log.d("Selected Map Style Position", selectedMapStylePosition + "");
					selectedMapStyleView = adapterView.getChildAt(selectedMapStylePosition);
					selectedMapStyleView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
				}
				selectedMapStyleView = view;
				selectedMapStyleView.setBackground(context.getResources().getDrawable(R.drawable.app_selected_item));
				selectedMapStylePosition = position;
			} catch (Exception e) {
				//e.printStackTrace();
			}
		});
		btn_save_map_style = findViewById(R.id.btn_save_map_style);
		btn_save_map_style.setOnClickListener(this);

	}

	void displayMenuOptions(String selectedMenuItemTitle) {
		switch (selectedMenuItemTitle) {
			case SETTINGS_MENU_TITLE_1:
				break;
			case SETTINGS_MENU_TITLE_2:
				mapStylesOptionsOpen = true;
				setActivityTitle(2);
				lv_settings.setVisibility(View.GONE);
				ll_map_styles.setVisibility(View.VISIBLE);
				break;
		}
	}

	void setActivityTitle(int option) {
		switch (option) {
			case 0:
				activity.setTitle(context.getResources().getString(R.string.title_activity_settings));
				break;
			case 1:
				activity.setTitle(SETTINGS_MENU_TITLE_1);
				break;
			case 2:
				activity.setTitle(SETTINGS_MENU_TITLE_2);
				break;
		}
	}

	void saveMapStyle() {
		sharedPreferences.edit()
				.putInt(PreferenceKeys.SETTINGS_MAP_STYLE_POSITION, selectedMapStylePosition)
				.putInt(PreferenceKeys.SETTINGS_MAP_STYLE_ID, MAP_STYLE_IDS_ARRAY[selectedMapStylePosition])
				.apply();
		savedMapStylePosition = selectedMapStylePosition;
		mapStylesOptionsOpen = false;
		setActivityTitle(0);
		lv_settings.setVisibility(View.VISIBLE);
		ll_map_styles.setVisibility(View.GONE);
		appCommonUtils.appShowShortToast("Selected map style saved successfully.");
	}
}