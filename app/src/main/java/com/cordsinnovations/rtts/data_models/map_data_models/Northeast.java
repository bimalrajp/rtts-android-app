package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Northeast implements Parcelable {

	public static final Creator<Northeast> CREATOR = new Creator<Northeast>() {
		@Override
		public Northeast createFromParcel(Parcel in) {
			return new Northeast(in);
		}

		@Override
		public Northeast[] newArray(int size) {
			return new Northeast[size];
		}
	};

	private double lat;
	private double lng;

	protected Northeast(Parcel in) {
		lat = in.readDouble();
		lng = in.readDouble();
	}

	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "Northeast [lat = " + lat + ", lng = " + lng + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeDouble(lat);
		parcel.writeDouble(lng);
	}
}
