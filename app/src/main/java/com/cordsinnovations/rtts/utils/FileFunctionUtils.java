package com.cordsinnovations.rtts.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.RequiresApi;

import java.net.URISyntaxException;

/**
 * Created by Bimal on 21-02-2017.
 */

public class FileFunctionUtils {

	public static String getPath(Context context, Uri uri) throws URISyntaxException {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = {"_data"};
			Cursor cursor = null;
			try {
				cursor = context.getContentResolver().query(uri, projection, null, null, null);
				int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception e) {
				// Eat it
			}
		} else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}
		return null;
	}
}


    /*@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case Constants.FILE_OPERATIONS.ACTIVITY_CAPTURE_IMAGE:
                try {
                    if (cache.exists()) {
                        Log.d("File Path", cache.getAbsolutePath());
                        msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                        scaledBm = fIleOperations.resizeBitmap(msgImageBitmap);
                        //Bitmap scaledBm = getBitmap(cache.getAbsolutePath());
                        *//*txt_image_preview.setText(fileName);*//*
                        //showImagePreview(rotateBitmap(scaledBm, -90));
                        fIleOperations.showImagePreview(scaledBm);
                        isFileSelected = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CHOOSE_IMAGE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = appFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    fIleOperations.getMimeType(fileName);
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_IMAGE_JPEG && fileType != Constants.FILE_TYPES.FILE_IMAGE_JPG && fileType != Constants.FILE_TYPES.FILE_IMAGE_PNG) {
                        Toast.makeText(context, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        fIleOperations.getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_IMAGE_SIZE) {
							*//*ll_image_picker.setVisibility(View.GONE);
							fl_image_preview.setVisibility(View.VISIBLE);
							txt_image_preview.setText(fileName);*//*
                            cache = new File(Constants.FILES_APP.FOLDER_RTTS_CACHE, fileName);
                            try {
                                fIleOperations.copyFile(filePath, cache, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (cache.exists()) {
                                msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
                                scaledBm = fIleOperations.resizeBitmap(msgImageBitmap);
                                fIleOperations.showImagePreview(scaledBm);
                            }
                        } else {
                            Toast.makeText(context, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_IMAGE_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CAPTURE_VIDEO:
                try {
                    if (cache.exists()) {
						*//*ll_video_picker.setVisibility(View.GONE);
						fl_video_preview.setVisibility(View.VISIBLE);
						txt_video_preview.setText(fileName);*//*
                        isFileSelected = true;
                        //getMimeType(fileName);
                        filePath = cache.getAbsolutePath();
                        Log.d("File Path", filePath);
                    }
                    if (isFileSelected) {
                        MediaController controller = new MediaController(this);
                        controller.setAnchorView(null);
                        controller.setMediaPlayer(null);
                        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
                        int width = thumbnail.getWidth();
                        int height = thumbnail.getHeight();
                        int targetWidth;
                        int targetHeight;
                        float scaleFactor;
                        if (width > height) {
                            scaleFactor = ((float) 250) / ((float) width);
                            targetWidth = 250;
                            targetHeight = (int) (height * scaleFactor);
                        } else {
                            scaleFactor = ((float) 250) / ((float) height);
                            targetHeight = 250;
                            targetWidth = (int) (width * scaleFactor);
                        }
                        fIleOperations.videoParameters(null, targetHeight, targetWidth);
						*//*vid_video_preview.setMediaController(controller);
						vid_video_preview.setVideoPath(filePath);
						vid_video_preview.start();*//*
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CHOOSE_VIDEO:
                if (resultCode == RESULT_OK) {
                    boolean isContentType = true;
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Uri tempFileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        isContentType = true;
                        ContentResolver cR = this.getContentResolver();
                        fileType = cR.getType(fileUri);
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            tempFileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    } else {
                        isContentType = false;
                    }
                    Log.d("File Uri", "" + tempFileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = appFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = tempFileUri.getLastPathSegment();
                    //fileName = fileName.replace(" ", "");
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    if (!isContentType) {
                        fIleOperations.getMimeType(fileName);
                    }
                    Log.d("File Mime", fileType);
                    *//*if (chooseFileType != Constants.FILE_TYPES.FILE_VIDEO_MP4 && chooseFileType != Constants.FILE_TYPES.FILE_VIDEO_3GP && chooseFileType != Constants.FILE_TYPES.FILE_VIDEO_3GPP && chooseFileType != Constants.FILE_TYPES.FILE_VIDEO_AVI) {
                        Snackbar.make(activity_chat, "Sorry..!! Wrong video type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {*//*
                    // Get the size of the selected file
                    fIleOperations.getFileSize(filePath);
                    Log.d("File Size", fileSize + "");
                    if (fileSize < Constants.FILE_SIZES.FILE_VIDEO_SIZE) {
						*//*ll_video_picker.setVisibility(View.GONE);
						fl_video_preview.setVisibility(View.VISIBLE);
						txt_video_preview.setText(fileName);*//*
                        cache = new File(Constants.FILES_APP.FOLDER_RTTS_CACHE, "VIDEO." + fileType.replace("video/", ""));
                        try {
                            fIleOperations.copyFile(filePath, cache, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (isFileSelected) {
                            MediaController controller = new MediaController(this);
                            controller.setAnchorView(null);
                            controller.setMediaPlayer(null);
                            Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
                            int width = thumbnail.getWidth();
                            int height = thumbnail.getHeight();
                            int targetWidth;
                            int targetHeight;
                            float scaleFactor;
                            if (width > height) {
                                scaleFactor = ((float) 250) / ((float) width);
                                targetWidth = 250;
                                targetHeight = (int) (height * scaleFactor);
                            } else {
                                scaleFactor = ((float) 250) / ((float) height);
                                targetHeight = 250;
                                targetWidth = (int) (width * scaleFactor);
                            }
                            fIleOperations.videoParameters(null, targetHeight, targetWidth);
							*//*vid_video_preview.setMediaController(controller);
							vid_video_preview.setVideoPath(filePath);
							vid_video_preview.start();*//*
                        }
                    } else {
                        Toast.makeText(context, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_VIDEO_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        //vid_video_preview.setVisibility(View.GONE);
                    }
                    //}
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CAPTURE_AUDIO:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        ContentResolver cR = this.getContentResolver();
                        fileType = cR.getType(fileUri);
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = appFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    fIleOperations.getMimeType(fileName.replace(" ", ""));
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_AUDIO_MPEG && fileType != Constants.FILE_TYPES.FILE_AUDIO_3GP && fileType != Constants.FILE_TYPES.FILE_AUDIO_AMR && fileType != Constants.FILE_TYPES.FILE_AUDIO_WAV && fileType != Constants.FILE_TYPES.FILE_AUDIO_OGG) {
                        Toast.makeText(context, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        fIleOperations.getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_AUDIO_SIZE) {
							*//*ll_audio_picker.setVisibility(View.GONE);
							fl_audio_preview.setVisibility(View.VISIBLE);
							txt_audio_preview.setText(fileName);*//*
                            cache = new File(Constants.FILES_APP.FOLDER_RTTS_CACHE, fileName);
                            try {
                                fIleOperations.copyFile(filePath, cache, true);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CHOOSE_AUDIO:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        fileUri = data.getData();
                        Log.d("Content Uri", fileUri.toString());
                        if (data.getData().getScheme().equals("content")) {
                            ContentResolver cR = this.getContentResolver();
                            fileType = cR.getType(fileUri);
                            try {
                                Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                                if (cursor.moveToFirst()) {
                                    fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                                }
                                cursor.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("File Uri", "" + fileUri.toString());
                        // Get the path of the selected file
                        filePath = appFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                        // Get the name of the selected file
                        fileName = fileUri.getLastPathSegment();
                        Log.d("File Name", fileName);
                        // Get the type of the selected file
                        fIleOperations.getMimeType(fileName);
                        Log.d("File Mime", fileType);
                        if (fileType != Constants.FILE_TYPES.FILE_AUDIO_MPEG && fileType != Constants.FILE_TYPES.FILE_AUDIO_3GP && fileType != Constants.FILE_TYPES.FILE_AUDIO_AMR && fileType != Constants.FILE_TYPES.FILE_AUDIO_WAV && fileType != Constants.FILE_TYPES.FILE_AUDIO_OGG) {
                            Toast.makeText(context, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                        } else {
                            // Get the size of the selected file
                            fIleOperations.getFileSize(filePath);
                            Log.d("File Size", fileSize + "");
                            if (fileSize < Constants.FILE_SIZES.FILE_AUDIO_SIZE) {
								*//*ll_audio_picker.setVisibility(View.GONE);
								fl_audio_preview.setVisibility(View.VISIBLE);
								txt_audio_preview.setText(fileName);*//*
                                cache = new File(Constants.FILES_APP.FOLDER_RTTS_CACHE, fileName);
                                try {
                                    fIleOperations.copyFile(filePath, cache, false);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(context, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(this,"Please select the audio with a suitable file manager..!!",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                break;
            case Constants.FILE_OPERATIONS.ACTIVITY_CHOOSE_FILE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    fileUri = data.getData();
                    Log.d("Content Uri", fileUri.toString());
                    if (data.getData().getScheme().equals("content")) {
                        Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                        if (cursor.moveToFirst()) {
                            fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
                        }
                        cursor.close();
                    }
                    Log.d("File Uri", "" + fileUri.toString());
                    // Get the path of the selected file
                    try {
                        filePath = appFileUtils.getPath(this, fileUri);
                        Log.d("File Path", "" + filePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Get the name of the selected file
                    fileName = fileUri.getLastPathSegment();
                    Log.d("File Name", fileName);
                    // Get the type of the selected file
                    fIleOperations.getMimeType(fileName);
                    Log.d("File Mime", fileType);
                    if (fileType != Constants.FILE_TYPES.FILE_PDF && fileType != Constants.FILE_TYPES.FILE_DOC && fileType != Constants.FILE_TYPES.FILE_XLS && fileType != Constants.FILE_TYPES.FILE_PPT) {
                        Toast.makeText(context, "Sorry..!! Wrong file type chosen..!!", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Get the size of the selected file
                        fIleOperations.getFileSize(filePath);
                        Log.d("File Size", fileSize + "");
                        if (fileSize < Constants.FILE_SIZES.FILE_ATTACH_SIZE) {
							*//*ll_file_picker.setVisibility(View.GONE);
							fl_file_preview.setVisibility(View.VISIBLE);
							txt_file_preview.setText(fileName);*//*
                            cache = new File(Constants.FILES_APP.FOLDER_RTTS_CACHE, fileName);
                            try {
                                fIleOperations.copyFile(filePath, cache, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Sorry..!! The file size exceeds " + Constants.FILE_SIZES.FILE_ATTACH_SIZE + "MB..!!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/