package com.cordsinnovations.rtts.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.VideoView;

import com.cordsinnovations.rtts.BuildConfig;
import com.cordsinnovations.rtts.globals.Constants.FILE_OPERATIONS;
import com.cordsinnovations.rtts.globals.Constants.FILE_SIZES;
import com.cordsinnovations.rtts.globals.Constants.FILE_TYPES;
import com.cordsinnovations.rtts.globals.Constants.FILES_APP;
import com.cordsinnovations.rtts.services.native_services.RTTSFileDownloadService;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import static com.cordsinnovations.rtts.globals.Constants.FILE_OPERATIONS.*;
import static com.cordsinnovations.rtts.globals.Constants.FILE_REQUEST_TYPES.*;
import static com.cordsinnovations.rtts.globals.Constants.PUT_EXTRA_KEYS.DOWNLOAD_FILE_PUT_EXTRA_KEYS.*;

import static com.cordsinnovations.rtts.utils.FileOperationUtils.UTIL_COMMONS.*;
import static com.cordsinnovations.rtts.utils.FileOperationUtils.OTHER_UTILS.*;
import static com.cordsinnovations.rtts.utils.FileOperationUtils.UTIL_BOOLEANS.*;
import static com.cordsinnovations.rtts.utils.FileOperationUtils.UTIL_VARIABLES.*;
import static com.cordsinnovations.rtts.utils.FileOperationUtils.UTIL_BITMAP_VARIABLES.*;

public class FileOperationUtils {

	static class UTIL_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static DisplayMetrics displayMetrics;
		static float screenWidthInDp, screenHeightInDp;
		static final int MAX_IMAGE_DIMENSION = 720;
	}

	static class OTHER_UTILS {
		static AppCommonUtils appCommonUtils;
	}

	static class UTIL_BOOLEANS {
		static boolean isFileSelected = false;
	}

	static class UTIL_VARIABLES {
		static Uri fileUri = null;
		static String filePath = null, fileName = null, fileType = null;
		static float fileSize = 0;

		static File source, cache, destination;
		static Bitmap msgImageBitmap, scaledBm;
		static FileOutputStream finalOutput = null;
	}

	static class UTIL_BITMAP_VARIABLES {
		static int bitmapHeight = 100, bitmapWidth = 100;
		static Bitmap bitmapTemp, bitmap;
		static BitmapDrawable bitmapDrawable;
		static Canvas canvas;
		static Drawable drawable;
	}

	public FileOperationUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		UTIL_COMMONS.handler = new Handler();
		displayMetrics = activity.getResources().getDisplayMetrics();
		screenWidthInDp = displayMetrics.widthPixels / displayMetrics.density;
		screenHeightInDp = displayMetrics.heightPixels / displayMetrics.density;
		appCommonUtils = new AppCommonUtils(activity, context);
	}

	public static Bitmap createBitmapFromDrawable(int drawableID) {
		bitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(drawableID);
		bitmapTemp = bitmapDrawable.getBitmap();
		bitmap = Bitmap.createScaledBitmap(bitmapTemp, bitmapWidth, bitmapHeight, false);
		return bitmap;
	}

	public static Bitmap createBitmapFromVector(int drawableID, int width, int height) {
		drawable = context.getResources().getDrawable(drawableID);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			drawable = (DrawableCompat.wrap(drawable)).mutate();
		}
		bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		if (width == 0 && height == 0) {
			drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		} else {
			drawable.setBounds(0, 0, width, height);
		}
		drawable.draw(canvas);
		return bitmap;
	}

	public File showFileChooser(int fileType, int subType) {
		Calendar cal = Calendar.getInstance();
		switch (fileType) {
			case FILE_REQUEST_IMAGE:
				switch (subType) {
					case ACTIVITY_CAPTURE_IMAGE:
						try {
							fileName = String.format("IMG_%02d_%02d_%04d_%02d_%02d_%02d.jpg", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
							FILES_APP.FOLDER_RTTS_CACHE.mkdirs();
							cache = new File(FILES_APP.FOLDER_RTTS_CACHE, fileName);
							filePath = cache.getAbsolutePath();
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cache));
							} else {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
							}
							activity.startActivityForResult(intent, ACTIVITY_CAPTURE_IMAGE);
						} catch (Exception e) {
							appCommonUtils.appShowShortToast("Please install a suitable Camera application..!!");
							Log.d("Exception", e.getMessage());
						}
						break;
					case ACTIVITY_CHOOSE_IMAGE:
						try {
							cache = null;
							appCommonUtils.appShowShortToast("Select IMAGE files");
							Intent mRequestImageIntent = new Intent(Intent.ACTION_PICK);
							mRequestImageIntent.setType(FILE_TYPES.FILE_IMAGE);
							activity.startActivityForResult(Intent.createChooser(mRequestImageIntent, "Select an Image to upload"), ACTIVITY_CHOOSE_IMAGE);
						} catch (android.content.ActivityNotFoundException e) {
							appCommonUtils.appShowShortToast("Please install a suitable File Manager application..!!");
						}
						break;
				}
				break;
			case FILE_REQUEST_VIDEO:
				switch (subType) {
					case ACTIVITY_CAPTURE_VIDEO:
						try {
							fileName = String.format("VID_%d_%d_%d_%d_%d_%d.mp4", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
							//fileName = "VIDEO.mp4";
							FILES_APP.FOLDER_RTTS_CACHE.mkdirs();
							cache = new File(FILES_APP.FOLDER_RTTS_CACHE, fileName);
							Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
							intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
							intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, (FILE_SIZES.ONE_MB * FILE_SIZES.FILE_VIDEO_SIZE));
							if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cache));
							} else {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
							}
							activity.startActivityForResult(intent, ACTIVITY_CAPTURE_VIDEO);
						} catch (Exception e) {
							appCommonUtils.appShowShortToast("Please install a suitable Video Recorder application..!!");
							e.printStackTrace();
						}
						break;
					case ACTIVITY_CHOOSE_VIDEO:
						try {
							cache = null;
							appCommonUtils.appShowShortToast("Select VIDEO files");
							Intent mRequestVideoIntent = new Intent(Intent.ACTION_PICK);
							mRequestVideoIntent.setType(FILE_TYPES.FILE_VIDEO);
							mRequestVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, (FILE_SIZES.ONE_MB * FILE_SIZES.FILE_VIDEO_SIZE));
							activity.startActivityForResult(Intent.createChooser(mRequestVideoIntent, "Select a video to upload"), ACTIVITY_CHOOSE_VIDEO);
						} catch (android.content.ActivityNotFoundException e) {
							appCommonUtils.appShowShortToast("Please install a suitable File Manager application..!!");
						}
						break;
				}
				break;
			case FILE_REQUEST_AUDIO:
				switch (subType) {
					case ACTIVITY_CAPTURE_AUDIO:
						try {
							fileName = String.format("AUD_%d_%d_%d_%d_%d_%d.amr", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
							//fileName = "AUDIO.mp3";
							FILES_APP.FOLDER_RTTS_CACHE.mkdirs();
							cache = new File(FILES_APP.FOLDER_RTTS_CACHE, fileName);
							Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
							//intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(ChatActivity.this, BuildConfig.APPLICATION_ID + ".provider", cache));
							if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", cache));
							} else {
								intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cache));
							}
							activity.startActivityForResult(intent, ACTIVITY_CAPTURE_AUDIO);
						} catch (Exception e) {
							appCommonUtils.appShowShortToast("Please install a suitable Audio Recorder application..!!");
							e.printStackTrace();
						}
						break;
					case ACTIVITY_CHOOSE_AUDIO:
						try {
							cache = null;
							appCommonUtils.appShowShortToast("Select AUDIO files");
							Intent mRequestAudioIntent = new Intent(Intent.ACTION_PICK);
							mRequestAudioIntent.setType(FILE_TYPES.FILE_AUDIO);
							mRequestAudioIntent.setAction(Intent.ACTION_GET_CONTENT);
							activity.startActivityForResult(Intent.createChooser(mRequestAudioIntent, "Select an audio to upload"), ACTIVITY_CHOOSE_AUDIO);
						} catch (android.content.ActivityNotFoundException e) {
							appCommonUtils.appShowShortToast("Please install a suitable File Manager application..!!");
						}
						break;
				}
				break;
			case FILE_REQUEST_ATTACH:
				try {
					cache = null;
					appCommonUtils.appShowShortToast("Select PDF or MICROSOFT OFFICE files");
					Intent galleryFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
					galleryFileIntent.setType(FILE_TYPES.FILE_PDF + "|" + FILE_TYPES.FILE_DOC + "|" + FILE_TYPES.FILE_XLS + "|" + FILE_TYPES.FILE_PPT);
					galleryFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
					activity.startActivityForResult(Intent.createChooser(galleryFileIntent, "Select a File to upload"), FILE_OPERATIONS.ACTIVITY_CHOOSE_FILE);
				} catch (android.content.ActivityNotFoundException e) {
					appCommonUtils.appShowShortToast("Please install a suitable File Manager application..!!");
				}
				break;
		}
		return cache;
	}

	public Bitmap resizeBitmap(Bitmap bm) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		//Log.d(SENSOR_TAG, "resizeBitmap w: " + width + " h: " + height);
		int targetWidth;
		int targetHeight;
		float scaleFactor;
		if (width > height) {
			scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) width);
			targetWidth = MAX_IMAGE_DIMENSION;
			targetHeight = (int) (height * scaleFactor);
		} else {
			scaleFactor = ((float) MAX_IMAGE_DIMENSION) / ((float) height);
			targetHeight = MAX_IMAGE_DIMENSION;
			targetWidth = (int) (width * scaleFactor);
		}

		//Log.d(SENSOR_TAG, "resizeBitmap scaled to w: " + targetWidth + " h: " + targetHeight);
		return Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, true);
	}

	public void showImagePreview(final Bitmap bm, final ImageView iv_item) {
		activity.runOnUiThread(() -> {
			try {
				finalOutput = new FileOutputStream(cache.getAbsolutePath());
				bm.compress(Bitmap.CompressFormat.PNG, 100, finalOutput);
				finalOutput.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int width = bm.getWidth();
			int height = bm.getHeight();
			int targetWidth;
			int targetHeight;
			float scaleFactor;
			if (width > height) {
				scaleFactor = ((float) 250) / ((float) width);
				targetWidth = 250;
				targetHeight = (int) (height * scaleFactor);
			} else {
				scaleFactor = ((float) 250) / ((float) height);
				targetHeight = 250;
				targetWidth = (int) (width * scaleFactor);
			}
			imageParameters(iv_item, targetHeight, targetWidth);
			/*ll_image_picker.setVisibility(View.GONE);
			fl_image_preview.setVisibility(View.VISIBLE);*/
			iv_item.setImageBitmap(bm);
		});
	}

	public void imageParameters(ImageView layout, int height, int width) {
		ViewGroup.LayoutParams params = layout.getLayoutParams();
		float hdp = 0;
		float wdp = 0;
		if (height == 0) {
			hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenHeightInDp, displayMetrics);
		} else {
			hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, displayMetrics);
		}
		if (width == 0) {
			wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenWidthInDp, displayMetrics);
		} else {
			wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
		}
		params.height = (int) hdp;
		params.width = (int) wdp;
		layout.setLayoutParams(params);
	}

	public void videoParameters(VideoView layout, int height, int width) {
		ViewGroup.LayoutParams params = layout.getLayoutParams();
		float hdp = 0;
		float wdp = 0;
		if (height == 0) {
			hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenHeightInDp, displayMetrics);
		} else {
			hdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, displayMetrics);
		}
		if (width == 0) {
			wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screenWidthInDp, displayMetrics);
		} else {
			wdp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
		}
		params.height = (int) hdp;
		params.width = (int) wdp;
		layout.setLayoutParams(params);
	}

	public String getMimeType(String Filename) {
		Filename = Filename.toLowerCase();
		//Filename = Filename.replace(" ", "");
		Filename = Filename.replaceAll("[^a-zA-Z0-9.-]", "_");
		//String extension = Filename.substring(Filename.lastIndexOf("."));
		String extension = MimeTypeMap.getFileExtensionFromUrl(Filename);
		Log.d("File Extension", extension);
		if (extension != null) {
			fileType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		}
		return fileType;
	}

	public void getFileSize(String passedPath) {
		source = new File(passedPath);
		fileSize = (Float.parseFloat(String.valueOf(source.length() / FILE_SIZES.ONE_MB)));
	}

	public void copyFile(String sourcePath, File destination, boolean delete) throws IOException {
		source = new File(sourcePath);
		moveFile(source, destination, delete);
		isFileSelected = true;
	}

	public void moveFile(File sourceFile, File destinationFile, boolean delete) {
		try {
			FileUtils.copyFile(sourceFile, destinationFile);
			if (delete) {
				sourceFile.delete();
				isFileSelected = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File removeFile(File cache) {
		if (cache.delete()) {
			return null;
		} else {
			return cache;
		}
	}

	public Bitmap rotateBitmap(Bitmap bitmap, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		return rotatedBitmap;
	}

	public void downloadFile(final String fileName, final int FileDownloadType) {
		activity.runOnUiThread(() -> handler.post(() -> {
			Intent downloadServiceIntent = new Intent(context, RTTSFileDownloadService.class)
					.putExtra(PUT_EXTRA_KEY_DOWNLOAD_FILE_NAME, fileName)
					.putExtra(PUT_EXTRA_KEY_DOWNLOAD_FILE_TYPE, FileDownloadType);
			activity.startService(downloadServiceIntent);
		}));
	}
}
