package com.cordsinnovations.rtts.hardware;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.util.Log;

import com.cordsinnovations.rtts.utils.MathematicsUtils;
import com.cordsinnovations.rtts.utils.VibrationUtils;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_SHAKE_CONSTANTS.DEVICE_SENSOR_CONSTANTS.SENSOR_TAG;

import static com.cordsinnovations.rtts.hardware.RotationDetector.SENSOR_UTILS.*;
import static com.cordsinnovations.rtts.hardware.RotationDetector.SENSOR_COMMONS.*;
import static com.cordsinnovations.rtts.hardware.RotationDetector.SENSOR_VARIABLES.*;

public class RotationDetector implements SensorEventListener {

	static class SENSOR_UTILS {
		static MathematicsUtils mathematicsUtils;
		static VibrationUtils vibrationUtils;
	}

	static class SENSOR_COMMONS {
		static Activity activity;
		static Context context;
		static SensorManager sensorManager;
		static Sensor sensorMagnetometer;
		static OnRotationListener rotationListener;
	}

	static class SENSOR_VARIABLES {
		static float angleAzimuth = 0.0f, anglePitch = 0.0f, angleRoll = 0.0f, anglePreAzimuth = 0.0f, angleAzimuthDifference = 0.0f;
		static Vibrator vibrator;
	}

	public interface OnRotationListener {
		void onDeviceRotation();
	}

	public RotationDetector(Activity activity, Context context) {
		SENSOR_COMMONS.activity = activity;
		SENSOR_COMMONS.context = context;
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensorMagnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		if (sensorMagnetometer == null) {
			Log.e(SENSOR_TAG, "No magnetometer sensors found");
		}
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		mathematicsUtils = new MathematicsUtils(activity, context);
		vibrationUtils = new VibrationUtils(activity, context);
	}

	public void start() {
		sensorManager.registerListener(this, sensorMagnetometer, SensorManager.SENSOR_DELAY_FASTEST);
		//Toast.makeText(context, "Azimuth angle : " + angleAzimuth + "\nPitch angle : " + anglePitch + "\nRoll angle : " + angleRoll, Toast.LENGTH_SHORT).show();
	}

	public void stop() {
		sensorManager.unregisterListener(this, sensorMagnetometer);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		angleAzimuth = event.values[0];
		anglePitch = event.values[1];
		angleRoll = event.values[2];
		angleAzimuthDifference = mathematicsUtils.absoluteValue(anglePreAzimuth - angleAzimuth);
		if (angleAzimuthDifference >= 1.0f) {
			vibrationUtils.vibrateOnExactDirections(angleAzimuth);
			anglePreAzimuth = angleAzimuth;
			rotationListener.onDeviceRotation();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	public void setRotationListener(OnRotationListener rotationListener) {
		SENSOR_COMMONS.rotationListener = rotationListener;
	}

	public float getAzimuthAngle() {
		return angleAzimuth;
	}
}
