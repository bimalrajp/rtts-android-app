package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Duration2 implements Parcelable {

	private String text;
	private int value;

	protected Duration2(Parcel in) {
		text = in.readString();
		value = in.readInt();
	}

	public static final Creator<Duration2> CREATOR = new Creator<Duration2>() {
		@Override
		public Duration2 createFromParcel(Parcel in) {
			return new Duration2(in);
		}

		@Override
		public Duration2[] newArray(int size) {
			return new Duration2[size];
		}
	};

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Duration2 [text = " + text + ", value = " + value + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(text);
		parcel.writeInt(value);
	}
}
