package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GeocodedWaypoint implements Parcelable {

	private String geocoder_status;
	private String place_id;
	private String[] types;

	protected GeocodedWaypoint(Parcel in) {
		geocoder_status = in.readString();
		place_id = in.readString();
		types = in.createStringArray();
	}

	public static final Creator<GeocodedWaypoint> CREATOR = new Creator<GeocodedWaypoint>() {
		@Override
		public GeocodedWaypoint createFromParcel(Parcel in) {
			return new GeocodedWaypoint(in);
		}

		@Override
		public GeocodedWaypoint[] newArray(int size) {
			return new GeocodedWaypoint[size];
		}
	};

	public String getGeocoder_status() {
		return geocoder_status;
	}
	public void setGeocoder_status(String geocoder_status) {
		this.geocoder_status = geocoder_status;
	}
	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	public String[] getTypes() {
		return types;
	}
	public void setTypes(String[] types) {
		this.types = types;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(geocoder_status);
		parcel.writeString(place_id);
		parcel.writeStringArray(types);
	}
}