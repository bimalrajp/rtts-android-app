package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class OverviewPolyline implements Parcelable {

	private String points;

	protected OverviewPolyline(Parcel in) {
		points = in.readString();
	}

	public static final Creator<OverviewPolyline> CREATOR = new Creator<OverviewPolyline>() {
		@Override
		public OverviewPolyline createFromParcel(Parcel in) {
			return new OverviewPolyline(in);
		}

		@Override
		public OverviewPolyline[] newArray(int size) {
			return new OverviewPolyline[size];
		}
	};

	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "OverviewPolyline [points = " + points + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(points);
	}
}
