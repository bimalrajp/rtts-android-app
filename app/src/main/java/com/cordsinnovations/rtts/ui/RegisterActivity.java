package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.api_callbacks.RegisterOTP;
import com.cordsinnovations.rtts.globals.Constants;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.CustomerUtils;
import com.cordsinnovations.rtts.utils.FileOperationUtils;
import com.cordsinnovations.rtts.utils.PermissionRequestUtils;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.utils.FileFunctionUtils;

import com.cordsinnovations.rtts.globals.Constants.FILE_SIZES;
import com.cordsinnovations.rtts.globals.Constants.FILE_TYPES;
import com.cordsinnovations.rtts.globals.Constants.FILES_APP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cordsinnovations.rtts.RightToTravelSafe.api;
import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.CUSTOMER_CONSTANTS.*;
import static com.cordsinnovations.rtts.globals.Constants.FILE_OPERATIONS.*;
import static com.cordsinnovations.rtts.globals.Constants.FILE_REQUEST_TYPES.FILE_REQUEST_IMAGE;

import static com.cordsinnovations.rtts.ui.RegisterActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.RegisterActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.RegisterActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.RegisterActivity.UI_ELEMENTS.UI_ALERTS.*;
import static com.cordsinnovations.rtts.ui.RegisterActivity.UI_ELEMENTS.FILE_VARIABLES.*;
import static com.cordsinnovations.rtts.ui.RegisterActivity.UI_ELEMENTS.REGISTER_FORM.*;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static InputMethodManager inputMethodManager = null;
		static Typeface typeFace;
	}

	static class ACTIVITY_UTILS {
		static PermissionRequestUtils permissionRequestUtils;
		static AppCommonUtils appCommonUtils;
		static CustomerUtils customerUtils;
		static FileOperationUtils FileOperationUtils;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static CoordinatorLayout cl_register_activity;
			static TextView tv_register_title;
		}

		static class UI_ALERTS {
			static AlertDialog.Builder builder;
			static AlertDialog alertDialog;
			static View alertDialogView;
			static LinearLayout ll_camera_btn, ll_gallery_btn, ll_cancel_btn;
		}

		static class FILE_VARIABLES {
			static Uri fileUri = null;
			static String filePath = null;
			static String fileName = null;
			static String fileType = null;
			static float fileSize = 0;

			static boolean isFileSelected;

			static File source, cache, destination;
			static Bitmap msgImageBitmap, scaledBm;
			static FileOutputStream finalOutput = null;
		}

		static class REGISTER_FORM {
			static ArrayAdapter customerTypeArrayAdapter, customerGenderArrayAdapter;
			static String string_customer_dob_db, string_customer_dob_display;
			static TextView tv_customer_dob, tv_mandatory_instruction_register;
			static Spinner spn_customer_type, spn_customer_gender;
			static EditText et_customer_full_name, et_customer_username, et_customer_password, et_customer_mobile, et_customer_permanent_address, et_customer_permanent_pin_code, et_customer_current_address, et_customer_current_pin_code, et_customer_emergency_contact_1, et_customer_emergency_contact_2, et_customer_email;
			static CheckBox cb_same_current_address;
			static boolean sameCurrentAddress = false, registerButtonShown = false;
			static LinearLayout ll_same_current_address;
			static ImageView iv_customer_image;
			static Button btn_customer_sign_up;
			static final Calendar myCalendar = Calendar.getInstance();
			static Animation animationAlphaFadeIn, animationAlphaFadeOut;
		}

		static class OTHER_OPTIONS {
			static Button btn_customer_login;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		configView();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.iv_customer_image:
				runnable = () -> {
					builder = new AlertDialog.Builder(context);
					alertDialogView = getLayoutInflater().inflate(R.layout.layout_image_chooser, null);

					ll_camera_btn = alertDialogView.findViewById(R.id.ll_camera_btn);
					ll_camera_btn.setOnClickListener(view13 -> {
						deleteFileIfSelected();
						cache = FileOperationUtils.showFileChooser(FILE_REQUEST_IMAGE, ACTIVITY_CAPTURE_IMAGE);
						alertDialog.cancel();
					});

					ll_gallery_btn = alertDialogView.findViewById(R.id.ll_gallery_btn);
					ll_gallery_btn.setOnClickListener(view12 -> {
						deleteFileIfSelected();
						FileOperationUtils.showFileChooser(FILE_REQUEST_IMAGE, ACTIVITY_CHOOSE_IMAGE);
						alertDialog.cancel();
					});

					ll_cancel_btn = alertDialogView.findViewById(R.id.ll_cancel_btn);
					ll_cancel_btn.setOnClickListener(view1 -> alertDialog.cancel());

					builder.setView(alertDialogView);

					alertDialog = builder.create();
					alertDialog.setCancelable(true);
					alertDialog.show();
				};
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					permissionRequestUtils.requestPermissions(runnable, Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_AND_STORAGE_PERMISSIONS);
				} else {
					runnable.run();
				}
				break;
			case R.id.btn_customer_sign_up:
				registerUser();
				break;
			/*case R.id.btn_customer_login:
				startActivity(new Intent(context, LoginActivity.class));
				finish();
				break;*/
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_AND_STORAGE_PERMISSIONS) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAM, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_CAM, false).apply();
			}
			if (grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, true).apply();
			} else {
				sharedPreferences.edit()
						.putBoolean(PreferenceKeys.PERMISSION_STR, false).apply();
			}

			if (sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_CAM, false) && sharedPreferences.getBoolean(PreferenceKeys.PERMISSION_STR, false)) {
				if (runnable != null) {
					runnable.run();
				}
			}
		}
	}

	@Override
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
			case ACTIVITY_CAPTURE_IMAGE:
				try {
					if (cache.exists()) {
						filePath = cache.getAbsolutePath();
						fileUri = Uri.fromFile(cache);
						fileName = fileUri.getLastPathSegment();
						fileType = FileOperationUtils.getMimeType(fileName);
						msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
						scaledBm = FileOperationUtils.resizeBitmap(msgImageBitmap);
						FileOperationUtils.showImagePreview(scaledBm, iv_customer_image);
						isFileSelected = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case ACTIVITY_CHOOSE_IMAGE:
				if (resultCode == RESULT_OK) {
					// Get the Uri of the selected file
					fileUri = data.getData();
					Log.d("Content Uri", fileUri.toString());
					if (data.getData().getScheme().equals("content")) {
						Cursor cursor = getBaseContext().getContentResolver().query(fileUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
						if (cursor.moveToFirst()) {
							fileUri = Uri.parse("file://" + Uri.parse(cursor.getString(0)).getPath());
						}
						cursor.close();
					}
					Log.d("File Uri", "" + fileUri.toString());
					// Get the path of the selected file
					try {
						filePath = FileFunctionUtils.getPath(context, fileUri);
						Log.d("File Path", "" + filePath);
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
					// Get the name of the selected file
					fileName = fileUri.getLastPathSegment();
					Log.d("File Name", fileName);
					// Get the type of the selected file
					fileType = FileOperationUtils.getMimeType(fileName);
					Log.d("File Mime", fileType);
					if (fileType != FILE_TYPES.FILE_IMAGE_JPEG && fileType != FILE_TYPES.FILE_IMAGE_JPG && fileType != FILE_TYPES.FILE_IMAGE_PNG) {
						appCommonUtils.appShowShortToast("Sorry! Wrong file type chosen!");
					} else {
						// Get the size of the selected file
						FileOperationUtils.getFileSize(filePath);
						Log.d("File Size", fileSize + "");
						if (fileSize < FILE_SIZES.FILE_IMAGE_SIZE) {
							cache = new File(FILES_APP.FOLDER_RTTS_CACHE, fileName);
							try {
								FileOperationUtils.copyFile(filePath, cache, false);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if (cache.exists()) {
								msgImageBitmap = BitmapFactory.decodeFile(cache.getAbsolutePath());
								scaledBm = FileOperationUtils.resizeBitmap(msgImageBitmap);
								FileOperationUtils.showImagePreview(scaledBm, iv_customer_image);
								isFileSelected = true;
							}
						} else {
							appCommonUtils.appShowShortToast("Sorry! The file size exceeds " + FILE_SIZES.FILE_IMAGE_SIZE + "MB!");
						}
					}
				}
				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		deleteFileIfSelected();
		startActivity(new Intent(context, LoginActivity.class));
		finish();
	}

	void configView() {

		getSupportActionBar().hide();

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");
		runnable = () -> mandatoryFieldsCheck();

		permissionRequestUtils = new PermissionRequestUtils(activity, context);
		appCommonUtils = new AppCommonUtils(activity, context);
		customerUtils = new CustomerUtils(activity, context);
		FileOperationUtils = new FileOperationUtils(activity, context);

		final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		};

		cl_register_activity = findViewById(R.id.cl_register_activity);

		btn_customer_sign_up = findViewById(R.id.btn_customer_sign_up);
		btn_customer_sign_up.setTypeface(typeFace);
		/*btn_customer_login = findViewById(R.id.btn_customer_login);*/

		tv_register_title = findViewById(R.id.tv_register_title);
		tv_register_title.setTypeface(typeFace);
		animationAlphaFadeIn = AnimationUtils.loadAnimation(context, R.anim.alpha_fade_in);
		animationAlphaFadeOut = AnimationUtils.loadAnimation(context, R.anim.alpha_fade_out);

		et_customer_full_name = findViewById(R.id.et_customer_full_name);
		appCommonUtils.editTextWatcher(et_customer_full_name, runnable);
		spn_customer_type = findViewById(R.id.spn_customer_type);
		customerTypeArrayAdapter = new ArrayAdapter(context, R.layout.layout_spinner, R.id.tv_spinner_item, CUSTOMER_TYPES);
		spn_customer_type.setAdapter(customerTypeArrayAdapter);
		et_customer_username = findViewById(R.id.et_customer_username);
		appCommonUtils.editTextWatcher(et_customer_username, runnable);
		et_customer_password = findViewById(R.id.et_customer_password);
		appCommonUtils.editTextWatcher(et_customer_password, runnable);
		et_customer_mobile = findViewById(R.id.et_customer_mobile);
		appCommonUtils.editTextWatcher(et_customer_mobile, runnable);
		tv_customer_dob = findViewById(R.id.tv_customer_dob);
		tv_mandatory_instruction_register = findViewById(R.id.tv_mandatory_instruction_register);
		spn_customer_gender = findViewById(R.id.spn_customer_gender);
		customerGenderArrayAdapter = new ArrayAdapter(context, R.layout.layout_spinner, R.id.tv_spinner_item, CUSTOMER_GENDERS);
		spn_customer_gender.setAdapter(customerGenderArrayAdapter);
		et_customer_permanent_address = findViewById(R.id.et_customer_permanent_address);
		et_customer_permanent_pin_code = findViewById(R.id.et_customer_permanent_pin_code);
		cb_same_current_address = findViewById(R.id.cb_same_current_address);
		ll_same_current_address = findViewById(R.id.ll_same_current_address);
		et_customer_current_address = findViewById(R.id.et_customer_current_address);
		et_customer_current_pin_code = findViewById(R.id.et_customer_current_pin_code);
		et_customer_emergency_contact_1 = findViewById(R.id.et_customer_emergency_contact_1);
		appCommonUtils.editTextWatcher(et_customer_emergency_contact_1, runnable);
		et_customer_emergency_contact_2 = findViewById(R.id.et_customer_emergency_contact_2);
		appCommonUtils.editTextWatcher(et_customer_emergency_contact_2, runnable);
		et_customer_email = findViewById(R.id.et_customer_email);

		iv_customer_image = findViewById(R.id.iv_customer_image);

		tv_customer_dob.setOnClickListener(v -> new DatePickerDialog(context, date, myCalendar
				.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
				myCalendar.get(Calendar.DAY_OF_MONTH)).show());

		cb_same_current_address.setOnCheckedChangeListener((compoundButton, state) -> {
			if (state) {
				sameCurrentAddress = true;
				ll_same_current_address.setVisibility(View.GONE);
			} else {
				sameCurrentAddress = false;
				ll_same_current_address.setVisibility(View.VISIBLE);
			}
		});

		iv_customer_image.setOnClickListener(this);

		btn_customer_sign_up.setOnClickListener(this);
		/*btn_customer_login.setOnClickListener(this);*/
	}

	void updateLabel() {

		String myDOBDisplayFormat = "MMM dd, yyyy";
		SimpleDateFormat simpleDateFormatDisplay = new SimpleDateFormat(myDOBDisplayFormat, Locale.US);

		String myDOBDBFormat = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormatDB = new SimpleDateFormat(myDOBDBFormat, Locale.US);

		string_customer_dob_display = simpleDateFormatDisplay.format(myCalendar.getTime());
		tv_customer_dob.setText(string_customer_dob_display);

		string_customer_dob_db = simpleDateFormatDB.format(myCalendar.getTime());
	}

	public void deleteFileIfSelected() {
		if (isFileSelected) {
			cache = FileOperationUtils.removeFile(cache);
			if (cache == null) {
				isFileSelected = false;
			}
		}
	}

	public void mandatoryFieldsCheck() {
		String userFullName = et_customer_full_name.getText().toString();
		String userUsername = et_customer_username.getText().toString();
		String userPassword = et_customer_password.getText().toString();
		String userMobile = et_customer_mobile.getText().toString();
		String userEmergencyContact1 = et_customer_emergency_contact_1.getText().toString();
		String userEmergencyContact2 = et_customer_emergency_contact_2.getText().toString();

		if (userFullName.equals("")
				|| userUsername.equals("")
				|| userPassword.equals("")
				|| userMobile.equals("")
				|| (userEmergencyContact1.equals("") && userEmergencyContact2.equals(""))
				|| (userEmergencyContact1.equals(userMobile)
				|| userEmergencyContact2.equals(userMobile)
				|| userEmergencyContact1.equals(userEmergencyContact2))
				|| userMobile.length() < 10
				|| (userEmergencyContact1.length() < 10 && userEmergencyContact2.length() < 10)
				) {
			if (!userFullName.equals("")
					&& !userUsername.equals("")
					&& !userPassword.equals("")
					&& !userMobile.equals("")
					&& (!userEmergencyContact1.equals("") || !userEmergencyContact2.equals(""))
					) {
				if (userEmergencyContact1.equals(userMobile) || userEmergencyContact2.equals(userMobile) || userEmergencyContact1.equals(userEmergencyContact2)) {
					appCommonUtils.appShowLongToast("Mobile and Emergency Contact Numbers must not be the same! Provide a different Emergency Contact!");
				}
			}
			if (registerButtonShown) {
				runnable = () -> {
					tv_mandatory_instruction_register.setVisibility(View.VISIBLE);
					btn_customer_sign_up.setVisibility(View.GONE);
				};
				btn_customer_sign_up.setEnabled(false);
				appCommonUtils.animationListener(animationAlphaFadeOut, runnable);
				btn_customer_sign_up.startAnimation(animationAlphaFadeOut);
				registerButtonShown = false;
			}
		} else {
			runnable = () -> registerUser();
			appCommonUtils.onEditorActionListener(et_customer_password, EditorInfo.IME_ACTION_DONE, runnable);
			btn_customer_sign_up.setEnabled(true);
			if (!registerButtonShown) {
				tv_mandatory_instruction_register.setVisibility(View.GONE);
				btn_customer_sign_up.startAnimation(animationAlphaFadeIn);
				btn_customer_sign_up.setVisibility(View.VISIBLE);
				registerButtonShown = true;
			}
		}
	}

	private void registerUser() {
		try {
			inputMethodManager.hideSoftInputFromWindow(et_customer_full_name.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_username.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_password.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_mobile.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_email.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_current_address.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_current_pin_code.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_permanent_address.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_permanent_pin_code.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_emergency_contact_1.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			inputMethodManager.hideSoftInputFromWindow(et_customer_emergency_contact_2.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String userFullName = et_customer_full_name.getText().toString();
		String userType = spn_customer_type.getSelectedItem().toString();
		String userUsername = et_customer_username.getText().toString();
		String userPassword = et_customer_password.getText().toString();
		String userMobile = et_customer_mobile.getText().toString();
		String userEmail = et_customer_email.getText().toString();
		String userDOB = string_customer_dob_db;
		String userGender = spn_customer_gender.getSelectedItem().toString();
		String userPermanentAddress = et_customer_permanent_address.getText().toString();
		String userPermanentPincode = et_customer_permanent_pin_code.getText().toString();
		String userCurrentAddress, userCurrentPincode;
		if (sameCurrentAddress) {
			userCurrentAddress = userPermanentAddress;
			userCurrentPincode = userPermanentPincode;
		} else {
			userCurrentAddress = et_customer_current_address.getText().toString();
			userCurrentPincode = et_customer_current_pin_code.getText().toString();
		}
		String userEmergencyContact1 = et_customer_emergency_contact_1.getText().toString();
		String userEmergencyContact2 = et_customer_emergency_contact_2.getText().toString();

		if (userFullName.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide your Full Name!");
		} else if (userUsername.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide a Username!");
		} else if (userPassword.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide a Password!");
		} else if (userMobile.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide your Mobile Number!");
		} else if (userEmergencyContact1.isEmpty() && userEmergencyContact2.isEmpty()) {
			appCommonUtils.appShowShortToast("Provide at least one Emergency Contact!");
		} else if (userEmergencyContact1.equals(userMobile) || userEmergencyContact2.equals(userMobile) || userEmergencyContact1.equals(userEmergencyContact2)) {
			appCommonUtils.appShowLongToast("Mobile and Emergency Contact Numbers must not be the same! Provide a different Emergency Contact!");
		} else {
			if (isFileSelected) {
				String filename = cache.getName();
			}
			Call<RegisterOTP> registerOTPCall = api.register(
					userFullName,
					userType,
					userUsername,
					userPassword,
					userMobile,
					userDOB,
					userGender,
					userCurrentAddress,
					userCurrentPincode,
					userPermanentAddress,
					userPermanentPincode,
					userEmergencyContact1,
					userEmergencyContact2,
					userEmail,
					cache
			);
			registerOTPCall.enqueue(new Callback<RegisterOTP>() {
				@Override
				public void onResponse(Call<RegisterOTP> call, Response<RegisterOTP> response) {
					if (response.isSuccessful()) {
						RegisterOTP responseBody = response.body();
						if (responseBody.ErrorFlag == 0) {
							appCommonUtils.appShowShortToast("Registration Successful.\nEnter the OTP for verification.");
							customerUtils.customerRegister(responseBody.OTP, responseBody.PKCustomerID);
							//destination = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, fileName);
							if (cache != null) {
								destination = new File(FILES_APP.FOLDER_IMAGE_CUSTOMER, "customerfullname1" + filePath.substring(filePath.lastIndexOf(".")));
								FileOperationUtils.moveFile(cache, destination, true);
							}
						} else {
							appCommonUtils.appShowShortToast("Unable to Register you at this point of time! Please try again later!");
						}
					} else {
						appCommonUtils.appShowShortToast("Error! Please try again!");
					}
				}

				@Override
				public void onFailure(Call<RegisterOTP> call, Throwable t) {
					appCommonUtils.appShowShortToast("Can't connect to Server! Please check your Network Connection!");
				}
			});
		}
	}
}
