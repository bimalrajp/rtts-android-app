package com.cordsinnovations.rtts.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.globals.PreferenceKeys;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_VIBRATION_CONSTANTS.VIBRATION_SHORT_MILLI_SECONDS;

import static com.cordsinnovations.rtts.utils.AppCommonUtils.OTHER_UTILS.*;
import static com.cordsinnovations.rtts.utils.AppCommonUtils.UTIL_COMMONS.*;
import static com.cordsinnovations.rtts.utils.AppCommonUtils.UTIL_BOOLEANS.*;
import static com.cordsinnovations.rtts.utils.AppCommonUtils.UTIL_ALERTS.*;
import static com.cordsinnovations.rtts.utils.AppCommonUtils.UTIL_VARIABLES.*;

public class AppCommonUtils {

	static class UTIL_COMMONS {
		static Context context;
		static Activity activity;
		static Handler handler;
	}

	static class OTHER_UTILS {
		static FileOperationUtils fileOperationUtils;
	}

	static class UTIL_BOOLEANS {
		static boolean closeOnBackPress = false;
	}

	static class UTIL_ALERTS {
		static AlertDialog.Builder builder;
		static AlertDialog alertDialog;
	}

	static class UTIL_VARIABLES {
		static Bitmap bitmap;
		static Drawable drawable;

		static Animation.AnimationListener listener;
	}

	public AppCommonUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		handler = new Handler();
	}

	public void appAlreadyOpened() {
		if (!sharedPreferences.getBoolean(PreferenceKeys.APP_ALREADY_OPENED, false)) {
			sharedPreferences.edit()
					.putBoolean(PreferenceKeys.APP_ALREADY_OPENED, true)
					.putInt(PreferenceKeys.SETTINGS_MAP_STYLE_ID, R.raw.map_style_01)
					.putInt(PreferenceKeys.SETTINGS_MAP_STYLE_POSITION, 0)
					.putBoolean(PreferenceKeys.DIRECTION_VIBRATIONS, false)
					.putInt(PreferenceKeys.DIRECTION_VIBRATION_NORTH, VIBRATION_SHORT_MILLI_SECONDS)
					.putInt(PreferenceKeys.DIRECTION_VIBRATION_EAST, VIBRATION_SHORT_MILLI_SECONDS)
					.putInt(PreferenceKeys.DIRECTION_VIBRATION_SOUTH, VIBRATION_SHORT_MILLI_SECONDS)
					.putInt(PreferenceKeys.DIRECTION_VIBRATION_WEST, VIBRATION_SHORT_MILLI_SECONDS)
					.apply();
		}
	}

	public void appShowShortToast(String Message) {
		Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
	}

	public void appShowLongToast(String Message) {
		Toast.makeText(context, Message, Toast.LENGTH_LONG).show();
	}

	public void editTextWatcher(EditText editText, final Runnable runnable){
		editText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				runnable.run();
			}
		});
	}

	public void onEditorActionListener(EditText editText, final int actionID, final Runnable runnable){
		editText.setOnEditorActionListener((v, actionId, event) -> {
			if ((actionId == actionID)) {
				runnable.run();
				return true;
			}
			return false;
		});
	}

	public void animationListener(Animation animation, final Runnable runnable){
		listener = new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				runnable.run();
			}
		};
		animation.setAnimationListener(listener);
	}

	public void appExitConfirmation() {
		if (!closeOnBackPress) {
			fileOperationUtils = new FileOperationUtils(activity, context);
			closeOnBackPress = true;
			builder = new AlertDialog.Builder(context);
			builder.setTitle(R.string.alert_title_exit);
			builder.setIcon(R.mipmap.ic_launcher);
			builder.setMessage(R.string.alert_message_choose_exit);
			builder.setPositiveButton(R.string.btn_positive_1, (dialog, which) -> {
				appShowShortToast(" Stay Healthy. Stay Safe. \n Have A Nice Day ");
				handler.postDelayed(() -> {
					closeOnBackPress = false;
					activity.finish();
				}, 2000);
			});
			//builder.setPositiveButtonIcon(activity.getDrawable(R.drawable.ic_exit));
			/*bitmap = fileOperationUtils.createBitmapFromVector(R.drawable.btn_positive, 35, 35);
			drawable = new BitmapDrawable(bitmap);
			builder.setPositiveButtonIcon(drawable);*/

			builder.setNegativeButton(R.string.btn_negative_1, (dialog, which) -> {
				appShowShortToast(" Cool ");
				alertDialog.dismiss();
				handler.postDelayed(() -> closeOnBackPress = false, 2000);
			});
			/*bitmap = fileOperationUtils.createBitmapFromVector(R.drawable.btn_negative, 35, 35);
			drawable = new BitmapDrawable(bitmap);
			builder.setNegativeButtonIcon(drawable);*/

			alertDialog = builder.create();
			alertDialog.setCancelable(false);
			alertDialog.show();
		} else {
			closeOnBackPress = false;
		}
	}
}
