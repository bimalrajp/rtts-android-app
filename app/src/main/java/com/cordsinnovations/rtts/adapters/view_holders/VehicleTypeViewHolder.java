package com.cordsinnovations.rtts.adapters.view_holders;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.adapters.ui_adapters.VehicleTypeAdapter;

public class VehicleTypeViewHolder extends RecyclerView.ViewHolder {

	public CardView cv_vehicle_type, cv_vehicle_type_details;
	public ImageView VehicleTypeImage;
	public TextView VehicleTypeName, tv_seating_capacity, tv_vehicle_rate, tv_waiting_charge;

	public VehicleTypeViewHolder(@NonNull View itemView, final VehicleTypeAdapter.OnItemClickListener onItemClickListener) {
		super(itemView);

		cv_vehicle_type_details = itemView.findViewById(R.id.cv_vehicle_type_details);
		tv_seating_capacity = itemView.findViewById(R.id.tv_seating_capacity);
		tv_vehicle_rate = itemView.findViewById(R.id.tv_vehicle_rate);
		tv_waiting_charge = itemView.findViewById(R.id.tv_waiting_charge);

		cv_vehicle_type = itemView.findViewById(R.id.cv_vehicle_type);
		VehicleTypeImage = itemView.findViewById(R.id.iv_vehicle_type);
		VehicleTypeName = itemView.findViewById(R.id.tv_vehicle_type);

		cv_vehicle_type_details.setOnClickListener(view -> {
			if (onItemClickListener != null) {
				int position = getAdapterPosition();
				if (position != RecyclerView.NO_POSITION) {
					onItemClickListener.onItemClick(position);
				}
			}
		});

		cv_vehicle_type.setOnClickListener(view -> {
			if (onItemClickListener != null) {
				int position = getAdapterPosition();
				if (position != RecyclerView.NO_POSITION) {
					onItemClickListener.onItemClick(position);
				}
			}
		});
	}
}
