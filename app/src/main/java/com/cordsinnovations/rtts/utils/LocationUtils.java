package com.cordsinnovations.rtts.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.cordsinnovations.rtts.utils.LocationUtils.UTIL_COMMONS.*;
import static com.cordsinnovations.rtts.utils.LocationUtils.OTHER_UTILS.*;
import static com.cordsinnovations.rtts.utils.LocationUtils.UTIL_VARIABLES.*;

public class LocationUtils implements LocationListener {

	static class UTIL_COMMONS {
		static Context context;
		static Activity activity;
	}

	static class OTHER_UTILS {
		static AppCommonUtils appCommonUtils;
	}

	static class UTIL_VARIABLES {
		static LocationManager locationManager;
		static Geocoder geocoder;
		static LatLng latLng;
		static List<Address> addressList;
		static Address address;
		static String locationProvider;
		static Location location;
		static Double Latitude, Longitude;
	}

	public LocationUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		locationProvider = getTheLocationProvider();
		appCommonUtils = new AppCommonUtils(activity, context);
	}

	public Location getLocation() {

		location = null;

		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return location;
		} else {
			if (locationProvider != null) {
				location = locationManager.getLastKnownLocation(locationProvider);
			} else {
				appCommonUtils.appShowShortToast("Enable location settings and try again!");
				displayLocationSettingsRequest(context);
			}
		}

		return location;
	}

	private void displayLocationSettingsRequest(final Context context) {

		LocationRequest mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(10000);
		mLocationRequest.setFastestInterval(5000);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
				.addLocationRequest(mLocationRequest);

		SettingsClient client = LocationServices.getSettingsClient(context);
		Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

		task.addOnSuccessListener(activity, locationSettingsResponse -> {
			// All location settings are satisfied. The client can initialize
			// location requests here.
			locationProvider = getTheLocationProvider();
		});

		task.addOnFailureListener(activity, e -> {
			if (e instanceof ResolvableApiException) {
				// Location settings are not satisfied, but this can be fixed
				// by showing the user a dialog.
				try {
					// Show the dialog by calling startResolutionForResult(),
					// and check the result in onActivityResult().
					ResolvableApiException resolvable = (ResolvableApiException) e;
					resolvable.startResolutionForResult(activity, 1);
					locationProvider = getTheLocationProvider();
				} catch (IntentSender.SendIntentException sendEx) {
					// Ignore the error.
				}
			}
		});
	}

	private String getTheLocationProvider() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
			if (locationManager.isLocationEnabled()) {
				if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
					locationProvider = LocationManager.NETWORK_PROVIDER;
				} else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
					locationProvider = LocationManager.GPS_PROVIDER;
				} else {
					locationProvider = null;
				}
			} else {
				//appCommonUtils.appShowShortToast("Enable location settings");
			}
		} else {
			if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				locationProvider = LocationManager.NETWORK_PROVIDER;
			} else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				locationProvider = LocationManager.GPS_PROVIDER;
			} else {
				locationProvider = null;
			}
			if (locationProvider == null) {
				//appCommonUtils.appShowShortToast("Enable location settings");
			}
		}
		return locationProvider;
	}

	public LatLng getLatLngFromAddress(String placeNameString, int noOfResults) {
		try {
			geocoder = new Geocoder(context, Locale.getDefault());
			addressList = null;
			addressList = geocoder.getFromLocationName(placeNameString, noOfResults);
			if (addressList.size() > 0) {
				address = addressList.get(0);
				latLng = new LatLng(address.getLatitude(), address.getLongitude());
			} else {
				latLng = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return latLng;
	}

	public String getLocationAddress(LatLng latLng, boolean fullAddress) {
		String Address = null;
		geocoder = new Geocoder(context, Locale.getDefault());
		try {
			addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
			String address = addressList.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
			String city = addressList.get(0).getLocality();
			String state = addressList.get(0).getAdminArea();
			String country = addressList.get(0).getCountryName();
			String postalCode = addressList.get(0).getPostalCode();
			String knownName = addressList.get(0).getFeatureName();
			if (fullAddress) {
				Address = address;
			} else {
				if (city != null && !city.isEmpty()) {
					Address = city;
				} else {
					Address = "Point on Map";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Address;
	}

	public String getRequestedUrl(LatLng origin, LatLng destination) {
		String str_org = "origin=" + origin.latitude + "," + origin.longitude;
		String str_dest = "destination=" + destination.latitude + "," + destination.longitude;
		String sensor = "sensor=false";
		String mode = "mode=driving";
		String param = str_org + "&" + str_dest + "&" + sensor + "&" + mode;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
		Log.d("URL :", url);
		return url;
	}

	public List<LatLng> decodePoly(String encoded) {

		List latLngList = new ArrayList<LatLng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			LatLng latLng = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
			latLngList.add(latLng);
		}

		return latLngList;
	}

	public LatLngBounds findRouteLatLangBounds(LatLng sourceLatLng, LatLng destinationLatLng) {
		LatLngBounds routeLatLngBounds;
		double sourceLatitude = sourceLatLng.latitude, sourceLongitude = sourceLatLng.longitude, destinationLatitude = destinationLatLng.latitude, destinationLongitude = destinationLatLng.longitude;
		if (sourceLatitude < destinationLatitude) {
			if (sourceLongitude < destinationLongitude) {
				routeLatLngBounds = new LatLngBounds(sourceLatLng, destinationLatLng);
			} else {
				routeLatLngBounds = new LatLngBounds(new LatLng(sourceLatitude, destinationLongitude), new LatLng(destinationLatitude, sourceLongitude));
			}
		} else {
			if (destinationLongitude < sourceLongitude) {
				routeLatLngBounds = new LatLngBounds(destinationLatLng, sourceLatLng);
			} else {
				routeLatLngBounds = new LatLngBounds(new LatLng(destinationLatitude, sourceLongitude), new LatLng(sourceLatitude, destinationLongitude));
			}
		}
		return routeLatLngBounds;
	}

	@Override
	public void onLocationChanged(Location location) {
		/*Latitude = location.getLatitude();
		Longitude = location.getLongitude();*/
		UTIL_VARIABLES.location = location;
	}

	@Override
	public void onStatusChanged(String s, int i, Bundle bundle) {

	}

	@Override
	public void onProviderEnabled(String s) {

	}

	@Override
	public void onProviderDisabled(String s) {
		context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
}
