package com.cordsinnovations.rtts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Handler;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;
import com.cordsinnovations.rtts.globals.PreferenceKeys;
import com.cordsinnovations.rtts.hardware.RotationDetector;
import com.cordsinnovations.rtts.utils.AppCommonUtils;
import com.cordsinnovations.rtts.utils.FileOperationUtils;
import com.cordsinnovations.rtts.utils.LocationUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.ui.NavigationActivity.ACTIVITY_COMMONS.*;
import static com.cordsinnovations.rtts.ui.NavigationActivity.ACTIVITY_UTILS.*;
import static com.cordsinnovations.rtts.ui.NavigationActivity.UI_ELEMENTS.UI_COMMONS.*;
import static com.cordsinnovations.rtts.ui.NavigationActivity.UI_ELEMENTS.NAVIGATION_DETAILS.*;

public class NavigationActivity extends FragmentActivity implements OnMapReadyCallback, RotationDetector.OnRotationListener {

	static class ACTIVITY_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Runnable runnable;
		static InputMethodManager inputMethodManager = null;
		static Typeface typeFace;
	}

	static class ACTIVITY_UTILS {
		static AppCommonUtils appCommonUtils;
		static FileOperationUtils fileOperationUtils;
		static LocationUtils locationUtils;
	}

	static class UI_ELEMENTS {

		static class UI_COMMONS {
			static GoogleMap navigationMap;
		}

		static class NAVIGATION_DETAILS {
			static LinearLayout ll_navigation_instructions, ll_navigation_instruction_details;
			static ImageView iv_navigation_direction;
			static TextView tv_navigation_direction_title, tv_navigation_direction_distance;
			static RotationDetector rotationDetector;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_navigation);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_navigation);
		mapFragment.getMapAsync(this);

		configView();
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		navigationMap = googleMap;
		try {
			boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, sharedPreferences.getInt(PreferenceKeys.SETTINGS_MAP_STYLE_ID, R.raw.map_style_01)));
			if (!success) {
				Log.e("MapStyle", "Style parsing failed.");
			}
		} catch (Resources.NotFoundException e) {
			Log.e("MapStyle", "Can't find style. Error: ", e);
		}

		runnable = () -> handler.postDelayed(() -> {
			try {
				rotationDetector.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, 2500);

		runOnUiThread(() -> runnable.run());

		//Add a marker in Sydney and move the camera
		LatLng sydney = new LatLng(-34, 151);
		navigationMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
		navigationMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
		//navigationMap.setMapType(navigationMap.MAP_TYPE_HYBRID);
	}

	@Override
	public void onDeviceRotation() {
		rotateCurrentMarker();
	}


	private void rotateCurrentMarker() {
		//markerOperations(MARKER_REMOVE_OPERATION, MARKER_CURRENT_LOCATION, false);
		float angleAzimuth = rotationDetector.getAzimuthAngle();
		//getCurrentLocation(REQUEST_CURRENT_LOCATION, false);
		//setMarkerOptions(REQUEST_CURRENT_LOCATION);
		//markerOperations(MARKER_ADD_OPERATION, MARKER_CURRENT_LOCATION, false);
	}

	private void configView() {

		activity = this;
		context = this;
		handler = new Handler();
		inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/bebas.ttf");

		appCommonUtils = new AppCommonUtils(activity, context);
		fileOperationUtils = new FileOperationUtils(activity, context);
		locationUtils = new LocationUtils(activity, context);

		ll_navigation_instructions = findViewById(R.id.ll_navigation_instructions);
		ll_navigation_instruction_details = findViewById(R.id.ll_navigation_instruction_details);
		iv_navigation_direction = findViewById(R.id.iv_navigation_direction);
		tv_navigation_direction_title = findViewById(R.id.tv_navigation_direction_title);
		tv_navigation_direction_distance = findViewById(R.id.tv_navigation_direction_distance);

		rotationDetector = new RotationDetector(activity, context);

		rotationDetector.setRotationListener(this);
		rotationDetector.start();
	}
}