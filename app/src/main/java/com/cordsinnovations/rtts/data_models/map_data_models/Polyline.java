package com.cordsinnovations.rtts.data_models.map_data_models;

import android.os.Parcel;
import android.os.Parcelable;

public class Polyline implements Parcelable {

	private String points;

	protected Polyline(Parcel in) {
		points = in.readString();
	}

	public static final Creator<Polyline> CREATOR = new Creator<Polyline>() {
		@Override
		public Polyline createFromParcel(Parcel in) {
			return new Polyline(in);
		}

		@Override
		public Polyline[] newArray(int size) {
			return new Polyline[size];
		}
	};

	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "Polyline [points = " + points + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(points);
	}
}
