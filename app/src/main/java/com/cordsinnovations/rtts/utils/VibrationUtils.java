package com.cordsinnovations.rtts.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Vibrator;

import com.cordsinnovations.rtts.globals.PreferenceKeys;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_ROTATION_CONSTANTS.EXACT_DIRECTION_ANGLES.*;
import static com.cordsinnovations.rtts.globals.Constants.APPLICATION_CONSTANTS.DEVICE_VIBRATION_CONSTANTS.*;

import static com.cordsinnovations.rtts.utils.VibrationUtils.UTIL_COMMONS.*;
import static com.cordsinnovations.rtts.utils.VibrationUtils.OTHER_UTILS.*;
import static com.cordsinnovations.rtts.utils.VibrationUtils.UTIL_BOOLEANS.*;

/**
 * Created by Bimal on 14-09-2018.
 */

public class VibrationUtils {

	static class UTIL_COMMONS {
		static Activity activity;
		static Context context;
		static Handler handler;
		static Vibrator vibrator;
	}

	static class OTHER_UTILS {
		static MathematicsUtils mathematicsUtils;
	}

	static class UTIL_BOOLEANS {
		static boolean vibrateDevice = false;
	}

	public VibrationUtils(Activity activity, Context context) {
		UTIL_COMMONS.activity = activity;
		UTIL_COMMONS.context = context;
		UTIL_COMMONS.handler = new Handler();
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrateDevice = sharedPreferences.getBoolean(PreferenceKeys.DIRECTION_VIBRATIONS, false);

		mathematicsUtils = new MathematicsUtils(activity, context);
	}

	public void vibrateOnExactDirections(float azimuth) {
		int Azimuth = mathematicsUtils.absoluteValue(mathematicsUtils.roundFloat(azimuth));
		switch (Azimuth) {
			case DIRECTION_NORTH:
				vibrateDevice(sharedPreferences.getInt(PreferenceKeys.DIRECTION_VIBRATION_NORTH, VIBRATION_SHORT_MILLI_SECONDS), vibrateDevice);
				break;
			case DIRECTION_EAST:
				vibrateDevice(sharedPreferences.getInt(PreferenceKeys.DIRECTION_VIBRATION_EAST, VIBRATION_SHORT_MILLI_SECONDS), vibrateDevice);
				break;
			case DIRECTION_SOUTH:
				vibrateDevice(sharedPreferences.getInt(PreferenceKeys.DIRECTION_VIBRATION_SOUTH, VIBRATION_SHORT_MILLI_SECONDS), vibrateDevice);
				break;
			case DIRECTION_WEST:
				vibrateDevice(sharedPreferences.getInt(PreferenceKeys.DIRECTION_VIBRATION_WEST, VIBRATION_SHORT_MILLI_SECONDS), vibrateDevice);
				break;
		}
	}

	private void vibrateDevice(int durationInMillis, boolean vibrateDevice) {
		if (vibrateDevice) {
			vibrator.vibrate(durationInMillis);
		}
	}
}