package com.cordsinnovations.rtts.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cordsinnovations.rtts.globals.Constants;
import com.cordsinnovations.rtts.globals.PreferenceKeys;

import static com.cordsinnovations.rtts.RightToTravelSafe.sharedPreferences;

public class PermissionRequestUtils {

	private Context context;
	private Activity activity;

	private Runnable runAfterPermissionGrant;

	//private LocationUtils locationUtils;

	public PermissionRequestUtils(Activity activity, Context context) {
		this.context = context;
		this.activity = activity;
		//locationUtils = new LocationUtils(activity, context);
	}

	@TargetApi(Build.VERSION_CODES.M)
	public void requestPermissions(Runnable ifAllSetDoWhat, int requestCode) {

		runAfterPermissionGrant = ifAllSetDoWhat;

		int callPhonePermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
		int cameraAccessPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
		int contactsReadPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
		int contactsWritePermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS);
		int locationCourseAccessPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
		int locationFineAccessPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
		int microphoneAccessPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
		int networkStatePermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE);
		int smsSendPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS);
		int storageReadPermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
		int storageWritePermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
		int vibratePermissionStatus = ContextCompat.checkSelfPermission(context, Manifest.permission.VIBRATE);

		switch (requestCode) {
			case Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSIONS:
				if (callPhonePermissionStatus == PackageManager.PERMISSION_DENIED
						|| cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| contactsReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| contactsWritePermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationCourseAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationFineAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| microphoneAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| networkStatePermissionStatus == PackageManager.PERMISSION_DENIED
						|| smsSendPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageWritePermissionStatus == PackageManager.PERMISSION_DENIED
						|| vibratePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity, new String[]{
									Manifest.permission.CALL_PHONE,
									Manifest.permission.CAMERA,
									Manifest.permission.READ_CONTACTS,
									Manifest.permission.WRITE_CONTACTS,
									Manifest.permission.ACCESS_COARSE_LOCATION,
									Manifest.permission.ACCESS_FINE_LOCATION,
									Manifest.permission.RECORD_AUDIO,
									Manifest.permission.ACCESS_NETWORK_STATE,
									Manifest.permission.SEND_SMS,
									Manifest.permission.READ_EXTERNAL_STORAGE,
									Manifest.permission.WRITE_EXTERNAL_STORAGE,
									Manifest.permission.VIBRATE},
							Constants.PERMISSION_REQUESTS.REQUEST_ALL_PERMISSIONS);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAL, true)
							.putBoolean(PreferenceKeys.PERMISSION_CAM, true)
							.putBoolean(PreferenceKeys.PERMISSION_CNT, true)
							.putBoolean(PreferenceKeys.PERMISSION_LOC, true)
							.putBoolean(PreferenceKeys.PERMISSION_MIC, true)
							.putBoolean(PreferenceKeys.PERMISSION_NET, true)
							.putBoolean(PreferenceKeys.PERMISSION_SMS, true)
							.putBoolean(PreferenceKeys.PERMISSION_STR, true)
							.putBoolean(PreferenceKeys.PERMISSION_VBR, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION:
				if (callPhonePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.CALL_PHONE
							},
							Constants.PERMISSION_REQUESTS.REQUEST_CALL_PERMISSION);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAL, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSION:
				if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.CAMERA
							},
							Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_PERMISSION);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAM, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_CONTACT_PERMISSIONS:
				if (contactsReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| contactsWritePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.READ_CONTACTS,
									Manifest.permission.WRITE_CONTACTS
							},
							Constants.PERMISSION_REQUESTS.REQUEST_CONTACT_PERMISSIONS);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CNT, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_PERMISSIONS:
				if (locationCourseAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationFineAccessPermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.ACCESS_COARSE_LOCATION,
									Manifest.permission.ACCESS_FINE_LOCATION
							},
							Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_PERMISSIONS);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_LOC, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_MICROPHONE_PERMISSION:
				if (microphoneAccessPermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.RECORD_AUDIO
							},
							Constants.PERMISSION_REQUESTS.REQUEST_MICROPHONE_PERMISSION);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_MIC, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_NETWORK_STATE_PERMISSION:
				if (networkStatePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.ACCESS_NETWORK_STATE
							},
							Constants.PERMISSION_REQUESTS.REQUEST_NETWORK_STATE_PERMISSION);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_NET, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_SMS_PERMISSION:
				if (smsSendPermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.SEND_SMS
							},
							Constants.PERMISSION_REQUESTS.REQUEST_SMS_PERMISSION);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_SMS, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS:
				if (storageReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageWritePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.READ_EXTERNAL_STORAGE,
									Manifest.permission.WRITE_EXTERNAL_STORAGE
							},
							Constants.PERMISSION_REQUESTS.REQUEST_STORAGE_PERMISSIONS);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_STR, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_VIBRATE_PERMISSIONS:
				if (vibratePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity,
							new String[]{
									Manifest.permission.VIBRATE
							},
							Constants.PERMISSION_REQUESTS.REQUEST_VIBRATE_PERMISSIONS);
				} else {
					sharedPreferences.edit()
							.putBoolean(PreferenceKeys.PERMISSION_VBR, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_AND_STORAGE_PERMISSIONS:
				if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageWritePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity, new String[]{
									Manifest.permission.CAMERA,
									Manifest.permission.READ_EXTERNAL_STORAGE,
									Manifest.permission.WRITE_EXTERNAL_STORAGE},
							Constants.PERMISSION_REQUESTS.REQUEST_CAMERA_AND_STORAGE_PERMISSIONS);
				} else {
					sharedPreferences
							.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAM, true)
							.putBoolean(PreferenceKeys.PERMISSION_STR, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_VIDEO_AND_STORAGE_PERMISSIONS:
				if (cameraAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| microphoneAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageReadPermissionStatus == PackageManager.PERMISSION_DENIED
						|| storageWritePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity, new String[]{
									Manifest.permission.CAMERA,
									Manifest.permission.RECORD_AUDIO,
									Manifest.permission.READ_EXTERNAL_STORAGE,
									Manifest.permission.WRITE_EXTERNAL_STORAGE},
							Constants.PERMISSION_REQUESTS.REQUEST_VIDEO_AND_STORAGE_PERMISSIONS);
				} else {
					sharedPreferences
							.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAM, true)
							.putBoolean(PreferenceKeys.PERMISSION_MIC, true)
							.putBoolean(PreferenceKeys.PERMISSION_STR, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_AND_NETWORK_STATE_PERMISSIONS:
				if (locationCourseAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationFineAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| networkStatePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity, new String[]{
									Manifest.permission.ACCESS_COARSE_LOCATION,
									Manifest.permission.ACCESS_FINE_LOCATION,
									Manifest.permission.ACCESS_NETWORK_STATE},
							Constants.PERMISSION_REQUESTS.REQUEST_LOCATION_AND_NETWORK_STATE_PERMISSIONS);
				} else {
					sharedPreferences
							.edit()
							.putBoolean(PreferenceKeys.PERMISSION_LOC, true)
							.putBoolean(PreferenceKeys.PERMISSION_NET, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
			case Constants.PERMISSION_REQUESTS.REQUEST_CALL_AND_LOCATION_AND_NETWORK_STATE_PERMISSIONS:
				if (callPhonePermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationCourseAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| locationFineAccessPermissionStatus == PackageManager.PERMISSION_DENIED
						|| networkStatePermissionStatus == PackageManager.PERMISSION_DENIED) {
					ActivityCompat.requestPermissions(activity, new String[]{
									Manifest.permission.CALL_PHONE,
									Manifest.permission.ACCESS_COARSE_LOCATION,
									Manifest.permission.ACCESS_FINE_LOCATION,
									Manifest.permission.ACCESS_NETWORK_STATE},
							Constants.PERMISSION_REQUESTS.REQUEST_CALL_AND_LOCATION_AND_NETWORK_STATE_PERMISSIONS);
				} else {
					sharedPreferences
							.edit()
							.putBoolean(PreferenceKeys.PERMISSION_CAL, true)
							.putBoolean(PreferenceKeys.PERMISSION_LOC, true)
							.putBoolean(PreferenceKeys.PERMISSION_NET, true)
							.apply();
					if (runAfterPermissionGrant != null) {
						runAfterPermissionGrant.run();
					}
				}
				break;
		}
	}
}
