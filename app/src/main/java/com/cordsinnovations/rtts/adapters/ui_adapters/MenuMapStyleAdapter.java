package com.cordsinnovations.rtts.adapters.ui_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cordsinnovations.rtts.R;

public class MenuMapStyleAdapter extends BaseAdapter {

	Context context;
	String Titles[];
	int Images[];
	int SelectedPosition;
	LayoutInflater layoutInflater;

	public MenuMapStyleAdapter(Context context, String[] titles, int[] images, int selectedPosition) {
		this.context = context;
		Titles = titles;
		Images = images;
		SelectedPosition = selectedPosition;
		layoutInflater = (LayoutInflater.from(context));
	}

	@Override
	public int getCount() {
		return Titles.length;
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		view = layoutInflater.inflate(R.layout.layout_map_style, null);
		TextView tv_map_style_title = view.findViewById(R.id.tv_map_style_title);
		ImageView iv_map_style = view.findViewById(R.id.iv_map_style);
		tv_map_style_title.setText(Titles[position]);
		if (position == SelectedPosition) {
			view.setBackground(context.getResources().getDrawable(R.drawable.app_selected_item));
		}
		iv_map_style.setImageDrawable(context.getResources().getDrawable(Images[position]));
		return view;
	}
}
